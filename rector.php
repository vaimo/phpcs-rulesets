<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

declare(strict_types=1);

use Rector\Config\RectorConfig;
use Rector\Php74\Rector\Property\TypedPropertyRector;
use Rector\Set\ValueObject\LevelSetList;

return static function (RectorConfig $rectorConfig): void {
    $rectorConfig->bootstrapFiles([
        // Composer autoload
        __DIR__ . '/vendor/autoload.php',
        // PHPCS uses its own autoload, so we need it
        __DIR__ . '/vendor/squizlabs/php_codesniffer/autoload.php',
        // PHPCS defines all the token constants in this file and does not require it in composer.json
        __DIR__ . '/vendor/squizlabs/php_codesniffer/src/Util/Tokens.php',
    ]);

    $rectorConfig->paths([
        __DIR__ . '/Vaimo/',
        __DIR__ . '/tests/',
    ]);

    // We don't want to "fix" test dummies, the vast majority of them are not even compilable
    $rectorConfig->skip([
        __DIR__ . '/tests/**/data/*',
    ]);

    $rectorConfig->importNames();
    $rectorConfig->disableImportShortClasses();

    $rectorConfig->sets([
        LevelSetList::UP_TO_PHP_74,
    ]);

    $rectorConfig->ruleWithConfiguration(TypedPropertyRector::class, [
        TypedPropertyRector::INLINE_PUBLIC => true,
    ]);
};
