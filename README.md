Vaimo Coding Standard
=====================

This repository contains configuration for `phpcs` and a group of additional sniffs. 
Ruleset is actually a mixture of a bit reconfigured Doctrine Coding Standard, Magento Coding Standard,
and a bunch of additional checks.

Installation
------------

For Magento projects, you must first remove the already required version of squizlabs/php_codesniffer.
```
composer remove --dev --no-update squizlabs/php_codesniffer
```

Now you can require this package
```
composer require --dev --with-all-dependencies vaimo/phpcs-rulesets
```

Yes, that's it, you're ready to go!

```
vendor/bin/phpcs --standard=Vaimo --extensions=phtml,php,graphqls -p -s src/
```

Configuration
-------------

### phpcs.xml

In order to NOT type this command each time you want to check your code, 
it would be nice to put `phpcs.xml` (or `phpcs.xml.dist`) into you project root folder. 
In this case, `phpcs` will read all configuration parameters from this file.

```xml
<ruleset name="" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
         xsi:noNamespaceSchemaLocation="https://raw.githubusercontent.com/squizlabs/PHP_CodeSniffer/master/phpcs.xsd">
    <!-- A fully annotated list of options is available here: 
         https://github.com/squizlabs/PHP_CodeSniffer/wiki/Annotated-Ruleset -->
    <!-- File extensions to check -->
    <arg name="extensions" value="php,phtml,graphqls" />
    <!--  Strip base directory from file paths in output -->
    <arg name="basepath" value="." />
    <!-- Use colored output -->
    <arg name="colors" />
    <!-- Show progress bar -->
    <arg value="p" />
    <!-- Show sniff message codes in errors -->
    <arg value="s" />
    <!-- Configures overall minimum severity -->
    <arg name="severity" value="1" />
    <!-- Configure minimum severity for errors -->
    <arg name="error-severity" value="1" />
    <!-- Configure minimum severity for warnings -->
    <arg name="warning-severity" value="1" />
    
    <!-- Files to check if none are specified on the command line -->
    <file>./src</file>
    <file>./test</file>

    <exclude-pattern>coverage/*</exclude-pattern>
    <exclude-pattern>vendor/*</exclude-pattern>

    <rule ref="Vaimo" />
</ruleset>
```

You can use `ci/phpcs.xml` as a reference (explained in details in 
[phpcs documentation](https://github.com/squizlabs/PHP_CodeSniffer/wiki/Advanced-Usage#using-a-default-configuration-file) 
or in [phpcs.xml reference](docs/how-to/phpcs_xml.md)).

### Pre commit hook

Assuming you use GIT, open `.git/hooks/pre-commit` and add there this script:

```bash
# Get the list of staged files from GIT (what are you going to commit)
FILES=`git diff --name-only --diff-filter=ACMR --staged`
# if the list is not empty
if [ ! -z "$FILES" ]; then
    # run phpcs for these files (this line assumes you have configured phpcs via phpcs.xml)
    # Adapt this line to your virtual machine setup!
    vendor/bin/phpcs $FILES
    # exit with code from phpcs, it will return 1 if there were any errors and thus prevent actual commit
    exit $?;
fi
```

The snippet above is good in case you run `git` on host machine and `phpcs` on virtual machine. 
If you use both `git` and `phpcs` on only one machine, you can just replace it with

```shell
vendor/bin/phpcs --filter=gitstaged
```

Make sure the file is executable! (`chmod +x` it)

**Note**: Use [CaptainHook](https://captainhookphp.github.io/captainhook/index.html) 
to store your pre commit hooks as a code!

Vaimo Code Quality integration
------------------------------

Follow [this tutorial](docs/how-to/vcq-integration.md).

Further Reading
---------------

* [Contribution guide](docs/contributing.md)
* [Coding Standard description](docs/standard.md)
* [Get started with your project](docs/how-to/old-project-onboarding.md)
* [Known Issues](docs/known-issues.md)


How To:
-------

* [Add a Sniff](https://github.com/squizlabs/PHP_CodeSniffer/wiki/Coding-Standard-Tutorial)
* [Configure a language level](docs/how-to/language_level.md)
