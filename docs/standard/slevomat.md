Slevomat Sniffs
===============

Started as Slevomat's internal project it quickly grew into something like 
"collection of best practices" for Symfony-related PHP world.

These sniffs are intended to:

* improve safety and behavior of the code;
* dead code detection;
* improve code formatting to make it look more consistent.

If you don't want this part in your ruleset, simply do

```
<ruleset name="">
    <rule ref="Vaimo">
        <exclude name="Slevomat">
    </rule>
</ruleset>
```

Although, it is encouraged to at least give it a try :)  

SlevomatCodingStandard.Arrays.DisallowImplicitArrayCreation
-----------------------------------------------------------

Sniff forbids adding items to not existing array:

```php
foreach ($array as $item) {
    $result[] = doSomething($item); // $result is not created earlier
}
```

So, sniffs always requires to create an array first:

```php
$result = [];
foreach ($array as $item) {
    $result[] = doSomething($item);
}
```

SlevomatCodingStandard.Arrays.SingleLineArrayWhitespace
-------------------------------------------------------

Sniff ensures spacing in single line array declarations:

* There MUST NOT be a whitespace after opening bracket and before closing one;
* There MUST be exactly one whitespace after a comma;
* There MUST NOT be a whitespace before comma.


Instead of:

```php
$test = [ 'test' => 'value','test' ,'value' ];
```

Do this:

```php
$test = ['test' => 'value', 'test', 'value'];
```

SlevomatCodingStandard.Arrays.TrailingArrayComma
------------------------------------------------

Requires trailing comma in multi-line arrays:

```php
return [
    'one',
    'two', // <- this comma
];
```

It helps with `git diff`. If you add next element to an array _with_ trailing comma, 
only one line will get added in a diff. Otherwise, there would be 1 deletion and 2 additions, 
making diff bigger and harder to read.

SlevomatCodingStandard.Classes.ClassConstantVisibility
------------------------------------------------------

Implements of [PSR-12 Section 4.3](https://www.php-fig.org/psr/psr-12/#43-properties-and-constants) in part:

> Visibility MUST be declared on all constants
> if your project PHP minimum version supports constant visibilities (PHP 7.1 or later).

Sniff is better implemented than PSR12.Properties.ConstantVisibility:

* It has auto fixing;
* It has better error message.


SlevomatCodingStandard.Classes.DisallowLateStaticBindingForConstants
--------------------------------------------------------------------

Forbids use of `static::CONSTANT`:

```
class Example {
    public const CONSTANT = 1;
    
    public function getConstant() 
    {
        return static::CONSTANT;
    }
}
```

Calling `static::CONSTANT` instead of `self::CONSTANT` implies that constant might be changed
in a class extending `Example`, which contradicts nature of a _constant_: being unchangeable.

SlevomatCodingStandard.Classes.DisallowMultiConstantDefinition
--------------------------------------------------------------

Sniff disallows multiple constant declarations one line:

Instead of
```php
public const ONE = 'one', TWO = 'two';
```

Do

```php
public const ONE = 'one';
public const TWO = 'two';
```

This makes constant declaration more visible. 
Interesting that PSR-12 explicitly forbids multiline property declarations, but allows it for constants.
The sniff fixes this inconsistency.

SlevomatCodingStandard.Classes.EmptyLinesAroundClassBraces
----------------------------------------------------------

Enforces one configurable number of lines after opening class/interface/trait brace and one empty line before the closing brace.

Instead of 

```php
class Example
{

    public const TEST = 'value';
    
}
```

Do:

```php
class Example
{
    public const TEST = 'value';
}
```

Sniff provides the following settings:

### linesCountAfterOpeningBrace ###

**default value**: 0

Allows to configure the number of lines after opening brace.

### linesCountBeforeClosingBrace ###

**default value**: 0

Allows to configure the number of lines before closing brace.

SlevomatCodingStandard.Classes.ModernClassNameReference
-------------------------------------------------------

Requires use of `::class` constant instead of `get_class($this)`, `__CLASS__`, `get_called_class`, etc...

This constant was introduced a while ago, and covers all possible use-cases, so that it would be nice to make it only one way:

```php
class Example {
    public function __construct()
    {
        var_dump(static::class); // you can do it this way.
    }
}

class T extends Example
{

}

$var = new T();
```

```php
$var = new T();
var_dump(get_class($var)); 
// This code is absolutely fine though, since use `$var::class` is possible only in PHP 8.
```

SlevomatCodingStandard.Classes.SuperfluousAbstractClassNaming
-------------------------------------------------------------

Forbids `Abstract` _suffix_ for abstract class names:

```
abstract class AbstractForm {} // is fine
abstract class FormAbstract {} // is not
```

SlevomatCodingStandard.Classes.SuperfluousExceptionNaming
---------------------------------------------------------

Forbids `Exception` _prefix_ for exception class names:

```
abstract class FormException {} // is fine
abstract class ExceptionForm {} // is not
```

SlevomatCodingStandard.Classes.SuperfluousInterfaceNaming
---------------------------------------------------------

Forbids `Interface` _prefix_ for interfaces:

```php
interface InterfaceLogger {} // looks weird
interface LoggerInterface {} // is ok
```

SlevomatCodingStandard.Classes.TraitUseSpacing
----------------------------------------------

Forces empty lines around trait uses in a class:

* There MUST be an empty line after last use, but not in case it's last line in a class:
  
```php
class Example 
{
  use LoggableTrait;

  public function __construct() {}
}

class Example 
{
  use LoggableTrait;
}
```

* There MUST be no empty lines between trait uses;
* There MUST NOT be an empty line before first use, if it is the first member of a class.
  

The ruleset above is a result of default sniff config. It has the following properties:

### linesCountAfterLastUse ###

**default value**: 1

### linesCountAfterLastUseWhenLastInClass ###

**default value**: 0

### linesCountBeforeFirstUse ###

**default value**: 0

### linesCountBetweenUses ###

**default value**: 0

SlevomatCodingStandard.Classes.UselessLateStaticBinding
-------------------------------------------------------

Forbids use of [late static binding](https://www.php.net/manual/en/language.oop5.late-static-bindings.php) in final classes:

```php
final class Example
{
    public function doSomething()
    {
        return static::class; // use `self` instead.
    }
}
```

Since class is final, it is impossible to extend it and therefore use of LSB makes no sense here.

SlevomatCodingStandard.Commenting.DisallowOneLinePropertyDocComment
-------------------------------------------------------------------

Forbids one-line property docs:

```php
class Example 
{
    // Instead of this:
    /** @var LoggerInterface */
    private $logger;

    // do this:
    /**
     * @var Property
     */
    private $property; 
}
```

The reasoning is again: "there must be one and only way of doing things". 
It's better when it is consistent throughout the whole codebase. 

In case you want _other_ way of consistent, 
disable it and enable `SlevomatCodingStandard.Commenting.RequireOneLinePropertyDocCommentSniff`.

SlevomatCodingStandard.Commenting.EmptyComment
----------------------------------------------

Forbids empty comments:

```php
//
```

It's not very informative, is it?

SlevomatCodingStandard.Commenting.ForbiddenAnnotations
------------------------------------------------------

Forbids the following phpDoc annotations:                 

 * @category
 * @copyright
 * @created
 * @license
 * @package
 * @subpackage
 * @version

This information is accessible from LICENSE file, package _name_, class namespace, and VCS;
there is no need to duplicate it.

SlevomatCodingStandard.Commenting.ForbiddenComments
---------------------------------------------------

Reports if comment contains useless information by comparing comment description with a list of patterns.

### forbiddenCommentPatterns ###

Array of regexp patterns to look for. Default items are:

* `~^(?:(?!private|protected|static)\S+ )?(?:con|de)structor\.\z~i` comment should not be a 
  visibility modifier and should say "something constructor".
* `~^Created by \S+\.\z~i"` `git blame` is better alternative.
* `~^\S+ [gs]etter\.\z~i"`. Well, if it's called `getSomething()`, there is no need for a comment "something getter.".

SlevomatCodingStandard.Commenting.InlineDocCommentDeclaration
-------------------------------------------------------------

Forces two additional rules for inline doc blocks containing `@var` annotation:

* It MUST start with `/**`, not `/*`:

```php
/** @var Type $name */
$name = new Type();
```

* It MUST be in the following format: `/** @var Type $name`.

SlevomatCodingStandard.ControlStructures.AssignmentInCondition
--------------------------------------------------------------

Forbids assignments in condition:

```php
if ($response = $this->getResponse()) {
    doSomething();
}
```

One can read it two ways: 

* oh, it looks like a typo, it should be `==`;
* oh, someone was lazy to type `$response` twice.

It _always_ better to have only one way to understand something. Even though I personally like this 
assignment in condition (I am lazy), I do agree with this rule.


SlevomatCodingStandard.ControlStructures.BlockControlStructureSpacing
---------------------------------------------------------------------

Enforces configurable number of lines around block control structures (if, foreach, ...).

Instead of:
```php
foreach ($list as $item) {

    if ($item === null) {
        doSomethingElse();
    }
    if ($item === 'test') {
        continue;
    }
    doSomething($item);
    
}
```

Do this:
```php
foreach ($list as $item) {
    if ($item === null) {
        doSomethingElse();
    }
    
    if ($item === 'test') {
        continue;
    }
    
    doSomething($item);
}
```

These additional whitespaces serve the same purpose as smaller paragraphs in natural texts: they help to break the idea 
in to more digestable pieces so that it is much easier to understand it. 

Having whitespaces between control structures helps a lot with code readability.

Note that by default sniff does complain only about inconsistent spacing **after** a control structure 
to avoid duplicated notices.

Sniff provides the following settings:

### linesCountBefore ###

**default value**: 1

Allows to configure the number of lines before control structure.

### linesCountBeforeFirst ###

**default value**: 0

Allows to configure the number of lines before first control structure.

### linesCountAfter ###

**default value**: 1

Allows to configure the number of lines after control structure.

### linesCountAfterLast ###

**default value**: 0

Allows to configure the number of lines after last control structure.

### controlStructures ### 

**default value**: [if, do, while, for, foreach, switch, try, default]

Allows to narrow the list of checked control structures.

SlevomatCodingStandard.ControlStructures.DisallowYodaComparison
---------------------------------------------------------------

Forbids [Yoda conditions](https://en.wikipedia.org/wiki/Yoda_conditions).
Yes, everybody agrees it is a safe way to avoid accidental assignment instead of comparison in condition, 
but it's unnatural to read in a context where everything is written left-to-right.

Although, one can disable this sniff and enable its `RequireYodaComparison` counterpart if their team finds it better.

SlevomatCodingStandard.ControlStructures.EarlyExit
--------------------------------------------------

Forces using [early exit](https://szymonkrajewski.pl/why-should-you-return-early/) wherever it's possible.

It helps with making code more flat and easy to read:

```php
function sendEmail(string $email, string $message)
{
    if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
        if ($message !== '') {
            return Mailer::send($email, $message);
        } else {
            throw new InvalidArgumentException('Cannot send an empty message.');
        }
    } else {
        throw new InvalidArgumentException('Email is not valid.');
    }
}
```

```php
function sendEmail(string $email, string $message)
{
    if (! filter_var($email, FILTER_VALIDATE_EMAIL)) {
        throw new InvalidArgumentException('Email is not valid.');
    }

    if ($message === '') {
        throw new InvalidArgumentException('Cannot send an empty message.');
    }

    return Mailer::send($email, $message);
}
```

The second example is simpler to understand, isn't it?

SlevomatCodingStandard.ControlStructures.JumpStatementsSpacing
--------------------------------------------------------------

Enforces configurable number of lines around jump statements (continue, return, ...).

Like `BlockControlStructureSpacing`, this sniff makes sure there is some whitespace around jump statements
which helps with code readability.

Instead of:

```php
    public function example(): int
    {
        switch (true) {
            case 'asdf':
                return 1;
            case 'asdfsdf':
                return 2;
        }
    }
```

Do this:

```php
    public function example(): int
    {
        switch (true) {
            case 'asdf':
                return 1;
                
            case 'asdfsdf':
                return 2;
        }
    }
```

Sniff provides the following settings:

### allowSingleLineYieldStacking ###

**default value**: true

Whether or not to allow multiple yield/yield from statements in a row without blank lines.

### linesCountBefore ###

**default value**: 1

Allows to configure the number of lines before jump statement.

### linesCountBeforeFirst ###

**default value**: 0

Allows to configure the number of lines before first jump statement.

### linesCountBeforeWhenFirstInCaseOrDefault ### 

**default value**: 1

Allows to configure the number of lines before jump statement that is first in `case` or `default`.

### linesCountAfter ###

**default value**: 1

Allows to configure the number of lines after jump statement.

### linesCountAfterLast ###

**default value**: 0

Allows to configure the number of lines after last jump statement.

### linesCountAfterWhenLastInCaseOrDefault ### 

**default value**: 1

Allows to configure the number of lines after jump statement that is last in `case` or `default`.

### linesCountAfterWhenLastInLastCaseOrDefault ### 

**default value**: 0

Allows to configure the number of lines after jump statement that is last in last `case` or `default`.

### jumpStatements ###

**default value**: [return, throw, yield, yield_from]

Allows to narrow the list of checked jump statements.

For example, with the following setting, only `continue` and `break` keywords are checked.

SlevomatCodingStandard.ControlStructures.LanguageConstructWithParentheses
-------------------------------------------------------------------------

Forbids use of PHP language constructs (`echo`, `print`, etc...) with brackets.

It's not a function, and it should not look like a function call.


SlevomatCodingStandard.ControlStructures.RequireNullCoalesceOperator
--------------------------------------------------------------------

Requires use of `??` instead of ternary operator when possible:

```php
// This can be simplified
isset($var['test']) ? $var['test'] : null;
// to this:
$var['test'] ?? null;
```

SlevomatCodingStandard.ControlStructures.RequireNullCoalesceEqualOperator
-------------------------------------------------------------------------

Require use of `??=` operator, when possible:

Instead of:

```php
$this->request->data['comments']['user_id'] = $this->request->data['comments']['user_id'] ?? 'value';
```

Do this:

```php
$this->request->data['comments']['user_id'] ??= 'value';
```

Note that this operator is introduced in PHP 7.4. This automatically disables itself on elder versions.

SlevomatCodingStandard.ControlStructures.UselessIfConditionWithReturn
---------------------------------------------------------------------

Forbids useless "if with return" conditions:

Instead of

```php
if ($test === 'test') {
    return true;
} else {
    return false;
}
```

Do this:

```php
return $test === 'test';
```

    
SlevomatCodingStandard.ControlStructures.UselessTernaryOperator
---------------------------------------------------------------

Forbids usage of boolean-only ternary operator usage (e.g. $foo ? true : false).

Instead of:

```
$test = $foo ? true : false;
```

Do:

```
$test = $foo;
```

The sniff has the following settings:

### assumeAllConditionExpressionsAreAlreadyBoolean ###

**default value**: false

By default, sniff checks whether condition is a boolean expression and error is raised only in this case. 
You can disable this behavior by setting the option to `true`.

SlevomatCodingStandard.Exceptions.DeadCatch
-------------------------------------------

Reports catch blocks after `\Throwable` is caught:

```php
try {
    doDangerousStuff();
} catch (\Throwable $e) {
    // handle exception
} catch (LocalizedException $e) {
    // you never gonna reach this, because it _always_ goes to `throwable` above.
}
```

Unfortunately, sniff checks only catches after `\Throwable`.

SlevomatCodingStandard.Exceptions.ReferenceThrowableOnly
--------------------------------------------------------

Before PHP 7.0, we used the following code as a gate-keeper for all possible errors:

```php
try {

} catch (\Exception $e) {}
```

Since PHP 7 introduced new set of errors that do not extend `\Exception` and `\Throwable` interface 
to combine both `Error` and `Exception` hierarchies, we should use the following as a gate-keeper:

```php
try {

} catch (\Throwable $e)
```

Although, remember that Magento DOES NOT support `\Throwable` in a lot of places (`Magento\Framework\Message\Manager` for example). It means we MUST be careful when following this recommendation.

SlevomatCodingStandard.Functions.StaticClosure
----------------------------------------------

Asks to mark static closure `static` expilicitly:

```php
class Example 
{
  public function filterArray(array $array) 
  {
    // This closure does not refer to `$this` and therefore can be declared static
    // to save some performance
    // and make it explicitly fail if one tries to use $this inside of it or bind new context to it
    return array_filter($array, function ($item) {
        return $item > 1;
    });
  }
}
```


SlevomatCodingStandard.Functions.UnusedInheritedVariablePassedToClosure
-----------------------------------------------------------------------

Reports when finds a closure that `uses` more variable than it actually needs:

```php
class Example 
{
  public function filterArray(array $array) 
  {
    // This closure declares `$array` as used, but never touches it in execution process.
    // This would be reported as an error. 
    return array_filter($array, static function ($item) use ($array) {
        return $item > 1;
    });
  }
}
```

SlevomatCodingStandard.Namespaces.AlphabeticallySortedUses
----------------------------------------------------------

Forces class `use` declarations to be sorted in alphabetical order.

SlevomatCodingStandard.Namespaces.MultipleUsesPerLine
-----------------------------------------------------

Forces one `use` per line rule:

```php
use Example, AnotherClass; // this is forbidden;

use AnotherClass;
use Example; // You should use one line per each statement.
```

SlevomatCodingStandard.Namespaces.NamespaceSpacing
--------------------------------------------------

Forces [PSR-12 Section 3](https://www.php-fig.org/psr/psr-12/#3-declare-statements-namespace-and-import-statements) in part:

> If present, each of the blocks below MUST be separated by a single blank line, and MUST NOT contain a blank line.

The rule is checked only for namespace declaration.

SlevomatCodingStandard.Namespaces.ReferenceUsedNamesOnly
--------------------------------------------------------

Sniff requires referencing names (classes, functions, and constants) only via `use` statements:

Instead of:

```
/**
 * @return Magento\Catalog\Api\ProductInterface[]
 */
public function getProducts(): array
```

Do:

```
use Magento\Catalog\Api\ProductInterface;

/**
 * @return ProductInterface[]
 */
public function getProducts(): array
```

This allows to have a consistent strategy regarding `use`/fqcn references throughout a codebase. 
In case team wants _other way_ of consistency, `SlevomatCodingStandard.Namespaces.UseOnlyWhitelistedNamespaces` should be used.

Note that the sniff is disabled in template files.

Sniff provides the following configuration:

### searchAnnotations ###

**default value**: true

Whether to search for references in annotations, like `@return`.

### namespacesRequiredToUse ###

**default value**: []

If not set, all namespaces are required to be used. When set, only mentioned namespaces are required to be used. 
Useful in tandem with UseOnlyWhitelistedNamespaces sniff.

### allowFullyQualifiedExceptions ###

**default value**: false

Whether to allows exception references to be FQCNs 
(`Magento\Framework\Exception\LocalizedException` instead of `LocalizedException` with `use` statement).

### specialExceptionNames ###

**default value**: []

If set, allows _some_ exceptions to be referenced as FQCN.

### allowFullyQualifiedNameForCollidingClasses ###

**default value**: true

Whether to allow reference by FQCN when it collides with already imported class:

```php
use Magento\Catalog\Model\ResourceModel\Product;

public function __construct(
    Product $productResource,
    Vaimo\FredhopperProduct\Indexer\Product $productIndexer
)
```

### allowFullyQualifiedNameForCollidingFunctions ###

**default value**: true

Whether to allow reference by FQCN when it collides with already imported function.

### allowFullyQualifiedNameForCollidingConstants ###

**default value**: true

Whether to allow reference by FQCN when it collides with already imported constant.

### allowFullyQualifiedGlobalClasses ###

**default value**: true

Whether to allow reference by FQCN for classes in global namespace:

```php
$now = new \DateTimeImmutable();
```

### allowFullyQualifiedGlobalFunctions ###

**default value**: true

Whether to allow reference by FQCN for functions in global namespace:

```php
$length = \strlen($string);
```

### allowFullyQualifiedGlobalConstants ###

**default value**: true

Whether to allow reference by FQCN for constants in global namespace:

```php
$version = \PHP_VERSION;
```

### allowFallbackGlobalFunctions ###

**default value**: false

Whether to allow using global functions via fallback name:

```php
namespace Vaimo\AwesomeModule;

$length = strlen($string); // This function name is resolved to `\strlen` via fallback name
```

### allowFallbackGlobalConstants ###

**default value**: true

Whether to allow using global constants via fallback name.

### allowPartialUse ###

**default value**: true

Whether to allow using the whole namespace and then referencing it:

```php
use Magento\Catalog\Model\ResourceModel;

public function __construct(ResourceModel\Product $productResource)
```

SlevomatCodingStandard.Namespaces.RequireOneNamespaceInFile
-----------------------------------------------------------

The sniff implements the following rule:

> A file must contain at most one namespace.

It is easier to autoload files with single namespace. 

**NB!** Having multiple namespaces in one file might be useful in tests (for global function overload), 
so that this sniff is disabled in test environment:

```xml
<rule ref="SlevomatCodingStandard.Namespaces.RequireOneNamespaceInFile">
  <exclude-pattern>*Test.php</exclude-pattern>
</rule>
```

SlevomatCodingStandard.Namespaces.UnusedUses
--------------------------------------------

Sniff iterates a list of imports in a file and finds those that are never used.

SlevomatCodingStandard.Namespaces.UseFromSameNamespace
------------------------------------------------------

Sniff iterates a list of imports in a file and finds those that are from the same namespace as the file itself:


```php
namespace Vaimo\ExampleModule\Service;

// This import is not needed, since current class is in the same namespace
use Vaimo\ExampleModule\Service\AnotherService; 

class Example {}
```

SlevomatCodingStandard.Namespaces.UseSpacing
--------------------------------------------

Forces use spacing according to [PSR-12 Section 3](https://www.php-fig.org/psr/psr-12/#3-declare-statements-namespace-and-import-statements):

> If present, each of the blocks below MUST be separated by a single blank line, 
> and MUST NOT contain a blank line. 
> Each block MUST be in the order listed below, although blocks that are not relevant may be omitted.
> 
> * One or more class-based use import statements.
> * One or more function-based use import statements.
> * One or more constant-based use import statements.

This is achieved by the following configuration options:

### linesCountBeforeFirstUse ###

**default value**: 1

### linesCountBetweenUseTypes ###

**default value**: 1

### linesCountAfterLastUse ###

**default value**: 1

SlevomatCodingStandard.Namespaces.UselessAlias
----------------------------------------------

Sniff iterates a list of imports in a file, finds aliased imports and checks that alias does not equal actual class name:

```php
// hello, I'm useless alias!
use Magento\Framework\Exception\LocalizedException as LocalizedException;
```

SlevomatCodingStandard.Operators.NegationOperatorSpacing
--------------------------------------------------------

Requires exactly 0 spaces after minus sign, when it is used as a negation operator.

Instead of:

```php
$t = - 1;
```

Do this:

```php
$t = -1;
```

Sniff has the only configuration option:

### spacesCount ###

**default value**: 0

Amount of spaces to require.

SlevomatCodingStandard.Operators.RequireCombinedAssignmentOperator
------------------------------------------------------------------

Sniff requires use of required combined assignment operators when it is possible:

```php
// This should be replaced with:
$i = $i + $b; 
// this:
$i += $b;
```

SlevomatCodingStandard.Operators.SpreadOperatorSpacing
------------------------------------------------------

Forbids whitespace inside spread operator:

```php
// Instead of
function doSomething(... $options) 
{
}

// Do this
function doSomething(...$options)
{
}
```

Starting from PHP 7.4 also checks spacing for spread operator in [array unpacking](https://wiki.php.net/rfc/spread_operator_for_array):

```php
$array = [...$array2];
```

SlevomatCodingStandard.PHP.OptimizedFunctionsWithoutUnpacking
-------------------------------------------------------------

Forbids argument unpacking for functions specialized by PHP VM:

```php
in_array(...$var);
```

SlevomatCodingStandard.PHP.ShortList
------------------------------------

Forces short `list` syntax:

```php
// instead of this:
list($a, $b) = $this->getSomething();
// use this:
[$a, $b] = $this->getSomething();
```

SlevomatCodingStandard.PHP.TypeCast
-----------------------------------

Sniff covers [PSR-12 rule about short form of type keywords](https://www.php-fig.org/psr/psr-12/#25-keywords-and-types) for type casts:

> Short form of type keywords MUST be used i.e. bool instead of boolean, int instead of integer etc.

In addition, it prevents use of `(binary)` and `(unset)` as casts.

```php
// Instead of
$t = (boolean) $t;
// use this:
$t = (bool) $t;
```

SlevomatCodingStandard.PHP.UselessParentheses
---------------------------------------------

Sniff looks for useless parentheses (that contain only one variable):

```php
// Instead of
$x = (self::$a);
// use this:
$x = self::$a;
```

Sniff can be configured to ignore ternary operators:

### ignoreComplexTernaryConditions ###

**default value**: false

SlevomatCodingStandard.PHP.UselessSemicolon
-------------------------------------------

Sniff looks for useless semicolons in code and reports findings:

```php
class Example 
{

}; // <-- this semicolon is useless
```

SlevomatCodingStandard.TypeHints.LongTypeHints
----------------------------------------------

Sniff covers [PSR-12 rule about short form of type keywords](https://www.php-fig.org/psr/psr-12/#25-keywords-and-types) for type hints:

> Short form of type keywords MUST be used i.e. bool instead of boolean, int instead of integer etc.

```php
// instead of this
function isThisTrue($a): boolean {}
// do this
function isThisTrue($a): bool {}
```

SlevomatCodingStandard.TypeHints.NullTypeHintOnLastPosition
-----------------------------------------------------------

Sniff forces `null` to be in last position in complex type hints:

```php
// instead of
/**
 * @var null|string|int 
 */
// do this
/**
 * @var string|int|null
 */
```

SlevomatCodingStandard.TypeHints.NullableTypeForNullDefaultValue
----------------------------------------------------------------

Sniff forces use of [nullable types](https://www.php.net/manual/en/language.types.declarations.php#language.types.declarations.nullable)
if `null` is default value for a function argument:

```php
// instead of
function doSomething(string $value = null) {}
// do this
function doSomething(?string $value = null){}
```

SlevomatCodingStandard.TypeHints.ParameterTypeHint
--------------------------------------------------

Sniff forces the following rules:

* If a parameter is declared as traversable type, there MUST be `@param` annotation describing item type:

Instead of 
```php
public function setProducts(array $products);
```

Do:
```php
/**
 * @param ProductInterface[] $products
 */
public function setProducts(array $products);
```

Please refer to [Advanced Type Hinting page](../how-to/type-hinting.md) for more details on how to type hint better.

* There MUST either `@param` annotation or native type hint for each parameter of the function:

Instead of:
```php
public function setProducts($products);
```

Do:

```php
/**
 * @param ProductInterface[] $products
 */
public function setProducts(array $products);
```

* There MUST NOT be useless suppresses of this sniff:

This `@phpcsSuppress` annotation does nothing because everything is ok with the code and sniff would not fire.

```php
/**
 * @phpcsSuppress SlevomatCodingStandard.TypeHints.ParameterTypeHint.MissingTraversableTypeHintSpecification
 * @param ProductInterface[] $products
 */
public function setProducts(array $products);
```

There are two more checks that the sniff can do, but they are disabled by default:

* It can check for useless `@param` (those that do not provide any additional information to the native type) 
  annotations and force to delete them;
* It can check for missing native type hints based on `@param` annotation.

One can enable them back by adding the following to project `phpcs.xml`:

```xml
  <!-- If sniff message is excluded, phpcs sets its severity to 0 so that we have to reset it to default 5 -->
  <rule ref="SlevomatCodingStandard.TypeHints.ParameterTypeHint.MissingNativeTypeHint">
      <severity>5</severity>
  </rule>
  <rule ref="SlevomatCodingStandard.TypeHints.ParameterTypeHint.UselessAnnotation">
      <severity>5</severity>
  </rule>
```

The sniff provides the following configuration options:

### enableObjectTypeHint ###

**default value**: true if PHP version > 7.2, false otherwise

Whether to enforce to transform `@param object` into native `object` typehint.

### enableMixedTypeHint ###

**default value**: true if PHP version > 8.0, false otherwise

Whether to enforce to transform `@param mixed` into native `mixed` typehint.

### enableUnionTypeHint ###

**default value**: true if PHP version > 8.0, false otherwise

Whether to enforce to transform `@param int|string` into native `int|string` typehint.

### traversableTypeHints ###

**default value**: [Traversable, Iterator, IteratorAggregate, Doctrine\Common\Collections\Collection, 
Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection]

Allows to specify which types are traversable and therefore must have an item type.

SlevomatCodingStandard.TypeHints.ParameterTypeHintSpacing
---------------------------------------------------------

Sniff forces the following rules about parameter type hint spacing:

* There MUST be exactly one space between type hint and a parameter name;

```php
// Instead of this
function doSomething(int$parameter) {}
// do this
function doSomething(int $parameter) {}
```

SlevomatCodingStandard.TypeHints.PropertyTypeHint
--------------------------------------------------

Sniff forces the following rules:

* If a property is declared as traversable type, there MUST be `@var` annotation describing item type:

Instead of
```php
private array $products;
```

Do:
```php
/**
 * @var ProductInterface[]
 */
private array $products;
```

Please refer to [Advanced Type Hinting page](../how-to/type-hinting.md) for more details on how to type hint better.

* There MUST either `@var` annotation or native type hint for each parameter of the function:

Instead of:
```php
private $products
```

Do:

```php
/**
 * @var ProductInterface[]
 */
private array $products;
```

* There MUST NOT be useless suppresses of this sniff:

This `@phpcsSuppress` annotation does nothing because everything is ok with the code and sniff would not fire.

```php
/**
 * @phpcsSuppress SlevomatCodingStandard.TypeHints.PropertyTypeHint.MissingTraversableTypeHintSpecification
 * @var ProductInterface[]
 */
private array $products;
```

There are two more checks that the sniff can do, but they are disabled by default:

* It can check for useless `@var` (those that do not provide any additional information to the native type)
  annotations and force to delete them;
* It can check for missing native type hints based on `@var` annotation.

One can enable them back by adding the following to project `phpcs.xml`:

```xml
  <!-- If sniff message is excluded, phpcs sets its severity to 0 so that we have to reset it to default 5 -->
  <rule ref="SlevomatCodingStandard.TypeHints.PropertyTypeHint.MissingNativeTypeHint">
      <severity>5</severity>
  </rule>
  <rule ref="SlevomatCodingStandard.TypeHints.PropertyTypeHint.UselessAnnotation">
      <severity>5</severity>
  </rule>
```

The sniff provides the following configuration options:

### enableNativeTypeHint ###

**default value**: true if PHP version >= 7.4, false otherwise

Whether to enforce to transform `@var int` into native `int` typehint.

### enableMixedTypeHint ###

**default value**: true if PHP version >= 8.0, false otherwise

Whether to enforce to transform `@var mixed` into native `mixed` typehint.
Requires `enableNativeTypeHint` to be enabled as well.

### enableUnionTypeHint ###

**default value**: true if PHP version >= 8.0, false otherwise

Whether to enforce to transform `@var int|string` into native `int|string` typehint.
Requires `enableNativeTypeHint` to be enabled as well.

### traversableTypeHints ###

**default value**: [Traversable, Iterator, IteratorAggregate, Doctrine\Common\Collections\Collection,
Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection]

Allows to specify which types are traversable and therefore must have an item type.

SlevomatCodingStandard.TypeHints.PropertyTypeHintSpacing
--------------------------------------------------------

In addition to [PSR2.Classes.PropertyDeclaration](psr.md#PSR2.Classes.PropertyDeclaration)
and [Squiz.WhiteSpace.ScopeKeywordSpacing](squiz.md#Squiz.WhiteSpace.ScopeKeywordSpacing), this sniff force the following rule:

* There MUST NOT be any whitespace between nullability symbol and property type hints.

Instead of:

```php
private ? int $propertyName;
```

Do:

```php
property ?int $propertyName;
```

SlevomatCodingStandard.TypeHints.ReturnTypeHint
-----------------------------------------------

Sniff forces the same rules as 
[`SlevomatCodingStandard.TypeHints.ParameterTypeHint`](#SlevomatCodingStandard.TypeHints.ParameterTypeHint), 
but for return type hints.

Please refer to [Advanced Type Hinting page](../how-to/type-hinting.md) for more details on how to type hint better.

SlevomatCodingStandard.TypeHints.UselessConstantTypeHint
--------------------------------------------------------

Sniff forces a developer to NOT use `@var` annotations against constants. 
The reasoning behind is... well, it's a constant, 
their type is obvious from their value and cannot be changed because of its constant nature.

```php
// Not much of a sense to have this docBlock:
/**
 * @var string 
 */
const NAME = 'value';
```

SlevomatCodingStandard.Variables.DuplicateAssignmentToVariable
--------------------------------------------------------------

Sniff forbids double assignment to the same variable:

```php
// Why do you need this?
$c = $c = 0;
// This totally legal:
$a = $b = 0;
// and this:
$c = 0;
$c = 0; // although I would question it on CR
```

SlevomatCodingStandard.Variables.UnusedVariable
-----------------------------------------------

Sniffs detects unused variables in a scope and report them:

```php
function doSomething($b, $c)
{
    $a = 0; // This variable is never used and will get reported
    
    return $b + $c;
}
```

SlevomatCodingStandard.Variables.UselessVariable
------------------------------------------------

Sniff reports useless variables:

```php
// Instead of this
function doSomething()
{
    $result = doWhatever();
    
    return $result;
}
// do this
function doSomething()
{
    return doWhatever();
}
```

Note, that assigning something to a variable before passing it to a function is _not useless_, especially with booleans:

```php
 // what exactly is true?
doSomething(true);

$dryRun = true;
doSomething($dryRun); // ah, it's whether to dry run or not!
```
