Generic sniffs
==============

This part of Vaimo Coding Standard is brought by Squizlabs and is bundled with `phpcs`.

Generic.Arrays.ArrayIndent
--------------------------

This sniff ensures that multi-line arrays are indented properly:

```php
$this->doSomething($first, [
    'one' => 1,
    'two' => 2,
    /** One can place a comment here */ 'three' => 3,
]);
```

And violation examples are:

```php
$this->doSomething($first, [
'one' => 1, // This line has incorrect indentation,
    'two' => 2,
        'three' => 3, // and this line too
    'four' => 4]); // Close brace should be on separate line 
```

Sniff can raise the following list of violations:

* **KeyIncorrect**: array key is not indented correctly;
* **CloseBraceNotNewLine**: array close brace is not on new line;
* **CloseBraceIncorrect**: array close brace is on new line, but is not indented correctly. 

All three violations are enabled in Vaimo ruleset.

Generic.Arrays.DisallowLongArraySyntax
--------------------------------------

Only short array syntax MUST be used.

PHP 7.0 introduced short array syntax: `$array = [1, 2, 3, 4];` and there is no point in using long 
syntax nowadays.

```php
$array = [
    'this',
    'is', 
    'an',
    'example',
    'of',
    'good',
    'array'
];
```

```php
$array = array(
    'this', 
    'is',
    'an',
    'example',
    'of',
    'bad',
    'one'
);
```

Sniff can raise only one violation: **Found**.

Generic.Classes.DuplicateClassName
----------------------------------

This sniff stores all checked class names and ensures that that all of them have unique name.

Unfortunately, this sniff makes sense only on "check the whole project" tasks.

Sniff can raise only one violation: **Found**.

Generic.CodeAnalysis.EmptyPHPStatement
--------------------------------------

Forbids empty PHP tags:

```php
<?php ?>
```

Generic.CodeAnalysis.ForLoopShouldBeWhileLoop
---------------------------------------------

This sniff checks if `for` expression can be simplified by `while`:

```php
for ($i = 0; $i < 10; $i++) {
    // Everything is fine
}

for (; $it->valid();) { 
    $it->next();
}
```

Second `for` can be simplified with `while` expression:

```php
while ($it->valid()) {
    $it->next();
}
```

The sniff raises only one warning: **CanSimplify**.

Generic.CodeAnalysis.UnconditionalIfStatement
---------------------------------------------

This sniffs looks into `if` condition and checks that it is neither `true` nor `false`:

```php
if (true) { // this will cause warning

} else if (false) { // and this as well 
    
} elseif (true) { // and even this!
    
}

if (file_exists(__FILE__)) { // this is fine though
    
}
```

Despite being easily tricked with more complex expression, the idea behind the sniff is
to avoid forgotten debugging or refactoring leftovers, when a developer disables or enforces 
execution route by making condition always `true` or `false`.

Generic.CodeAnalysis.UnnecessaryFinalModifier
---------------------------------------------

If the class is final itself, there is no point in making its methods final as well: 
they _are already_ final:

```php
final class Foo_Bar {
    // there is no point in having this final
    public final function fooBar() {}

    private function Bar() {}
    private final function Bard() {} // and this btw
}
```

This is what this sniff is about: it raises a warning if it encounters `final` method declaration 
inside a final class.

There is only one violation code: **Found**.

Generic.CodeAnalysis.UselessOverridingMethod
--------------------------------------------

This sniffs looks for methods that do only thing: call their `parent` counterparts with exactly 
the same parameters:

```php
class FooBar {
    public function __construct($a, $b) {
        // This will raise a warning.
        parent::__construct($a, $b); 
    }
}

class BarFoo {
    public function __construct($a, $b) {
        // This is totally fine because parent has different parameters set.
        parent::__construct($a, 'XML', $b); 
    }
}

class Foo {
    public function export($a, $b = null) {
        // This will raise a warning as well
        return parent::export($a, $b);
    }
}

class Bar {
    public function export($a, $b = null) {
        // And this is fine.
        return parent::export($a);
    }
}
```

Usually, such a function is debugging leftovers. Anyway, less code is better than more code, 
so that it's nice to remove unnecessary code. 

Generic.Commenting.Fixme
------------------------

This sniffs checks code comments and looks for "fixme" strings. The reasoning behind this is 
to prevent committing code that is known to be broken in some way.

The sniff has only one error code: **TaskFound**.

Generic.ControlStructures.InlineControlStructure
------------------------------------------------

This sniff enforces developers to follow 
[PSR-12 Control Structures](https://www.php-fig.org/psr/psr-12/#5-control-structures) guidelines, 
this part in particular:

> The body of each structure MUST be enclosed by braces. This standardizes how the structures look 
> and reduces the likelihood of introducing errors as new lines get added to the body.

Generic.Files.ByteOrderMark
---------------------------

[Byte Order Mark](https://en.wikipedia.org/wiki/Byte_order_mark) is a special unicode **invisible** 
character that can instruct reading software about order of bytes in unicode symbols.

[PSR-1 explicitly forbids using BOM](https://www.php-fig.org/psr/psr-1/#22-character-encoding) in php files:

> PHP code MUST use only UTF-8 without BOM.

This sniff checks if php file starts with this character and raises an error in this case.

The only error code: **Found**.

Generic.Files.InlineHTML
------------------------

We all usually agree that mixing up HTML and PHP code in one file is bad idea. 
The sniff check whether php file has html injections and raises an error if finds those.

The sniff is configured to ignore `*.phtml` files: HTML is kind of needed thing for templates, isn't it?

The only error code: **Found**.

Generic.Files.LineEndings
-------------------------

Different text editors in different operating systems can use several symbols for line ending: "\r", "
n", or "\n\r"; but [PSR-12 says](https://www.php-fig.org/psr/psr-12/#22-files):

> All PHP files MUST use the Unix LF (linefeed) line ending only.

This sniff makes sure that all the code has correct line ending.

The only error code: **InvalidEOLChar**.

Allowed EOL char is configured via `eolChar` configuration option, but it should be changed.

Generic.Files.LineLength
------------------------

Another rule from [PSR-12](https://www.php-fig.org/psr/psr-12/#23-lines):

> There MUST NOT be a hard limit on line length. 
> The soft limit on line length MUST be 120 characters. 
> Lines SHOULD NOT be longer than 80 characters; 
> lines longer than that SHOULD be split into multiple subsequent 
> lines of no more than 80 characters each.

So, this sniff implements this rule and raises a warning when it encounters a line 
that is longer than 120 characters.

Sniff has the following configuration options:

### lineLimit ###

**default value**: 120

If the line exceeds this limit, **TooLong** warning is thrown. Set to 0 to disable.

### absoluteLineLimit ###

**default value**: 0

If the line exceeds this limit, **MaxExceeded** error is thrown. Set to 0 to disable.
Yes, it is disabled by default, because PSR-12 explicitly forbids hard limits. 
Although, remember that if you want to proceed if only warnings are found, you'll need to tweak 
pre-commit hook.

### ignoreComments ###

**default value**: false

Whether to ignore comments line length. 

Generic.Formatting.DisallowMultipleStatements
---------------------------------------------

[PSR-12 says](https://www.php-fig.org/psr/psr-12/#23-lines):

> There MUST NOT be more than one statement per line.

This sniffs implements this rule.

There is only one error code: **SameLine**.

Generic.Formatting.SpaceAfterCast
---------------------------------

This sniff ensures that there is exactly one space between cast operator and its operand:

```php
// This is much more readable
$i = (int) $i;
// than this:
$i = (int)$i;
```

Sniff can raise **NoSpace** and **TooMuchSpace** error codes.

Generic.Functions.FunctionCallArgumentSpacing
---------------------------------------------

This sniff implements [Method and Function Arguments PSR-12 section](https://www.php-fig.org/psr/psr-12/#45-method-and-function-arguments).

It can raise the following list of error codes:

* **SpaceBeforeComma**
* **NoSpaceAfterComma**
* **TooMuchSpaceAfterComma**
* **NoSpaceBeforeEquals**
* **NoSpaceAfterEquals**

Generic.Metrics.CyclomaticComplexity
------------------------------------

[Cyclomatic complexity](https://en.wikipedia.org/wiki/Cyclomatic_complexity) is a measurement used 
to indicate complexity of a program or its piece. High complexity makes a program harder to read and increases
cognitive load on a developer.

Long story short, this sniff prevents "code roller coasters" by counting amount of control structures in a method.

If result amount is bigger exceeds soft or hard limit, warning or error is thrown.

Configuration parameters:

### complexity ###

**default value**: 10

If result amount exceeds this value, a **TooHigh** warning is thrown.

### absoluteComplexity ###

**default value**: 15

If result amount exceeds this value, "MaxExceeded" error is thrown.

Generic.Metrics.NestingLevel
----------------------------

This sniff prevents "code roller coasters" by limiting maximum level of nested control structures in a method.

```php
// what I mean by code roller coaster:
foreach ($collection as $item) {
    if ($condition) {
        // do some stuff
    } else {
        foreach ($item->getAttributes() as $attribute) {
            if ($attribute->getCode() === 'attr_code') {
                // do some other stuff
            } else {
                // do some more stuffs
            }
        }
    }
}
```

Imagine having _long_ code sections here. Roller coaster is ready for a ride! 

If maximum nesting level of a function exceeds soft limit, **TooHigh** warning is thrown. 
**MaxExceeded** error is thrown if hard limit is exceeded.

Note, that nesting level of a function itself is not counted.

### nestingLevel ###

**default value**: 5

Soft limit for nesting level.

### absoluteNestingLevel ###

**default value**: 5

Hard limit for nesting level.

Generic.NamingConventions.ConstructorName
-----------------------------------------

This sniff discourages use of PHP 4 style constructors:

```php
class Whatever
{
    public function whatever()
    {
        echo 'Yes, this is constructor';
    }
}
```

Sniff can raise the following errors:

* **OldStyle** when it finds an old-style constructor;
* **OldStyleCall** when it finds old-style parent constructor call.

This style of constructors is deprecated in PHP 7 and has been removed in PHP 8.0.

Generic.NamingConventions.UpperCaseConstantName
-----------------------------------------------

The first edition of "The C Programming language" (written in early 70s!) suggest uppercasing constants
as a naming convention. 

> Symbolic constant names are commonly written in upper case 
> so they can be readily distinguished from lower case variable names.

Moreover, [PSR-1](https://www.php-fig.org/psr/psr-1/#1-overview) requires class constants to be uppercase:

> Class constants MUST be declared in all upper case with underscore separators.

So, it is indeed old tradition, and it makes total sense to follow it. 
This sniff checks all constant names it encounters to ensure they are uppercased 
and raises **ClassConstantNotUpperCase** or **ConstantNotUpperCase** otherwise.

Generic.PHP.CharacterBeforePHPOpeningTag
----------------------------------------

[PSR-12 expilicitly defines order of block in file header](https://www.php-fig.org/psr/psr-12/#3-declare-statements-namespace-and-import-statements). 
According to the document, each php file must start with php open tag. This sniff implements this rule.

The only error code is **Found**.

Generic.PHP.DeprecatedFunctions
-------------------------------

Sniff gathers list of PHP internal functions that are marked as deprecated and raises either 
warning or error if it encounters those function calls in checked code.

### error ###

**default value**: true

If true, then error will be thrown, warning otherwise.

Generic.PHP.DisallowAlternativePHPTags
--------------------------------------

[PSR-1](https://www.php-fig.org/psr/psr-1/#21-php-tags):

> PHP code MUST use the long <?php ?> tags or the short-echo <?= ?> tags; it MUST NOT use the other tag variations.

Sniff disallows use of alternative php open tags:

* ASP-style: `<%`
* `<script language="php">`

Generic.PHP.DisallowShortOpenTag
--------------------------------

[PSR-1](https://www.php-fig.org/psr/psr-1/#21-php-tags) finally stopped discussion around short open tags in php community:

> PHP code MUST use the long <?php ?> tags or the short-echo <?= ?> tags; it MUST NOT use the other tag variations.

Sniff raises the following list of error codes:

* **Found** when short open tag is found;
* **EchoFound** when show open tag is found with echo: `<? echo $variable; ?>`;
* **PossibleFound** when possible short open tag is found in inlined HTML.

Generic.PHP.DiscourageGoto
--------------------------

Although `goto` can be actually useful in very rare scenarios, avoiding it at all cost is considered as best practice.

This sniff raises a warning when it encounters it.

The only warning code is **Found**.

Generic.PHP.ForbiddenFunctions
------------------------------

PHP standard library consists of loads of functions with weird names and non-consistent behavior.  

This sniff allows the team to raise an error when these functions are used and mention alternative in thrown error message:

```php
if (sizeof($array)) {
    // do something
}
```

Snippet above will raise an error:

```
The use of function sizeof() is forbidden; use count() instead.
```

### error ###

**default value:** true

If true, error is thrown. Warning otherwise. Be aware that by default `phpcs` return non-zero status code even if only warnings were thrown.

Generic.PHP.LowerCaseConstant
-----------------------------

Sniff makes sure that built-in php constants `true`, `false` and `null` are used lower-cased.

This rule comes from [PSR-12](https://www.php-fig.org/psr/psr-12/#25-keywords-and-types):

> All PHP [reserved keywords](https://www.php.net/manual/en/reserved.keywords.php) and [types](https://www.php.net/manual/en/reserved.other-reserved-words.php) MUST be in lower case.

Generic.PHP.LowerCaseKeyword
----------------------------

Sniff makes sure that php keywords are in lower case.

Generic.PHP.LowerCaseType
-------------------------

Sniffs make sure that php types are in lower case.

Generic.PHP.RequireStrictTypes
------------------------------

Sniff requires having a `strict_types` declaration in every PHP file. 
Formatting is controlled by a group of PSR-12 sniffs, this sniff only requires its existence.

It should look like this:

```php
<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

declare(strict_types=1);
```

Generic.PHP.SAPIUsage
---------------------

Both `php_sapi_name()` and the `PHP_SAPI` constant give the same value. 
But calling the method is less efficient than simply referencing the constant.

```php
if (php_sapi_name() === 'test') {
    // This will cause an "FunctionFound" error
}
if (PHP_SAPI === 'test') {
    // This is absolutely ok.
}
```

Generic.Strings.UnnecessaryStringConcat
---------------------------------------

Sniff looks for string literal concatenations and raises an error:

```php
$x = 'My '.'string'; // This line will raise an error
$x = 'My '. 1234; // And this won't, because one of operands is NOT a string
$x = 'My '.$y.' test'; // The same is previous, it's legit.

// Multiline concatenation can be allowed with configuration parameters
$string = 'This is a really long string. '
        . 'It is being used for errors. '
        . 'The message is not translated.';
```

### error ###

**default value:** true

Configuration parameter to determine whether to raise an error or warning.


### allowMultiline ###

**default value:** true

Whether to allow multiline string literals concatenation, since can be useful to avoid too long lines.

Generic.VersionControl.GitMergeConflict
---------------------------------------

Checks that file does not contain merge conflict leftovers:


```php
<<<<<<< HEAD
>>>>>>> master
```

Generic.WhiteSpace.DisallowTabIndent
------------------------------------

Sniff checks if Tab is used for indentation and raises an error if it finds Tab symbol.

Another holy war ended by [PSR-12 standard](https://www.php-fig.org/psr/psr-12/#24-indenting):

> Code MUST use an indent of 4 spaces for each indent level, and MUST NOT use tabs for indenting.

Generic.WhiteSpace.IncrementDecrementSpacing
--------------------------------------------

Forbids whitespace in increment/decrement operators:

```php
// Instead of
$i ++;
// Do
$i++;
```

Generic.WhiteSpace.ScopeIndent
------------------------------

Sniff checks if code is indented with proper amount of whitespace, according to [PSR-12 standard](https://www.php-fig.org/psr/psr-12/#24-indenting):

> Code MUST use an indent of 4 spaces for each indent level, and MUST NOT use tabs for indenting.

### indent ###

**default value:** 4

Correct amount of spaces to make 1 level of indentation.

### exact ###

**default value:** false

Determines whether amount of whitespace must be exactly as given in previous parameter, or can be equal or more.

### tabWidth ###

**default value:** false

Determines wheter Tab symbol should be used for indentation instead of spaces.

### ignoreIndentationTokens ###

**default value:** [T_COMMENT, T_DOC_COMMENT_OPEN_TAG]

List of code sniffer tokens to ignore indentation checks for. Default value allows arbitrary indentation in comments.
