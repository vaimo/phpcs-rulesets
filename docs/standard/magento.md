Magento Sniffs
==============

A bunch of sniffs taken from 
[Magento Coding Standard](https://devdocs.magento.com/guides/v2.4/coding-standards/code-standard-php.html).

Magento2.Classes.AbstractApi
---------------------------

Forbids use of `@api` annotations on abstract classes. 
It does not make a lot of sense to make as an api something that cannot be used directly.

Magento2.Classes.DiscouragedDependencies
---------------------------------------

Forbids depending on proxies and interceptors.

Instead of 
```php
class Example
{
    public function __construct(\Magento\Customer\Model\Session\Proxy $customerSession)
    {}
}
```

Better do

```php
class Example
{
    public function __construct(\Magento\Customer\Model\Session $customerSession)
    {}
}
```

and define proxy in di.xml:

```xml
<type name="Example">
    <arguments>
        <argument name="customerSession" xsi:type="object">Magento\Customer\Model\Session\Proxy</argument>
    </arguments>
</type>
```

Magento2.CodeAnalysis.EmptyBlock
--------------------------------

Replaces `Generic.CodeAnalysis.EmptyStatement` with the following changes:

* It supports checks for empty traits and functions;
* It allows to have empty `around*` functions.

This sniff checks for empty statements in code:

```php
foreach ($this->getCustomers() as $customer) {

}
```

Sniff can check these statements for their content: `try`, `catch`, `finally`, `do`, `if`, `else`, `elseif`, `foreach`, `for`, `switch`, and `while`.

By default, all statements except `catch` are checked. This exception is needed because
sometimes empty catch is required to suppress some errors.

In order to enable or disable any of statements, one shall use `phpcs.xml` like this:

```xml
<!-- Forbid empty statements -->
<rule ref="Magento2.CodeAnalysis.EmptyBlock">
    <!-- But allow empty catch -->
    <exclude name="Magento2.CodeAnalysis.EmptyBlock.DetectedCatch"/>
</rule>
```

By default, the sniff will NOT check for empty catches and functions.

Magento2.Exceptions.DirectThrow
-------------------------------

Forbid throwing `\Exception` and asks to use less specific exception classes. 
It is always a good idea to be as specific as possible in exceptions: it eases debugging a lot!

Instead of 

```php
if (!file_exists($filename)) {
    throw new \Exception('Error opening file: ' . $filename);
}
```

Do this:

```php
if (!file_exists($filename)) {
    throw new FileNotFoundException($filename . ' Not found');
}
```

Or event better, use named constructor for exception to hide the message itself:

```php
if (!file_exists($filename)) {
    throw FileNotFoundException::create($filename);
}
```

Magento2.Functions.DiscouragedFunction
---------------------------------------

Forbids some PHP built-in functions, providing Magento-way alternative for them whenever is possible.

Magento2.GraphQL.ValidArgumentName
----------------------------------

Sniff checks Magento GraphQL schema files forcing argument names to be camelCase.

Magento2.GraphQL.ValidEnumValue
-------------------------------

Sniff checks Magento GraphQL schema files forcing enum values to be SCREAMING_SNAKE_CASE.

Magento2.GraphQL.ValidTopLevelFieldName
---------------------------------------

Sniff checks Magento GraphQL schema files forcing top level field names to be camelCase.

Magento2.GraphQL.ValidTypeName
------------------------------

Sniff checks Magento GraphQL schema files forcing type names to be UpperCamelCase.

Magento2.Legacy.MageEntity
--------------------------

Forbids Magento 1 looking calls:

```php
Mage::model('Product'); 
```

Magento2.Methods.DeprecatedModelMethod
--------------------------------------

Forbids `getResource()->save()` chains in Model classes. 
Model class should never persist itself, use repository pattern instead.

Magento2.Namespaces.ImportsFromTestNamespace
--------------------------------------------

Forbids imports from `Magento\Tests` namespace in application scope. Do not rely on 3rd-party's tests!

Magento2.NamingConvention.InterfaceName
---------------------------------------

Requires interface names to have "Interface" suffix.

Instead of 

```php
interface Logger {}
```

Do this:

```php
interface LoggerInterface {}
```

Magento2.NamingConvention.ReservedWords
---------------------------------------

Class names and namespaces MUST not contain PHP reserved keywords.

Magento2.PHP.ShortEchoSyntax
----------------------------

Short echo syntax must be used in templates:

```php
?>
<!-- Instead of -->
<?php echo $product->getName(); ?>
<!-- Do this -->
<?= $product->getName(); ?>
```

Magento2.Performance.ForeachArrayMerge
--------------------------------------

`array_merge` is considered as resource-greedy operation and therefore should not be used in loops.

Instead of:

``` php
    $options = [];
    foreach ($configurationSources as $source) {
        // code here
        $options = array_merge($options, $source->getOptions());
    }
```

`array_merge` can be called only once:

``` php
    $options = [];
    foreach ($configurationSources as $source) {
        // code here
        $options[] = $source->getOptions();
    }

    $options = array_merge([], ...$options);
```

Magento2.SQL.RawQuery
---------------------

Forbids use of raw SQL queries. Better use search criteria and repositories. You can implement your own!

Magento2.Security.IncludeFile
-----------------------------

Forces the following rules for `include` operand:

* Including files by URL MUST NOT be used;
* String concatention and variables are not allowed in include statements.

Magento2.Security.InsecureFunction
----------------------------------

Forbids use of several PHP built-in functions in favor of better alternatives.

Magento2.Security.LanguageConstruct
-----------------------------------

Forbids use of `echo`, `print`, `exit` and `backtick` language constructs in PHP files.

Magento2.Security.Superglobal
-----------------------------

Forbids use of PHP built-in superglobal variables (`$_COOKIE`, `$GLOBALS`, etc...)

Magento2.Security.XssTemplate
-----------------------------

Finds unescaped statement by following rules: 
https://devdocs.magento.com/guides/v2.3/extension-dev-guide/xss-protection.html

Magento2.Strings.ExecutableRegEx
--------------------------------

Forbids executable regular expressions in `preg_replace`.

Magento2.Strings.StringConcat
-----------------------------

Forbids use of `+` for string concatenation. Use `.` operator instead.


Magento2.Templates.ThisInTemplate
---------------------------------

Forbids use of `$this` in templates. It's going to be deleted in the future. 
Need to load a helper? Helpers are deprecated too. Better use `ViewModel` pattern instead.
