PSR Sniffs
==========

This part of ruleset covers rules from PSR Coding Standards ([PSR-1](https://www.php-fig.org/psr/psr-1/), 
[PSR-2](https://www.php-fig.org/psr/psr-2/) and 
[PSR-12](https://www.php-fig.org/psr/psr-12/)).

PSR1.Classes.ClassDeclaration
-----------------------------

[PSR-1 Namespace and Class Names](https://www.php-fig.org/psr/psr-1/#3-namespace-and-class-names):

> Namespaces and classes MUST follow an “autoloading” PSR: [PSR-0, PSR-4].
> 
> This means each class is in a file by itself, and is in a namespace of at least one level: 
> a top-level vendor name.

PSR1.Files.SideEffects
----------------------

[PSR-1 Side Effects](https://www.php-fig.org/psr/psr-1/#23-side-effects):

> A file SHOULD declare new symbols (classes, functions, constants, etc.) 
> and cause no other side effects, or it SHOULD execute logic with side effects, but SHOULD NOT do both.
> 
> The phrase “side effects” means execution of logic not directly related to declaring classes, 
> functions, constants, etc., merely from including the file.

PSR1.Methods.CamelCapsMethodName
--------------------------------

[PSR-1 Method Names](https://www.php-fig.org/psr/psr-1/#43-methods):

> Method names MUST be declared in camelCase()

PSR12.Classes.AnonClassDeclaration
----------------------------------

Implements [PSR-12 Anonymous Classes](https://www.php-fig.org/psr/psr-12/#8-anonymous-classes) section.

PSR12.Classes.ClassInstantiation
--------------------------------

Sniff covers [PSR-12 rule about object instantiation](https://www.php-fig.org/psr/psr-12/#4-classes-properties-and-methods):

> When instantiating a new class, parentheses MUST always be present even when
> there are no arguments passed to the constructor.

PSR12.Classes.ClosingBrace
--------------------------

[PSR-12 Classes, Properties, and Methods](https://www.php-fig.org/psr/psr-12/#4-classes-properties-and-methods):

> Any closing brace MUST NOT be followed by any comment or statement on the same line.

PSR12.Classes.OpeningBraceSpace
-------------------------------

[PSR-12 Control Structures](https://www.php-fig.org/psr/psr-12)

> The body MUST be on the next line after the opening brace


PSR12.ControlStructures.BooleanOperatorPlacement
------------------------------------------------

Enforces boolean operators to be placed only at beginning of the line in multiline conditions:

```php
// Instead of

if (
    $a ||
    $b 
    || $c
) { 
} 

// Do this
if (
    $a 
    || $b
    || $c
) {
}
```

Although, since PSR-12 does not require condition position, but only consistency, it is possible to switch required position to last or just consistent.

### allowOnly ###

**default value**: first
**possible values**: first, last, `null`

If null, sniff will require consistency only, but not enforce particular placement.

PSR12.ControlStructures.ControlStructureSpacing
-----------------------------------------------

This sniff covers general style [rules for control structures](https://www.php-fig.org/psr/psr-2/#5-control-structures):

> * There MUST be one space after the control structure keyword
> * There MUST NOT be a space after the opening parenthesis
> * There MUST NOT be a space before the closing parenthesis
> * There MUST be one space between the closing parenthesis and the opening brace
> * The structure body MUST be indented once
> * The closing brace MUST be on the next line after the body

PSR12.Files.DeclareStatement
----------------------------

[PSR-12 Declare Statements](https://www.php-fig.org/psr/psr-12/#3-declare-statements-namespace-and-import-statements)

> Declare statements MUST contain no spaces and MUST be exactly `declare(strict_types=1)` (with an optional semi-colon terminator).

PSR12.Files.FileHeader
----------------------

Forces order of file header blocks, according to [PSR-12](https://www.php-fig.org/psr/psr-12/#3-declare-statements-namespace-and-import-statements).

PSR12.Files.ImportStatement
---------------------------

Forces order of import statements in a file header, according [PSR-12](https://www.php-fig.org/psr/psr-12/#3-declare-statements-namespace-and-import-statements).

PSR12.Files.OpenTag
-------------------

> Opening PHP tag must be on a line by itself

PSR12.Functions.NullableTypeDeclaration
---------------------------------------

[PSR-12 Method arguments](https://www.php-fig.org/psr/psr-12/#45-method-and-function-arguments):

> In nullable type declarations, there MUST NOT be a space between the question mark and the type.

PSR12.Functions.ReturnTypeDeclaration
-------------------------------------

[PSR-12 Method arguments](https://www.php-fig.org/psr/psr-12/#45-method-and-function-arguments):

> When you have a return type declaration present,
> there MUST be one space after the colon followed by the type declaration.
> The colon and declaration MUST be on the same line as the argument list
> closing parenthesis with no spaces between the two characters.

PSR12.Namespaces.CompoundNamespaceDepth
---------------------------------------

Sniff covers[max compound namespace depth](https://www.php-fig.org/psr/psr-12/#3-declare-statements-namespace-and-import-statements) rule:

> Compound namespaces with a depth of more than two MUST NOT be used.

PSR12.Operators.OperatorSpacing
-------------------------------

Forces the following rules from PSR-12 Section 6: Operators.

> All binary arithmetic, comparison, assignment, bitwise, logical, string, and type operators MUST be preceded and followed by at least one space.

> The conditional operator, also known simply as the ternary operator, MUST be preceded and followed by at least one space around both the ? and : characters.
> When the middle operand of the conditional operator is omitted, the operator MUST follow the same style rules as other binary comparison operators.

PSR12.Traits.UseDeclaration
---------------------------

Forbids multiple trait uses per `use`:

```
class Example 
{
    use LoggableTrait,
        AnotherTrait;
} 
```

[PSR-12 Using traits](https://www.php-fig.org/psr/psr-12/#42-using-traits):

> Each individual trait that is imported into a class MUST be included one-per-line
> and each inclusion MUST have its own use import statement.

PSR2.Classes.ClassDeclaration
-----------------------------

Sniff forces [PSR-2 classes declaration rules](https://www.php-fig.org/psr/psr-2/#4-classes-properties-and-methods).

PSR2.Classes.PropertyDeclaration
--------------------------------

Sniff forces [PSR-2 property declaration rules](https://www.php-fig.org/psr/psr-2/#42-properties).

PSR2.ControlStructures.ElseIfDeclaration
----------------------------------------

[PSR-2](https://www.php-fig.org/psr/psr-2/#51-if-elseif-else) discourages use of `else if` in favor of `elseif`:

> The keyword elseif SHOULD be used instead of else if so that all control keywords look like single words.

PSR2.ControlStructures.SwitchDeclaration
----------------------------------------

This sniff covers [switch declaration style rules](https://www.php-fig.org/psr/psr-2/#52-switch-case):

> A switch structure looks like the following. Note the placement of parentheses, spaces, and braces.
> The case statement MUST be indented once from switch, and the break keyword 
> (or other terminating keyword) MUST be indented at the same level as the case body. 
> There MUST be a comment such as // no break when fall-through is intentional in a non-empty case body.

Note, that some rules are covered by [`Generic.PHP.LowerCaseKeyword`](generic.md#Generic.PHP.LowerCaseType) 
and therefore excluded from this sniff.

PSR2.Files.ClosingTag
---------------------

[PSR-2 Files](https://www.php-fig.org/psr/psr-2/#22-files):

> The closing `?>` tag MUST be omitted from files containing only PHP.

PSR2.Files.EndFileNewline
-------------------------

[PSR-2 Files](https://www.php-fig.org/psr/psr-2/#22-files):

> All PHP files MUST end with a single blank line.

PSR2.Methods.FunctionCallSignature
----------------------------------

Sniff covers [PSR-2 Method and Function Calls](https://www.php-fig.org/psr/psr-2/#46-method-and-function-calls):

> When making a method or function call, 
> there MUST NOT be a space between the method or function name and the opening parenthesis, 
> there MUST NOT be a space after the opening parenthesis, 
> and there MUST NOT be a space before the closing parenthesis. 
> In the argument list, there MUST NOT be a space before each comma, 
> and there MUST be one space after each comma.

PSR2.Methods.FunctionClosingBrace
---------------------------------

Sniff covers on rule from [PSR-2 Methods](https://www.php-fig.org/psr/psr-2/#43-methods):

> Closing brace MUST go on the next line following the body.

PSR2.Methods.MethodDeclaration
------------------------------

Sniff covers [PSR-2 Methods](https://www.php-fig.org/psr/psr-2/#43-methods) rules.

The rule about names starting from `_` is disabled by default, because it's hard to follow in Magento.
