PEAR Sniffs
===========

Ages ago, PEAR was the main coding standard in PHP worlds, nowadays only two rules of it are still relevant.

PEAR.Commenting.InlineComment
-----------------------------

Having one and only one way of doing things is just a common sense, 
but PHP allows many ways to comment something so that PEAR team decided to discourage `# comments`.

```php
# This is not the way we should comment something
// This is the way.
```

PEAR.Functions.ValidDefaultValue
--------------------------------

This sniff forces [PSR-12 rule](https://www.php-fig.org/psr/psr-12/#45-method-and-function-arguments)
that arguments with default values must be at the end.
