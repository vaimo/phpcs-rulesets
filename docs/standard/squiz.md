Squiz Sniffs
============

Squizlabs is a company that ships `phpcs`. Code sniffer is bundled with a bunch of sniffs as well
that are divided in several groups. `Squiz` is one of them.

Squiz.Arrays.ArrayBracketSpacing
--------------------------------

Sniff checks for array square brackets to be properly spaced:

* There MUST NOT be whitespace around opening square bracket (`SpaceBeforeBracket`);
* There MUST NOT be whitespace before closing square bracket (`SpaceAfterBracket`).

Note, we are not talking about array declaration, but usage:

```php
// Spacing around these brackets will trigger the error
$myArray  [  'key'  ] = $value;
// This is totally fine!
$array = [
    'foo' => 'bar',
    'bar' => 'foo',
];
// And this is not fine
echo foo()[ 1 ];
```

Squiz.Arrays.ArrayDeclaration
-----------------------------

Sniff checks for some common sense rules regarding array declaration formatting:

* There MUST NOT be whitespace after `array` keyword (`SpaceAfterKeyword`): 
  
```php
$test = array (1, 2); // uh oh
$test = array(1, 2); // all good!
```
  
* There MUST NOT be whitespace between brackets in empty array (`SpaceInEmptyArray`):
  
```php
$test = [ ]; // uh oh
$test = []; // all good!
```
  
* There MUST NOT be whitespace between last comma and closing bracket in single-line array (`CommaAfterLast`):
  
```php
$test = [1, 2, ]; // uh oh
$test = [1, 2,]; // all good!
```
  
* There MUST be exactly 1 space before double arrow (`NoSpaceBeforeDoubleArrow` and `SpaceBeforeDoubleArrow`):
  
```php
$test = ['key'=> 'value', 'key2'  => 'value']; // uh oh
$test = ['key' => 'value', 'key2' => 'value']; // all good!
```
  
* There MUST be exactly 1 space after double arrow (`NoSpaceAfterDoubleArrow` and `SpaceAfterDoubleArrow`):
  
```php
$test = ['key' =>'value', 'key2' =>  'value']; // uh oh
$test = ['key' => 'value', 'key2' => 'value']; // all good!
```
  
* There MUST be exactly 1 space after comma in single-line arrays (`NoSpaceAfterComma`, `SpaceAfterComma`):
  
```php
$test = ['key' => 'value','key2' => 'value',  'key3' => 'value']; // uh oh
$test = ['key' => 'value', 'key2' => 'value', 'key3' => 'value']; // all good!
```
  
* There MUST NOT be space before a comma (`SpaceBeforeComma`):
  
```php
$test = ['key' => 'value' , 'key2' => 'value']; // uh oh
$test = ['key' => 'value', 'key2' => 'value']; // all good!
```
  
* Closing parenthesis of multiline array declaration MUST be on a new line (`CloseBraceNewLine`):
  
```php
$test = [
  'key' => 'value']; // uh oh

$test = [
  'key' => 'value',
]; // all good!
```
  
* Array MUST be either list of hashmap, not a combination of them (`NoKeySpecified`, `KeySpecified`).
  It means that we must make sure that every array item must have a key (making something like a hashmap), 
  or no array item has a key (making a list).
  
```php
$combination = [1, 2, 'key' => 'value', 'key2' => 'value']; // uh oh
$list = [1, 2]; // all good!
$hash = ['key' => 'value', 'key2' => 'value']; // all good!
```
  
* Each array item in multi-line array MUST be on new line (`ValueNoNewline`, `IndexNoNewline`, `FirstValueNoNewline`, `FirstIndexNoNewline`):
  
```php
$array = [
  'key' => 'value', 'key2' => 'value',
]; // uh oh

$array = [
  'key' => 'value',
  'key2' => 'value',
]; // all good!
``` 
  
As you can see, despite _looking_ scary, most of checks just ensure common sense practices 
to make arrays more readable.

Sniff actually offers more checks, but the rest of them are disabled because 
either there are better implementations out there, or they are too Squizlabs-specific.

Squiz.Classes.ClassFileName
---------------------------

Sniff checks if class name equals file name and throws `NoMatch` error if not.

Squiz.Classes.SelfMemberReference
---------------------------------

This sniff checks `self` references in class code and enforces some rules:

* There MUST NOT be whitespace before and after semicolon in self-references:
  
```
// Incorrect spacing.
self ::selfMemberReferenceUnitTestFunction();
self::  selfMemberReferenceUnitTestFunction();
self  :: selfMemberReferenceUnitTestFunction();
```

* Code MUST self-refer using either `self` or `static`, but not classname:
  
```
class Example 
{
    public function test() {
      Example::doSomething(); // this is an error. 
    }
}
```

Sniff is capable of checking case for `self` and `static`, but this functionality is covered by Generic.PHP.LowerCaseKeyword
and therefore is disabled.

Squiz.Classes.ValidClassName
----------------------------

Sniff enforces [PSR-1 rule about class naming](https://www.php-fig.org/psr/psr-12/#21-basic-coding-standard):

> Class names MUST be declared in StudlyCaps.

Squiz.Commenting.DocCommentAlignment
------------------------------------

Sniff checks docBlock sections and looks for asterisk spacing issues:

* Asterisks MUST be aligned:
  
```
/**
 * Class Example
*  This line will cause an error
 */
```

* There MUST at least one space after an asterisk:

```
/**
 * Class Example
 *asdfkasdf // This line will cause an error
 */
```

Sniff can enforce having exactly one space after an asterisk, but it seems to be quite useful for annotation indentations.

Squiz.Commenting.EmptyCatchComment
----------------------------------

This sniff enforces the following rule:

> Empty catch statement must have a comment to explain why the exception is not handled.

The reasoning is to enable developers to easily distinguish between "someone forgot to implement catch" and
"this exception is intentionally suppressed because of ...".

Good example of intended empty catch is 
[WhatFailureGroupHandler](https://github.com/Seldaek/monolog/blob/main/src/Monolog/Handler/WhatFailureGroupHandler.php) from Monolog.
It explicitly is designed to suppress any exceptions during logging.

Squiz.Commenting.FunctionComment
--------------------------------

This sniff performs several sanity-checks agains function doc blocks:

* There MUST NOT be more than one return tag:
  
```
/**
 * @return void
 * @return mixed
 */
```

* If there is `@return` tag, it MUST contain return type:

```
/**
 * @return
 */
```

* `@throws` tag MUST contain exception type:

```
/**
 * @throws
 */
```
  
* `@param` tag MUST contain both parameter name and type:

```
/**
 * @param string
 * @param 
 * @param $string
```
  
* `@param` tag MUST have the same parameter name as function definition:

```
/**
 * @param string $t
 */
public function doSomething(string $T) {}
```
  
* There MUST NOT be orphan `@param` tags, e.g. tag exists, but there is no such parameter:

```
/**
 * @param string $;
 */
public function doSomething() {}
```

Squiz.ControlStructures.ControlSignature
----------------------------------------

Enforces [PSR-12 Section 5](https://www.php-fig.org/psr/psr-12/#5-control-structures):

> The general style rules for control structures are as follows:
> 
> * There MUST be one space after the control structure keyword
> * There MUST NOT be a space after the opening parenthesis
> * There MUST NOT be a space before the closing parenthesis
> * There MUST be one space between the closing parenthesis and the opening brace
> * The structure body MUST be indented once
> * The body MUST be on the next line after the opening brace
> * The closing brace MUST be on the next line after the body
> 
> The body of each structure MUST be enclosed by braces. 
> This standardizes how the structures look and reduces the likelihood of introducing errors 
> as new lines get added to the body.

Squiz.ControlStructures.ForEachLoopDeclaration
----------------------------------------------

Enforces [PSR-12 Section 5.5](https://www.php-fig.org/psr/psr-12/#55-foreach)

Squiz.ControlStructures.ForLoopDeclaration
------------------------------------------

Enforces [PSR-12 Section 5.4](https://www.php-fig.org/psr/psr-12/#54-for)

Squiz.ControlStructures.InlineIfDeclaration
-------------------------------------------

Sniff checks spacing in ternary operator declaration:

* If `?:` version is used there MUST NOT be a space between `?` and `:`:

```
$result = $comparison ? : $else; // is `$then` missing?
$result = $comparison ?: $else; // Looks like [elvis operator](https://en.wikipedia.org/wiki/Elvis_operator), I know it!
```

Squiz.Functions.FunctionDeclaration
-----------------------------------

Enforces [PSR-2 Section 4.3](https://www.php-fig.org/psr/psr-2/#43-methods) (part about spacing): 

> Method names MUST NOT be declared with a space after the method name. 
> The opening brace MUST go on its own line, and the closing brace MUST go on the next line following the body.
> There MUST NOT be a space after the opening parenthesis, 
> and there MUST NOT be a space before the closing parenthesis.

Squiz.Functions.FunctionDeclarationArgumentSpacing
--------------------------------------------------

Enforces [PSR-2 Section 4.4](https://www.php-fig.org/psr/psr-2/#44-method-arguments) part about spacing:

> In the argument list, there MUST NOT be a space before each comma, 
> and there MUST be one space after each comma.

Squiz.Functions.GlobalFunction
------------------------------

Sniff looks for functions declared in the global namespace and suggests moving them to a static class.

Squiz.Functions.MultiLineFunctionDeclaration
--------------------------------------------

Enforces [PSR-12 Section 4.5](https://www.php-fig.org/psr/psr-12/#45-method-and-function-arguments) for multiline arguments:

> Argument lists MAY be split across multiple lines, where each subsequent line is indented once. 
> When doing so, the first item in the list MUST be on the next line, 
> and there MUST be only one argument per line.
> 
> When the argument list is split across multiple lines, 
> the closing parenthesis and opening brace MUST be placed together on their own line 
> with one space between them.

Squiz.NamingConventions.ValidFunctionName
-----------------------------------------

Only PHP magic methods should be prefixed with a double underscore:

```
public function __init() // no-no 
{
}

public function __sleep() // yes, it is php built-in
{
}
```

Squiz.Operators.ValidLogicalOperators
-------------------------------------

Forbids using `AND` and `OR` instead of `&&` and `||`. 
The reasoning is they have different precedence order and therefore can lead to hard-to-catch bugs.

```
  $something = $this or $that; // no-no
  $something = $this || $that; // all good!
```
 
Squiz.PHP.CommentedOutCode
--------------------------

Looks for commented out code and reports it as an error:

```
// $test = $this || that;
// Why would have commented out code in your app?
// If it is needed, uncomment it.
// If not, just delete.
```

Squiz.PHP.Eval
--------------

Discourages use of `eval`. It is unsafe and slow.

```
eval($_GET['command']); // A little exaggerated example of DO NOT.
```

Squiz.PHP.GlobalKeyword
-----------------------

Discourages use of `global` keyword. One should use `$GLOBALS` array or (better) do not do this at all:

```
global $variable; // when and where is defined?
```

Squiz.PHP.InnerFunctions
------------------------

Forbids functions inside functions. If you need something like this, consider anonymous functions:

```
function doStuffs($array) {
  function doStuffWithItem($item) {}
  
  return array_map($array, 'doStuffWithItem'); 
}

function doStuffs($array) {
  return array_map(
    function($item) {
    
    }, 
    $array
  );
}
```

Squiz.PHP.LowercasePHPFunctions
-------------------------------

Forces all PHP built-in function to be called in lower case:

```
EXPLODE(', ', $array); // why would you want to yell at me, code?
explode(', ', $array); // This is much better 
```

Squiz.PHP.NonExecutableCode
---------------------------

Looks for code place after `return`, `break`, and `continue` statements:

```
function doSomething() {
  return null;
  
  $sadCode = thatNeverGetExecuted();
}
```

It is usually a sign of forgotten debug leftovers. 

Squiz.Scope.MethodScope
-----------------------

Implements [PSR-12 Section 4.4](https://www.php-fig.org/psr/psr-12/#44-methods-and-functions) part about visibility:

> Visibility MUST be declared on all methods.

Squiz.Scope.StaticThisUsage
---------------------------

Forbids use of `$this` in static methods. It _might_ work in some cases 
(static function is called in object context, or it is bound to a context), but it can cause runtime errors.

Better make these functions non-static explicitly.

```
  class Example 
  {
    private $something; 
    
    public static function getSomething() 
    {
      return $this->something; // will throw an error if called `Example::getSomething()`
    }
  }
```

Squiz.Strings.ConcatenationSpacing
----------------------------------

Forces whitespace around concatenation operator (new lines are ignored):

```
$this.$is.$hard.$to.$read;
$this . $is . $much . $simpler;
```

Squiz.Strings.DoubleQuoteUsage
------------------------------

Sniffs force two additional rules for double quote strings (`"this"` instead of `'that'`):

* Double-quote string literals MUST NOT contain variables, string concatenation should be used instead.
  
```
$message = "Value is invalid, $expected expected, but $actual given"; // It works, but hard to follow
$message = sprintf('Value is invalid, %s expected, but %s given', $expected, $actual); // variable inserts are explicit.
```  

* Double-quote literal MUST be used ONLY in case they contain special characters:

```
return "Text"; // better use single quotes
return "Text\n"; // is ok
```

Squiz.WhiteSpace.CastSpacing
----------------------------

Cast statements must not contain whitespace inside brackets:

```
(int ) $something; // error
(int) $something; // all good!
```

Squiz.WhiteSpace.ControlStructureSpacing
----------------------------------------

Implements [PSR-12 Section 5](https://www.php-fig.org/psr/psr-12/#5-control-structures) part about spacing.

Squiz.WhiteSpace.FunctionOpeningBraceSpace
------------------------------------------

There MUST NOT be an empty line after function opening brace:

```
function doSomething() 
{

  // Why would you need that line above?
  $do = $something + $else;
}
```

Squiz.WhiteSpace.FunctionSpacing
--------------------------------

Sniff checks for amount of empty space between functions. 
Default configuration forces having exactly 1 line between functions and having 0 space before the first and after the last function. 

Instead of
```php
class Example 
{


    public function doSomething()
    {
    }
    public function doSomethingElse()
    {
    }
    
}
```

Do
```php
class Example
{
    public function doSomething()
    {
    }
    
    public function doSomethingElse()
    {
    }
}
```

### spacing ###

**default value**: 1

Defines amount of empty line between functions.

### spacingBeforeFirst ###

**default value**: 0

Defines amount of empty lines before the first function in a class.

### spacingAfterLast ###

**default value**: 0

Defines amount of empty lines after the last function in a class.

Squiz.WhiteSpace.LanguageConstructSpacing
-----------------------------------------

Sniff requires whitespace after language constructs:

```
echo$variable; // not readable
echo $variable; // nice and clean
```

Squiz.WhiteSpace.LogicalOperatorSpacing
---------------------------------------

Sniff requires whitespace around logical operators

```
$this || $that; // this is much better then
$this||$that;
```

Squiz.WhiteSpace.ObjectOperatorSpacing
--------------------------------------

Sniff forbids spaces around `->` operator, but allows them in case of multiline constructions:

```
$this -> doSomething(); // no-no
$this->doSomething(); // all good

$this->select()
     ->from()
     ->where(); // This is fine too!
```

Squiz.WhiteSpace.ScopeClosingBrace
----------------------------------

Implements [PSR-12 Section 5](https://www.php-fig.org/psr/psr-12/#5-control-structures) in part:

> The closing brace MUST be on the next line after the body

Squiz.WhiteSpace.ScopeKeywordSpacing
------------------------------------

Enforces single whitespace after scope keyword:

```
private$variable; // pretty hard to read
private $variable; // easy
```

Squiz.WhiteSpace.SemicolonSpacing
---------------------------------

Forbids space before semicolon:

```
$t = youNeverExpectIt() ; // why would you need it?
```

Squiz.WhiteSpace.SuperfluousWhitespace
--------------------------------------

Implements [PSR-12 Section 2.3](https://www.php-fig.org/psr/psr-12/#23-lines) in part:

> There MUST NOT be trailing whitespace at the end of lines.
