Vaimo Sniffs
============

This part of Vaimo Coding Standard is entirely developed at Vaimo. 
Some sniffs reimplement Magento or Slevomat sniffs to provide better documentation or configuration.


Vaimo.Classes.ConstantDeclarationOrder
--------------------------------------

The Sniff makes sure that all class constants are put in the very beginning of the class body: 
before any other members.

The reasoning behind this sniff is pretty simple: 
it is very easy to lose a constant definition in a class, 
and it just looks more natural to start with constants.

Sniff has no configuration properties.

Vaimo.Classes.ObjectInstantiation
---------------------------------

Reimplementation of MEQP2.Classes.ObjectInstantiation. 

Reasoning behind the Sniff is the way how Magento works. 
If you instantiate a class using `new` keyword instead of direct or indirect use of ObjectManager, 
you lose all the plugins and possible proxy that Magento DI may have applied to the class.

This implementation provides more flexible configuration to the sniff 
in order to work around possible edge cases:

### allowInstantiateFinalClasses ###

**default value:** true.

Since Magento cannot apply plugins or proxy to final class (because it is final 
and cannot be extended by generated code), it is safe to allow their direct instantiation.

### allowAnonymousClasses ###

**default value:** true

Again, Magento cannot apply a plugin to anonymous class (because you cannot define a plugin 
for anonymous class), it is safe to allow their direct instantiation.

Note, anonymous classes should be used with caution: 
it's very easy to create unreadable code if over-use them.

### allowNamedConstructors ###

**default value:** true

Named constructor is a special case of factory method, when this method 
is stored inside the class itself. It is very often used in exceptions like this:

```php
use Magento\Framework\Exception\LocalizedException;

class IllegalStatusTransitionException extends LocalizedException
{
    public static function create(string $oldStatus, string $newStatus): self
    {
        return new self(__(
            'It is not possible to switch customer status from "%old_status" to "%new_status"',
            [
                'old_status' => $oldStatus,
                'new_status' => $newStatus,
            ]
        ));
    }
}
```

### allowedClasses ###

**default value:** 
[DateTime, DateTimeImmutable, DateTimeZone, DateInterval, Zend_Db_Expr, DOMDocument]

Some classes are just too simple to have a plugin for so that 
it is safe to say one can instantiate them directly. 

Vaimo.Classes.UndefinedProperty
-------------------------------

This Sniff looks for properties that are used, but not declared in a class. 
Undeclared property call is almost certainly a bug.

The sniff is smart enough to consider inherited properties as declared. 
Another thing to mention, the sniff checks heredoc variables as well.

### skipOnReflectionError ###

**default value:** false

Whether to skip the check if sniff is not able to collect inherited properties 
because of reflection errors. If this option is enabled, sniff will throw exception 
and give up the file check.

Vaimo.Classes.PropertyUnderscore
--------------------------------

Sniff reimplements PSR2.Classes.PropertyDeclaration.Underscore 
adding an option to allow inherited properties to start from underscore.

### allowInherited ###

**default value:** true

Whether to allow inherited property names to start from underscore.

### skipOnReflectionError ###

**default value:** false

Whether to skip the check if sniff is not able to collect inherited properties
because of reflection errors. If this option is enabled, sniff will throw exception
and give up the file check.

Vaimo.Classes.ProtectedMember
-----------------------------

Sniff reimplements MEQP2.PHP.ProtectedClassMember that forbids use of protected members in classes.

It sounds reasonable indeed, but not always possible to achieve 
(think abstract classes with abstract protected parts and Magento itself).


The reasoning behind reimplementation is to provide more flexible configuration, 
which will help lowering amount of ignores in situations when you have to use protected members:

### allowAbstract ###

**default value**: true

Whether to allow `abstract protected function`.

### allowInherited ###

**default value**: true

Whether to allow redefining inherited `protected` members (both functions and properties)

### allowProtectedPropertyInAbstractClass ###

**default value**: false

Whether to allow declaring protected properties in abstract classes. 
It is disabled by default, because in this case use of `abstract protected function` 
is preferred to explicitly mark the member as abstract. 
Remember, there are no `abstract protected` properties in PHP.

### allowProtectedFunctionInAbstractClass ###

**default value**: true

Whether to allow declaring protected functions in abstract classes.

### considerInheritedOnReflectionError ###

**default value**: false

Whether to consider member always inherited if reflection error happens during the check 
(parent class is not autoloadable for some reason). Despite being possible to raise false-positive, 
this feature is disabled because it's preferred to have properly working build script
(that runs `composer install`) and all dependencies properly declared in composer.json.

Vaimo.CodeAnalysis.Copyright
----------------------------

Sniff checks that php file starts with Vaimo copyright disclaimer in given pattern enforcing the following rules:

* Copyright notice MUST be file-level dock block and MUST be placed right after php open tag;
* Copyright notice content MUST look like this:

```php
<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

namespace Vaimo\WhateverModule;
```

### pattern ###

**default value**: `'/^\* Copyright (?i:\x{00A9}|\(c\)) Vaimo Group\. All rights reserved\.\n \* See LICENSE_VAIMO\.txt for license details\.\n \*\//um'`

Pattern to compare copyright with. Comparison is performed using `preg_match` function.

Be aware that sniff uses the pattern again result of `\SlevomatCodingStandard\Helpers\DocCommentHelper::getDocComment` call
and it returns doc comment without opening and closing line, like this:

```
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
```

### example ###

**default value**:

```
/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */
```

Example to show if incorrect copyright is found.

Vaimo.CodeAnalysis.UnusedFunctionParameter
------------------------------------------

This sniff is a reimplementation of Generic.CodeAnalysis.UnusedFunctionParameter 
(which is bundled with `phpcs`).

The reasoning behind the sniff is easy: why would you bother with declaring parameter you never use?
Less parameters means less maintenance and possible BC breaks in the future.

Reimplementation provides more flexibility in configuration to achieve less inevitable ignores:

### allowedMethodPrefixes ###

**default value:** ['before', 'around', 'after']

This parameter allows ignoring unused function parameters 
if method name starts with one of given prefixes. 
Default configuration allows them in plugin methods.

### allowMethodParameters ###

**default value:** ['$subject', '$proceed']

This parameter allows ignoring unused function parameters with certain names.

### allowUnusedInheritedParameters ###

**default value:** true

Another source of false-positive alerts from this sniff is an interface 
that declares more parameters than some particular implementation actually needs. 
In order to overcome this issue, this parameter allows to ignore all parameters 
if the function declaration is inherited.

### considerInheritedOnReflectionError ###

**default value**: false

Whether to consider parameter always inherited if reflection error happens during the check
(parent class is not autoloadable for some reason). Despite being possible to raise false-positive,
this feature is disabled because it's preferred to have properly working build script
(that runs `composer install`) and all dependencies properly declared in composer.json.

Vaimo.Comments.DeprecationNotice
--------------------------------

Sniff forces the following rules about deprecation notices:

* There MUST be `@deprecated` annotation comment:

```php
// Instead of:
/**
 * @deprecated  
 */
// Do this:
/**
 * @deprecated use Example::getReplacementMethod() instead. To be removed in 3.0 
 * @see Example::getReplacementMethod()
 */
```

It makes easier to find non-deprecated alternative for the thing a developer might need.

* If class or function is deprecated, it MUST trigger silenced `E_USER_DEPRECATED` error:

```php
class Example
{
    /**
     * @deprecated use Example::getSomething() instead. To be removed in 3.0
     * @see Example::getSomething() 
     */
    public function doSomthing()
    {
        @trigger_error(
            'Example::doSomthing() is deprecated. Use Example::getSomething() instead.', 
            E_USER_DEPRECATED
        );
        
        $this->doSomething();
    }
    
    public function doSomething() 
    {
        // ...
    }
}
```

Why? It makes deprecation notices much more visible in good development environment: phpstorm shows 
these errors on console tab during debugging, [Vaimo Debug Toolbar](https://bitbucket.org/vaimo/module-debug-toolbar)
has a tab for these errors.

Make sure to have `@` error silencer before `trigger_error` to not bloat server logs. 

Vaimo.ControlStructures.NestedIf
--------------------------------

The sniff checks if there are any nested if statements inside `if`, `else` or `elseif` blocks. 
The reasoning behind this sniff is readability. Just compare these two pieces of code:

```php
if ($variable) {
  if ($variable === true) {
    $this->doSomething();
  }
  else {
    $this->doSomethingElse();
  }
} else {
  $this->falseyThing();
}
```

```php
if ($variable === true) {
    $this->doSomething();
    return;
}

if ($variable) {
    $this->doSomethingElse();
    return;
}

$this->falseyThing();
```

In our opinion, second example creates less cognitive load, 
especially if amount of expressions in all clause blocks is more than just one line.

This sniff is disabled for templates.

### maxAllowedNesting ###

**default value**: 0

By default, no nesting of conditionals is allowed, but the value is configurable.

Vaimo.ControlStructures.TernaryOperator
---------------------------------------

Ternary operator is an amazing tool to make your code shorter and more expressive. Although, overuse of it makes it harder to read.

Moreover, PHP has very hard to remember operator precedence order, which makes it even easier to shoot yourself in the foot by using complicated ternary expressions.

So, the sniff tries to setup a few rules on how to use ternary operators.

### Quick reminder

Ternary operator is a syntax sugar for creating if-then-else one-liners and consists of three parts 
that can be generalised in the following way:

```php
CONDITION ? THEN : ELSE;
```

There is one special form of ternary operator when `THEN` part is missing, 
it is called [Elvis operator](https://en.wikipedia.org/wiki/Elvis_operator).

```php
CONDITION ?: ELSE
```

### Nested ternary operators are not allowed

Whenever sniff finds ternary operator inside another ternary operator 
it fires `Vaimo.ControlStructures.TernaryOperator.Nested` error.

```php
return $this->isConditionMet()
    ? $this->something ? $this->doThis() : $this->doThat()
    : null;
```

### Use of ternary operators in if conditions is not allowed

```php
if ($this->lastProgressDisplayedTimestamp
    && $currentTimestamp - $this->lastProgressDisplayedTimestamp < ($remainingTime > 30 ? 3 : 1)) {
    return;
}
```

Although, in this particular example ternary operator itself is not the scariest part, 
using ternary operator in `if` condition either makes it hard to read or masks 
[nested if](https://bitbucket.org/vaimo/phpcs-rulesets/src/master/docs/standard/vaimo.md#markdown-header-vaimocontrolstructuresnestedif).

This rule is covered by `Vaimo.ControlStructures.TernaryOperator.FoundInCondition`

### Multiline expressions are not allowed in any part of ternary expression

Having multiline expression in ternary operator makes it really hard to find out what happens, 
and it is usually a sign of hard to read code. Even if it *looks* like a better solution right now, most probably, 
it will become unreadable in two weeks when you forget what it does.

```php
return $this->storeConfig->isWorkwearStore()
    ? ''
    : $this->getUrl(
        '',
        [
            '_secure' => true,
            '_direct' => 'store-finder',
            '_type' => UrlInterface::URL_TYPE_DIRECT_LINK,
        ]
    ) . '/';
```

This particular example I would rather refactor into something like this:

```php
// Replace the whole need of ternary operator with early exit.
if ($this->storeConfig->isWorkwearStore()) {
	return '';
}

// Make this call a separate expression, because it deserves it.
$url = $this->getUrl('',[
    '_secure' => true,
    '_direct' => 'store-finder',
    '_type' => UrlInterface::URL_TYPE_DIRECT_LINK,
]);

return $url . '/';
```

Another possible way of refactoring is extracting the whole `ELSE` part into a separate function:

```php
return $this->storeConfig->isWorkwearStore() ? '' : $this->composeStoreFinderUrl();
```

Vaimo.ControlStructures.TernaryOperatorPlacement
------------------------------------------------

This sniff checks that placement of ternary operator in multiline expressions is consistent.

Instead of 

```php
$baseDiscountTaxCompensationAmount = $this->globalStore ?
    $orderItem->getBaseDiscountTaxCompensationAmount()
    : $orderItem->getDiscountTaxCompensationAmount();
```

One should put both `?` and `:` either at the very beginning of the line or as last thing on previous one,
and use one style of placing them throughout the project.

### allowOnly ###

*default value*: first

By default, `first` placement is allowed, thus requires putting operators at the beginning of the line,
but this behaviour is configurable. Use `last` to switch to operators at the end of line.

Vaimo.NamingConventions.IllegalWordInClassName
----------------------------------------------

This sniff checks if declared class name contains words that should be avoided. 
For example, we know that words like "And" or "Or" in a class name can be a signal of SRP violation.

So, in order to prohibit use of these words in class names, we can configure this sniff to raise 
an error if these words are encountered in a class name.

### illegalKeywords ###

**default value:** []

This configuration option represents the list of keywords to alarm on. 
It is empty by default, so that every team can decide what keywords are "illegal" in their project.
Note, that keywords are lowercased before comparison.

Vaimo.NamingConventions.AndOrInClassName
----------------------------------------

This sniff extends IllegalWordInClassName just to have more precise error message and allow teams
to redefine list of "illegal" keywords easier.

Vaimo.NamingConventions.IllegalWordInMethodName
-----------------------------------------------

This sniff checks if declared method name contains words that should be avoided 
are signal of best practices violation.

### illegalKeywords ###

**default value:** []

This configuration option represents the list of keywords to alarm on.
It is empty by default, so that every team can decide what keywords are "illegal" in their project.
Note, that keywords are lowercased before comparison.

### allowInheritedMethods ###

**default value:** true

Whether to skip this check for methods that are declared in inherited class or interface.

### considerInheritedOnReflectionError ###

**default value:** false

It is possible that reflection error occurs in process of this sniff work. 
This configuration parameter defines what to do if it happens: either consider function 
as inherited despite error or allow some false-positives.

Vaimo.NamingConventions.AndOrInMethodName
-----------------------------------------

This sniff extends IllegalWordInMethodName just to have more precise error message and allow team 
to use original sniff for their own rules.

Vaimo.NamingConventions.IllegalWordInVariableName
-------------------------------------------------

This sniff checks if property or variable name contains words that should be avoided 
are signal of best practices violation.

### illegalKeywords ###

**default value:** []

This configuration option represents the list of keywords to alarm on.
It is empty by default, so that every team can decide what keywords are "illegal" in their project.
Note, that keywords are lowercased before comparison.

### allowInheritedProperties ###

**default value:** true

Whether to skip this check for properties that are declared in inherited class or interface.

### considerInheritedOnReflectionError ###

**default value:** false

It is possible that reflection error occurs in process of this sniff work.
This configuration parameter defines what to do if it happens: either consider function parameter
as inherited despite error or allow some false-positives.

Vaimo.NamingConventions.AndOrInVariableName
-------------------------------------------

This sniff extends IllegalWordInVariableName just to have more precise error message 
and allow team to use original sniff for their own rules.

Vaimo.NamingConventions.VariablesCamelCase
------------------------------------------

This sniff checks declared variables, class properties, or method parameters and ensures 
they are properly camelCased.

Vaimo.PHP.NoSilencedErrors
--------------------------

Reimplements `Generic.PHP.NoSilencedErrors` to allow configuring a list of allowed function calls 
that actually should be error-silenced  (i.e. `trigger_error`)

See Vaimo.Comments.DeprecationNotice for a use case.

### allowedFunctions ###

**default value**: [trigger_error]

List of function calls that are allowed to be error-silenced.

### error ###

**default value**: true

Whether to raise an error or warning.

Vaimo.TypeHints.IllegalReturnTypeAnnotation
-------------------------------------------

This sniff raises an alarm when it meets a return type annotation that is disallowed. 

The main reasoning behind this sniff is to stop people using `mixed` as a return type, 
because it makes no sense to use return type annotation in a way that does not provide 
any information about actual type. 

Sniff is implemented in more generic way so that the list of illegal return types is configurable:

### illegalReturnTypes ###

Vaimo.Exceptions.TryProcessSystemResources
------------------------------------------

This sniff is a replacement for the original one Magento2.Exceptions.TryProcessSystemResources.MissingTryCatch
Requires wrapping `stream_*` and `socket_*` functions with try-catch blocks. These functions are
almost always used for networking or IO and therefore are considered to be error-prone

### TryProcessSystemResources ###

**default value:** [mixed]
