Known Issues
============

This document is a list of issues that are related to other packages 
and can cause issues on your projects.

Patches removed by `composer install --no-dev`
----------------------------------------------

When you have [vaimo/composer-patches](https://github.com/vaimo/composer-patches) and
vaimo/phpcs-rulesets installed as `--dev` dependency, running `composer install --no-dev` will remove all patches.

It happens because of the bug in `vaimo/composer-patches`, when it checks if it is going to be deleted:
it false-positively treats `vaimo/phpcs-rulesets` as itself 
(because of "Vaimo" namespace) and therefore resets all applied patches.

There is an open [PR](https://github.com/vaimo/composer-patches/pull/101), 
so one just will need to update patcher (soonish). 
Right now, you can disable running plugins in this particular command:

```shell
composer install --no-dev --no-plugins
```

Errors being ignored in phpcs run
---------------------------------

When you run phpcs in parallel mode (either using --parallel=N or in phpcs.xml), 
it CAN ignore fatal errors being thrown (and thus preventing some files from being checked) in child process.

It might lead to optimistic build results: despite having code styling problems, 
phpcs will return 0 exit status and allow Jenkins to proceed.

I created a [PR](https://github.com/squizlabs/PHP_CodeSniffer/pull/3645) for this issue,
it has been merged and is planned for 3.7.2 release.

Right now, the only option we have is to NOT use parallel mode.
