Advanced Type Hinting
=====================

The main goal of precisely and honestly hinting your types is to provide more information 
about your type system to a reader.

`/** @param array */` does not provide any information about the type, 
because a reader might still wonder about the following questions and _have to read the code_:

* How do I iterate over the array? Should I respect keys?
* What is a type of item? Do all items have the same type? 
* If it is an associative array, what fields are there?

The only way to answer these question is to actually dive into the code. 
It happens because unfortunately PHP has mixed all sorts of absolutely different concepts
of “array-like” structures in one and called it array.

Moreover, there is no way to distinguish “a list of T” from “a bunch of junk” associative array on a language level.

Good news is we can do that with more advanced [array shape type hints](https://phpstan.org/writing-php-code/phpdoc-types#array-shapes).
Even better news: it is partially [supported by PhpStorm](https://blog.jetbrains.com/phpstorm/2021/07/phpstorm-2021-2-release/#array_shapes_phpdoc_syntax_support).

So, some examples of how to use it:

List of T
---------

For example, `['bananas', 'oranges', 'apples']` is a list of strings: each array value is a string (the same type!),
and we don't care much about array keys.

```php
/**
 * @var string[]
 * @var array<string> 
 */
private array $fruits = ['bananas', 'oranges', 'apples'];
```

[Tuples](https://en.wikipedia.org/wiki/Tuple)
---------------------------------------------

For example, your function returns a tuple of [int, string] for some reason. Valid (and readable by mere humans!) phpDoc
would look like this:

```php
/**
 * @return array{int, string}
 */
public function getError(): array 
{
    return [100, 'Something went wrong'];
}
```

This annotation says that given array has 2 items, first one is `int` and second one is `string`.

Short associative arrays
------------------------

The same example, but instead of a tuple, we return an associative array:

```php
/**
 * @return array{code: int, message: string} 
 */
public function getError(): array
{
    return ['code' => 100, 'message' => 'Something went wrong'];
}
```

Couple of additional notes on how flexible array shapes are:

* You can define a field as optional with `array{code: int, message?: string}`. 
    In this example, `code` is mandatory array field, 
    but `message` is optional and function user MUST perform "field existence" check.
* You can use class names as possible value types.

Big Assoc Arrays
----------------

Despite PHPStan not limiting the size of array shape,
PhpStorm currently supports only one-line array shapes without nesting.

In this case you can still use the same trick as in section earlier, sacrificing phpstorm support temporarily:

```php
/**
 * @return array{
 *    first_name: string,
 *    last_name: string,
 *    address: array{
 *        country: string,
 *        zip_code?: string
 *    }      
 * }     
 */
public function getVeryComplexStructure(): array;
```

Alternative option would be to use PHPStan's [local type aliases](https://phpstan.org/writing-php-code/phpdoc-types#local-type-aliases):

```php
/**
 * @phpstan-type Customer array{
 *    first_name: string,
 *    last_name: string,
 *    address: array{
 *        country: string,
 *        zip_code?: string
 *    }      
 * }    
 */
interface Whatever
{
    /**
     * @phpstan-return Customer
     */
    public function getVeryComplexStructure(): array;
}
```

### array{} vs array<> ###

Please note the difference between these two:

* `array{string, string}` is a tuple of two strings.
* `array<string, string>` is an associative array of arbitrary length where both keys and values are strings.

### Meh, it's too much information to note ###

Yes, sometimes it is hard to express your type with all these fancy annotations. 
Although, it means only that your type system is complex, 
and it is even harder to figure out all this information without these annotations.

So, if your type is too hard to express it with PHPStan annotations, 
it might be worth to convert into a native type (e.g. create a [Value Object](https://en.wikipedia.org/wiki/Value_object) or [DTO](https://en.wikipedia.org/wiki/Data_transfer_object) for it)?

### What about Magento? ###

As we know if the class or interface is going to be used anywhere in Magento web API, 
it MUST follow [Magento recommendation on typehints](https://devdocs.magento.com/guides/v2.4/extension-dev-guide/service-contracts/service-to-web-service.html#service-interface-requirements).

In this case, you most probably will have to either ignore `SlevomatCodingStandard.TypeHints` sniffs 
and/or introduce a lot of simple objects to represent your data in native way.

Although, Magento does not actually allow you to use assoc arrays as web services arguments or return types,
so this should not be a problem at all.

In addition, it is worth to note that you can use both `@return` and `@phpstan-return` annotations 
in order to both provide Magento-compatible annotations and something that is actually useful for the reader.

### Completely Made up Example

Imagine the following class:

```php
use Magento\Customer\CustomerData\SectionSourceInterface;

class Newsletter implements SectionSourceInterface
{
    /**
     * @return array 
     */
    public function getSectionData() {}
}
```

I intentionally left out _actual code_ of the method. What can you say about `getSectionData()` return type?

Well, _probably_ it is an array. I use "probably" because phpDoc does not enforce anything. 
Unfortunately though, we cannot move it to native type hint because `SectionSourceInterface` does not do this either.

Ok, it is an array. What kind of array? Questions a _reader_ might ask this code:

* How do I iterate over this array?
* If it is a list, what is an item type?
* If it's not, then what are fields in the array?

The only way to answer these question _now_ is to read actual code, 
which negates the whole idea of being able to skim through the code and slows down a reader _a lot_.

Now take a look on the same function:

```php
class Newsletter implements SectionSourceInterface
{
    /**
     * @return array{first_name?: string, last_name?: string, email?: string}
     * 
     * If customer is not logged in returns an empty array, all three fields otherwise.     
     */
    public function getSectionData() {}
}
```

This annotation actually serves its purpose and clearly answers the vast majority of questions:

* The user MUST respect array keys to iterate;
* This is not a list, but an associative array
* There are `first_name`, `last_name`, and `email` fields in this array. All of them are optional.
* But actually, they are not so optional:
    Textual part clarifies that function returns either empty array when customer is not logged in, or all fields otherwise.
