Working with PHP Language Level
===============================

The ruleset includes sniffs that check for some features available only in newer versions of PHP (7.4 or 8.0) so that
it is important to understand how to manipulate those checks and adjust your PHP language level in `phpcs`.

Sniffs MUST use something like this to define PHP language level:

```php
$phpVersion = \PHP_CodeSniffer\Config::getConfigData('php_version') ?? PHP_VERSION_ID;
``` 

Configure Language Level
------------------------

So, by default `phpcs` considers version of PHP that runs it as a language level, but one can set it up 
in configuration file using the following command:

```shell
# to set it to 7.4
vendor/bin/phpcs --config-set php_version 70400
```

The only problem with this approach is... `phpcs` stores config file in its `vendor/` folder so that 
it is not in version control system and therefore it is not that easy to share it with teammates and especially Jenkins.

Another way to do so is to call `phpcs` with runtime configuration parameter:

```
vendor/bin/phpcs --runtime-set php_version 70400 src/
```

In this case team can share their pre-commit hooks via [CaptainHook](https://captainhookphp.github.io/captainhook/)
or any other similar tool and configure it on Jenkins side easily.

Although, the best way to handle language level is probably setting it as a config option in phpcs.xml:

```xml
<ruleset name="MyAwesomeRuleset">
    <config name="php_version" value="70400" />
</ruleset>
```
