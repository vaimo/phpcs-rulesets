PHPCS Reference
===============

Before we go to phpcs.xml detailed explanation, here are some useful tips:

* Run `phpcs -i` to get list of installed coding standards
* Run `phpcs --standard=%name% -e` to get list of Sniffs configured for given standard.
* You can always run `phpcs --help` or read up on [phpcs documentation page](https://github.com/squizlabs/PHP_CodeSniffer/wiki/Advanced-Usage#using-a-default-configuration-file).

Ok, let's start from a quick recap. Phpcs.xml (or phpcs.xml.dist) is "final" configuration file 
that should be placed in project repository root so that running just `vendor/bin/phpcs path/to/file.php` 
will run phpcs with all the configuration needed without typing it each time.

**Note:** This current project is a bit special in terms of phpcs.xml location (it is located in `build` folder). 
This is made intentionally to avoid loading all the configuration in tests.

Besides storing `phpcs` configuration, this file can be used to change a ruleset 
in order to adjust it to the project's needs.

Code Sniffer CLI Configuration
------------------------------

This part of phpcs.xml is a replacement for `phpcs` cli arguments:

```xml
<!-- 
    comma-separated list of file extensions to check if phpcs is asked to check the directory:
    $ vendor/bin/phpcs src/ 
-->
<arg name="extensions" value="php,phtml" />
<!--
    The basepath will be stripped from the start of the filename in the output from phpcs.
    Setting this to your project root will result in short file paths. (e.g. `src/MyModel.php` instead of `/very/long/path/to/code/src/MyModel.php`)
-->
<arg name="basepath" value="." />
<!--
    Make output colorful to make easier to read for human beings.
-->
<arg name="colors" />
<!--
    Show progress on the run. Again, makes it easier to use for human beings.
-->
<arg value="p" />
<!--
    Show sniff code on error. Makes it simpler to debug (now you know what to ignore ;))
-->
<arg value="s" />
<!--
    Minimum severity of raised error to be included in report 
    (and therefore stop commit process if used on pre-commit hook)
    
    Severity can be from 1 to 10, when 1 is the smallest possible error, and 10 is the most serious one.
-->
<arg name="severity" value="1" />
<!--
    The same, but only of error-level violations
-->
<arg name="error-severity" value="1" />
<!--
    The same, but only of warning-level violations
-->
<arg name="warning-severity" value="1" />
```

Ruleset Adjustments
-------------------

#### Exclude files from checking

```xml
<!-- 
    File pattern to exclude from checking when folder is given. 
    In this particular case it asks phpcs to NOT check test stubs.
    They definitely contain styling errors (that should be found by tests)
--> 
<exclude-pattern>tests/*/*Stub.php</exclude-pattern>
```

Note, you can specify `exlude-pattern` globally for the whole ruleset or for particular rule:

```xml
<rule ref="Generic.PHP.CharacterBeforePHPOpeningTag">
    <!-- Allow symbols before PHP opening tag in templates -->
    <exclude-pattern>*\.phtml</exclude-pattern>
</rule>
```

#### Ruleset to use

```xml
<!-- 
    Vaimo is a name of coding standard to load
    You can load as many coding standards (or fine-grained rules separately) as you want.
--> 
<rule ref="Vaimo" />
```

#### Configure Sniff

If a Sniff has public properties, these properties can be configured from `phpcs.xml` or `ruleset.xml`.

```xml
<rule ref="Vaimo.Classes.ObjectInstantiation">
    <properties>
        <property name="allowedClasses" type="array">
            <element value="\ReflectionClass" />
            <element value="\ReflectionMethod" />
        </property>
    </properties>
</rule>
```

#### Load or Exclude a Sniff from Ruleset 

First, a quick recap on Sniff code structure: It consists of four dot-separated parts like this:

```
Vaimo.Classes.ObjectInstantiation.Found
```

* First part (Vaimo in our case) is a standard name;
* Second part (Classes) is sniff namespace or group;
* Third part (ObjectInstantiation) is actual Sniff name;
* Last part (Found) is an error (or warning) code that Sniff can raise. Note, that one sniff can raise several violation codes.

So, in order to load or enable anything you just specify `rule` as precise as you want:

```xml
<!-- This will load all sniffs under Vaimo standard and enable all violation codes -->
<rule ref="Vaimo" />
<!-- This will load all sniffs under Vaimo.Classes namespace -->
<rule ref="Vaimo.Classes" />
<!-- This will load only Vaimo.Classes.ObjectInstantiation sniff and enable all its violation codes -->
<rule ref="Vaimo.Classes.ObjectInstantiation" />
```

What if I want to load all, but...? Here `exclude` keywords enters the party. 
You can put it inside `rule` definition to exclude namespace, sniff or one particular violation code.
Be careful with specifying rule, because it will include everything in given scope: 

```xml
<!-- This will load the whole Vaimo standard and exclude Vaimo.Classes.ObjectInstantiation -->
<rule ref="Vaimo">
    <exclude name="Vaimo.Classes.ObjectInstantiation" />
</rule>
<!-- 
    In case you loaded Vaimo previously (or it was loaded by imported coding standard)
    and now you need to exclude one Sniff completely 
-->
<rule ref="Vaimo.Classes.ObjectInstantiation">
    <exclude name="Vaimo.Classes.ObjectInstantiation" />
</rule>
<!-- Add a sniff to current ruleset and enable all its violation codes except "Found" -->
<rule ref="Vaimo.Classes.ObjectInstantiation">
    <exclude name="Vaimo.Classes.ObjectInstantiation.Found" />
</rule>
```

