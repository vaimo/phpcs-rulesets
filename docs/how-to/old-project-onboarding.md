Get started with your project
-----------------------------

First of all, let me emphasize: it is ok to have tons upon tons of code style errors, 
when there is a big code base, and you are just starting to automate code style checks. 

So, on-boarding pretty big coding standard with lots of sniffs can be a challenging task on itself.

If you are trying to set up a coding standard on your 5 years old project and first run results look like this:

```
EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE  60 / 921 (7%)
EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE 120 / 921 (13%)
EEEEEEEEEEEEEEEEEEEEEEEEEEEEE.EEEEEEEEEEEEEEEEEEEEEEEEEEEEEE 180 / 921 (20%)
EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE 240 / 921 (26%)
EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE 300 / 921 (33%)
```

This guide can provide you a little help on how to even start.

### Automated fixing ###

In a lucky case you've got tests in your project, you can automatically fix some errors by running `phpcbf`, 
which is shipped with `phpcs` to automatically fix _fixable_ errors. It can get rid of quite a lot of standard violations.

**Note!**
Please be aware that running `phpcbf` without proper test coverage is not really a good idea, 
because it CAN and most probably WILL break your code.

For example, automated replacing phpDoc type hints with native types can be dangerous. Let's take a look at this code:

```php
class Example
{
    /** 
     * @var array 
     */
    private $modifiers;
    
    public function getModifiers(): array
    {
        if ($this->modifiers === null) {
            $this->modifiers = $this->initModifiers();
        }
        
        return $this->modifiers;
    }
}
```

After running `phpcbf` (with `SlevomatCodingStandard.TypeHints.ParameterTypeHint.MissingNativeTypeHint` sniff code, 
which is disabled by default though), it will be converted to the following code:

```php
class Example
{
    private array $modifiers;
    
    public function getModifiers(): array
    {
        if ($this->modifiers === null) {
            $this->modifiers = $this->initModifiers();
        }
        
        return $this->modifiers;
    }
}
```

And now it fails with "Typed property Example::$modifiers must not be accessed before initialization", 
because phpdoc was shamelessly lying about property type. It was `?array`, not `array`. 

### Check only updated files on commit ###

Next natural step to reduce amount of work needed to be done at once is to reduce scope: 
check only files you've touched before committing.

```shell
vendor/bin/phpcs --filter=gitstaged
```

You can instruct Jenkins to do the same using Bitbucket plugin (example assumes you use Multibranch pipeline):

```groovy

properties(
    [
        disableConcurrentBuilds(),
        overrideIndexTriggers(false),
        parameters(
            [
                string(defaultValue: env.BRANCH_NAME, description: '', name: 'SOURCE_BRANCH', trim: false),
                string(defaultValue: 'master', description: '', name: 'TARGET_BRANCH', trim: false)
            ]
        ),
        pipelineTriggers(
            [
                bitBucketTrigger(
                    [
                        [
                            $class: 'BitBucketPPRPullRequestTriggerFilter',
                            actionFilter: [$class: 'BitBucketPPRPullRequestCreatedActionFilter', allowedBranches: '']
                        ],
                        [
                            $class: 'BitBucketPPRPullRequestTriggerFilter',
                            actionFilter: [$class: 'BitBucketPPRPullRequestUpdatedActionFilter', allowedBranches: '']
                        ]
                    ]
                )
            ]
        )
    ]
)

pipeline {
    stage("Gathering Info") {
        if (env.BITBUCKET_SOURCE_BRANCH == null && env.SOURCE_BRANCH != null) {
            env.BITBUCKET_SOURCE_BRANCH = env.SOURCE_BRANCH
        }
        
        if (env.BITBUCKET_TARGET_BRANCH == null && env.TARGET_BRANCH != null) {
            env.BITBUCKET_TARGET_BRANCH = env.TARGET_BRANCH
        }

        def changedFiles = sh(returnStdout: true, script:"git diff --diff-filter=ACMR --name-only origin/${env.BITBUCKET_TARGET_BRANCH}..origin/${env.BITBUCKET_SOURCE_BRANCH}").trim()
        changedFiles = changedFiles.split('\n')
        env.CHANGED_FILES = changedFiles.join(" ")
    }
    
    stage("phpcs") {
        when {
            expression { env.CHANGED_FILES?.trim() }
        }
        steps {
            bitbucketStatus (key: 'code_quality', name: 'Code Quality', repo: "${env.REPOSITORY_URL}", commitId: "${env.BITBUCKET_PULL_REQUEST_LATEST_COMMIT_FROM_SOURCE_BRANCH}") {
                ansiColor("xterm") {
                    sh("vendor/bin/phpcs ${env.CHANGED_FILES}")
                }
            }
        }
    }
}
```

### Reduce noise ###

This is definitely a slippery ground, but you can also reduce amount of violations 
by disabling some sniffs that produce the most of the errors and warnings. 

**Note!**
Please think twice before proceeding with this advice!
Yes, it might help with embracing standards, but I believe it is not a good strategy in long-term, 
mainly because these sniffs were written with an intention to make a code more readable, 
so having lots of violations might be a learning opportunity.

The first step would be to find out the list of those noisy sniffs:

```shell
vendor/bin/phpcs -s --report=source
```

Instead of writing all the errors in every file, this report generates a list of error codes with number of occurrences.

```
PHP CODE SNIFFER VIOLATION SOURCE SUMMARY
----------------------------------------------------------------------------------
    SOURCE                                                                  COUNT
----------------------------------------------------------------------------------
[ ] Vaimo.Classes.ProtectedMember.Property                                  15
[x] SlevomatCodingStandard.Arrays.TrailingArrayComma.MissingTrailingComma   10
```

Be aware that the list can be actually pretty long, so that it would be a good idea to either `head -20` it or forward output to a file.

After review of results, you can add some sniffs to excludes in your `phpcs.xml`:

```xml
<rule ref="Vaimo">
    <exclude name="Vaimo.Classes.ProtectedMember.Property" />
    <exclude name="SlevomatCodingStandard.Arrays.TrailingArrayComma.MissingTrailingComma" />
</rule>
```
