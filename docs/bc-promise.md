Backward Compatibility Promise
==============================

MAJOR version is incremented when:

- Sniff is deleted from the code base
- Sniff code is deleted from the code base
- Breaking changes are introduced in any of the sniffs
- New coding standard (ruleset) is introduced or removed
- Breaking changes in Helpers are introduced

MINOR version is incremented when:

- Existing sniff is removed from the standard (but not from codebase!)
- New sniff or sniff code is added to existing coding standard
- Sniff scope is widened (e.g. UnusedFunctionParameter starts checking arrow functions in addition to methods and closures)

PATCH version is incremented when:

- Non-breaking bug fixes are made


Breaking Changes Definition
---------------------------

| Change we can make    | Is it Breaking?  | Can it break phpcs.xml       | Can it break build? | Can it break downstream ruleset? |
|-----------------------|------------------|------------------------------|---------------------|----------------------------------|
| Sniff introduced      | no               | no                           | yes                 | yes, in case of naming conflict  | 
| Sniff code introduced | no               | no                           | yes                 | yes, in case of naming conflict  | 
| Sniff deleted         | yes              | yes, in case it was excluded | no                  | yes, if it was extended          |
| Sniff code deleted    | yes              | yes                          | no                  | yes                              |
| BC breaks in Helpers  | yes              | no                           | no                  | yes                              | 
| BC breaks in Sniffs   | yes              | no                           | no                  | yes                              | 
| CLI output changes    | no               | no                           | no                  | no                               |

Notes:

* Despite being internal API, I'd rather encourage its reuse by downstream by covering it with BC promise.
* "BC breaks in Sniffs" covers almost nothing because only things a sniff exposes is a list of its configuration options
and methods required by interface.
* Parsing CLI output is horrible idea, use [custom reports](https://github.com/squizlabs/PHP_CodeSniffer/wiki/Reporting). 
