Vaimo Coding Standard
=====================

Coding standard consists of several groups of sniffs provided by four major vendors: Squizlabs (inventors of phpcs), Magento, Slevomat (brought by Doctrine Coding Standard), and Vaimo.

In order to avoid making over-sized document, sniffs description is split into a bunch of documents by sniff vendor name:

* [Generic](standard/generic.md) (brought by Squizlabs and `phpcs`)
* [Magento2](standard/magento.md) (brought by Magento Coding Standard)  
* [PEAR](standard/pear.md) (brought by Squizlabs and `phpcs`)
* [PSR](standard/psr.md) (brought by Squizlabs and `phpcs`)
* [SlevomatCodingStandard](standard/slevomat.md) (brought by Doctrine Coding Standard)
* [Squiz](standard/squiz.md) (brought by Squizlabs and `phpcs`)
* [Vaimo](standard/vaimo.md) (brought by this package)
