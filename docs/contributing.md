How to Contribute
=================

If there is something you want to add or change in this ruleset, do not hesitate to raise 
an issue or make a pull request!

Raise an issue
--------------

Vaimo Coding Standard has a separate [Jira project](https://jira.vaimo.com/projects/INTVCS).

Create issues under it.

### Generic Hints about Reporting Issues ###

* Start with reading documentation about a sniff-to-blame ;)
* Always attach the simplest possible test code that is a subject of issue. 
  Being able to just throw an example to samples directory and run phpcs helps a lot.
* Always attach phpcs output (run it with `-s` key) so that sniff code is visible.
  It helps with hunting down problematic sniff class. 
* Include a reasoning why do you want this to be changed (why do you think it is false-positive, etc...).
  Some issues are not even issues ;)

Making a Pull Request
---------------------

On a luck occasion you have some spare time to fix the issue and make a pull request, 
please consider the following guideline:

* This package uses [vaimo/composer-changelogs](http://github.com/vaimo/composer-changelogs) to create releases, 
  make sure you update changelog.json.

* Do not break [backward compatibility promise](bc-promise.md).

* Please put all automatically generated changes in separate commits and include the command you run as a commit message:

```shell
composer update
git commit -am "%issueNumber% composer update"
```

* Run `phpcs --standard=ci/phpcs.xml` before committing things (put it in pre-commit hooks). 
   Jenkins checks all the codebase in each pull request. 
   It is made in order to make sure that ruleset always follows its own rules. 
   If you add or change a Sniff, refactor the ruleset if there are any violations of the sniff as well. 

* Run `vendor/bin/phpunit` before committing things (yup, better as pre-commit hook), 
   Jenkins does this as well, but it is better to not have broken commits. 
   The best strategy is to run tests on pre-commit hook.
   
* Cover your changes with tests, Jenkins requires 100% test coverage for Sniff classes.

* Keep git history clean: 
    * Make meaningful commit messages
    * Use rebase to pull changes from master and fix conflicts
    * Use interactive rebase instead of "cr fixes" commits

* Document your changes if you add or delete sniffs. Sniffs in documentation should be ordered alphabetically.
   There is a test for that, just run `vendor/bin/phpunit`, and it will tell what's missing.
