<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

declare(strict_types=1);

namespace Vaimo\Helpers;

use PHP_CodeSniffer\Files\File;

use function array_map;
use function array_reverse;
use function in_array;

class ClassHelper
{
    public const CLASS_TOKEN_CODES = [T_CLASS, T_ANON_CLASS];

    public static function getCurrentClassPointer(File $file, int $pointer): ?int
    {
        $token = $file->getTokens()[$pointer] ?? null;

        if (!$token) {
            return null;
        }

        if (in_array($token['code'], self::CLASS_TOKEN_CODES, true)) {
            return $pointer;
        }

        $tokenConditions = array_reverse($token['conditions'], true);

        foreach ($tokenConditions as $stackPointer => $code) {
            if (in_array($code, self::CLASS_TOKEN_CODES, true)) {
                return $stackPointer;
            }
        }

        return null;
    }

    public static function getCurrentClassName(File $file, int $pointer): ?string
    {
        $classPointer = self::getCurrentClassPointer($file, $pointer);

        return $classPointer !== null
            ? self::getName($file, $pointer)
            : null;
    }

    public static function getName(File $file, int $pointer): ?string
    {
        if (self::isAnonymousClass($file, $pointer)) {
            return 'class@anonymous';
        }

        return $file->getDeclarationName($pointer);
    }

    public static function isAnonymousClass(File $file, int $classPointer): bool
    {
        $token = $file->getTokens()[$classPointer];

        return $token['code'] === T_ANON_CLASS;
    }

    public static function getFullyQualifiedName(File $file, int $pointer): ?string
    {
        return NamespaceHelper::findFqcnForClassName($file, self::getName($file, $pointer), $pointer);
    }

    public static function getCurrentClassFullyQualifiedName(File $file, int $pointer): ?string
    {
        $classPointer = self::getCurrentClassPointer($file, $pointer);

        if ($classPointer === null) {
            return null;
        }

        return NamespaceHelper::findFqcnForClassName(
            $file,
            self::getCurrentClassName($file, $classPointer),
            $classPointer
        );
    }

    public static function getExtendedClassName(File $file, int $pointer): ?string
    {
        $classPointer = self::getCurrentClassPointer($file, $pointer);

        if ($classPointer === null) {
            return null;
        }

        $extendedClassName = $file->findExtendedClassName($classPointer);

        return $extendedClassName === false ? null : $extendedClassName;
    }

    public static function getExtendedClassFullyQualifiedName(File $file, int $pointer): ?string
    {
        $extendedClassName = self::getExtendedClassName($file, $pointer);

        if (!$extendedClassName) {
            return null;
        }

        $imports = UseStatementHelper::findImportsInFile($file);

        return NamespaceHelper::composeFqcn(
            $extendedClassName,
            $imports,
            NamespaceHelper::findCurrentNamespaceName($file, $pointer)
        );
    }

    /**
     * Returns list of fully qualified names of implemented interfaces in a current class.
     *
     * @param int $pointer any pointer in a class to check
     * @return string[]
     */
    public static function getImplementedInterfaces(File $file, int $pointer): array
    {
        $classPointer = self::getCurrentClassPointer($file, $pointer);

        return array_map(
            static fn($interface) => NamespaceHelper::findFqcnForClassName($file, $interface, $classPointer),
            $file->findImplementedInterfaceNames($classPointer)
        );
    }
}
