<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

declare(strict_types=1);

namespace Vaimo\Helpers;

use function array_map;
use function preg_split;
use function strlen;
use function strpos;
use function substr;

class StringHelper
{
    /**
     * @return string[]
     */
    public static function splitCamelCaseToWords(string $camelCasedName): array
    {
        return array_map(
            'strtolower',
            preg_split('/(?=[A-Z])/', $camelCasedName)
        );
    }

    public static function startsWith(string $haystack, string $needle): bool
    {
        return strpos($haystack, $needle) === 0;
    }

    public static function endsWith(string $haystack, string $needle): bool
    {
        return substr($haystack, 0 - strlen($needle)) === $needle;
    }
}
