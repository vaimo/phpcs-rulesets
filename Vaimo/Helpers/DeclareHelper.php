<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

declare(strict_types=1);

namespace Vaimo\Helpers;

use PHP_CodeSniffer\Files\File;

use function count;

class DeclareHelper
{
    public static function findDeclareSectionEnd(File $file, int $startAt = 0): int
    {
        if (!self::hasDeclares($file)) {
            return $startAt;
        }

        $nextDeclareEnd = $startAt;
        while ($nextDeclareStart = self::findDeclareStatement($file, $nextDeclareEnd)) {
            $nextDeclareEnd = TokenHelper::findNext($file, [T_SEMICOLON], $nextDeclareStart + 1);
        }

        return $nextDeclareEnd;
    }

    public static function hasMultilineDeclareSection(File $file): bool
    {
        if (!self::hasDeclares($file)) {
            return false;
        }

        $declareEnds = self::findDeclareSectionEnd($file, 0);

        return $file->getTokens()[$declareEnds]['line'] > 1;
    }

    public static function hasDeclares(File $file): bool
    {
        foreach ($file->getTokens() as $token) {
            if ($token['code'] === T_DECLARE) {
                return true;
            }
        }

        return false;
    }

    public static function findDeclareStatement(File $file, int $startAt = 0): ?int
    {
        for ($i = $startAt, $total = count($file->getTokens()); $i < $total; $i++) {
            if ($file->getTokens()[$i]['code'] === T_DECLARE) {
                return $i;
            }
        }

        return null;
    }
}
