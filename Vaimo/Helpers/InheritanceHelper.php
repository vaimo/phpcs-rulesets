<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

declare(strict_types=1);

namespace Vaimo\Helpers;

use PHP_CodeSniffer\Files\File;
use Vaimo\ValueObject\ClassMember;

use function array_filter;
use function array_map;
use function array_merge;
use function array_reduce;
use function call_user_func;
use function implode;
use function in_array;

// phpcs:disable SlevomatCodingStandard.Classes.UnusedPrivateElements -- It can't find collect* methods usage

class InheritanceHelper
{
    private const COLLECTORS = [
        T_CONST => ['collectInheritedConstants'],
        T_FUNCTION => ['collectInheritedMethods', 'collectImplementedMethods'],
        T_VARIABLE => ['collectInheritedProperties'],
    ];

    /**
     * @var array<string, ClassMember[]>
     * class fqcn is an array key, and list of its inherited members is the value.
     * @see InheritanceHelper::collectAllInheritedMembers()
     */
    private static array $allMembersCache = [];

    /**
     * @var array<string, array<string, ClassMember[]>>
     * 2-dimensional array with the key is class name and value is key-value array of filter => members
     * @see InheritanceHelper::collectInheritedMembers()
     */
    private static array $filteredMembersCache = [];

    /**
     * @return ClassMember[]
     */
    private static function collectByType(File $file, int $classPointer, int $type): array
    {
        if (!isset(self::COLLECTORS[$type])) {
            throw new \InvalidArgumentException('Unknown type: ' . $type);
        }

        return array_reduce(
            self::COLLECTORS[$type],
            static fn($carry, $collectorMethod) => array_merge(
                $carry,
                call_user_func([self::class, $collectorMethod], $file, $classPointer)
            ),
            []
        );
    }

    /**
     * @return ClassMember[]
     * @throws \ReflectionException
     */
    private static function collectInheritedProperties(File $file, int $classPointer): array
    {
        $reflection = self::getExtendedClassReflection($file, $classPointer);
        if (!$reflection) {
            return [];
        }

        return array_map(
            static fn(\ReflectionProperty $property) => ClassMember::createProperty($property->getName()),
            $reflection->getProperties(\ReflectionProperty::IS_PUBLIC | \ReflectionProperty::IS_PROTECTED)
        );
    }

    /**
     * @return ClassMember[]
     * @throws \ReflectionException
     */
    private static function collectInheritedMethods(File $file, int $classPointer): array
    {
        $reflection = self::getExtendedClassReflection($file, $classPointer);
        if (!$reflection) {
            return [];
        }

        return array_map(
            static fn(\ReflectionMethod $method) => ClassMember::createMethod($method->getName()),
            $reflection->getMethods(\ReflectionMethod::IS_PUBLIC | \ReflectionMethod::IS_PROTECTED)
        );
    }

    /**
     * @return ClassMember[]
     * @throws \ReflectionException
     */
    private static function collectImplementedMethods(File $file, int $classPointer): array
    {
        $interfaces = $file->findImplementedInterfaceNames($classPointer);

        if (!$interfaces) {
            return [];
        }

        $methods = [];
        $names = [];

        foreach ($interfaces as $interfaceName) {
            $fqcn = NamespaceHelper::findFqcnForClassName($file, $interfaceName, $classPointer);
            $reflection = new \ReflectionClass($fqcn);

            foreach ($reflection->getMethods() as $method) {
                if (in_array($method->getName(), $names)) {
                    continue;
                }

                $methods[] = ClassMember::createMethod($method->getName());
                $names[] = $method->getName();
            }
        }

        return $methods;
    }

    /**
     * @return ClassMember[]
     * @throws \ReflectionException
     */
    private static function collectInheritedConstants(File $file, int $classPointer): array
    {
        $reflection = self::getExtendedClassReflection($file, $classPointer);
        if (!$reflection) {
            return [];
        }

        $members = [];

        /** @var \ReflectionClassConstant $constant */
        foreach ($reflection->getReflectionConstants() as $constant) {
            // TODO: switch to getConstants($filters) when we require PHP 8.0
            if ($constant->isPrivate()) {
                continue;
            }

            $members[] = ClassMember::createConstant($constant->getName());
        }

        return $members;
    }

    /**
     * @return ClassMember[]
     */
    private static function collectAllInheritedMembers(File $file, int $classPointer, string $className): array
    {
        if (!isset(self::$allMembersCache[$className])) {
            self::$allMembersCache[$className] = array_reduce(
                MemberHelper::CLASS_MEMBER_TOKENS,
                static fn(array $carry, int $type) => array_merge(
                    $carry,
                    self::collectByType($file, $classPointer, $type)
                ),
                []
            );
        }

        return self::$allMembersCache[$className];
    }

    /**
     * @param int[] $types
     * @return ClassMember[]
     * @throws \ReflectionException Sniff must handle this exception in either raising or suppressing error.
     */
    public static function collectInheritedMembers(
        File $file,
        int $classPointer,
        array $types = MemberHelper::CLASS_MEMBER_TOKENS
    ): array {
        $currentClassName = ClassHelper::getCurrentClassFullyQualifiedName($file, $classPointer);
        $typesKey = implode(',', $types);

        if (!isset(self::$filteredMembersCache[$currentClassName][$typesKey])) {
            self::$filteredMembersCache[$currentClassName][$typesKey] = array_filter(
                self::collectAllInheritedMembers($file, $classPointer, $currentClassName),
                static fn(ClassMember $member) => in_array($member->getType(), $types)
            );
        }

        return self::$filteredMembersCache[$currentClassName][$typesKey];
    }

    /**
     * @throws \ReflectionException
     */
    public static function isInheritedMember(File $file, int $pointer): bool
    {
        return self::getInheritedMember($file, $pointer) instanceof ClassMember;
    }

    public static function getInheritedMember(File $file, int $pointer): ?ClassMember
    {
        $member = MemberHelper::getMemberData($file, $pointer);

        if (!$member) {
            return null;
        }

        $inheritedMembers = self::collectInheritedMembers(
            $file,
            ClassHelper::getCurrentClassPointer($file, $pointer),
            [$member->getType()]
        );

        foreach ($inheritedMembers as $inheritedMember) {
            if ($inheritedMember->getName() === $member->getName()) {
                return $inheritedMember;
            }
        }

        return null;
    }

    /**
     * @return string[]
     */
    public static function collectInheritedMethodParameters(File $file, int $methodPointer): array
    {
        $inheritedMethod = self::getInheritedMember($file, $methodPointer);

        if (!$inheritedMethod) {
            return [];
        }

        $reflectionMethod = self::getInheritedMethodReflection($file, $methodPointer, $inheritedMethod->getName());

        if (!$reflectionMethod) {
            return [];
        }

        return array_map(
            static fn(\ReflectionParameter $parameter) => $parameter->getName(),
            $reflectionMethod->getParameters()
        );
    }

    private static function getInheritedMethodReflection(File $file, int $pointer, string $name): ?\ReflectionMethod
    {
        $classReflection = self::getExtendedClassReflection($file, $pointer);

        if ($classReflection) {
            return $classReflection->getMethod($name);
        }

        foreach (ClassHelper::getImplementedInterfaces($file, $pointer) as $interface) {
            $interfaceReflection = new \ReflectionClass($interface);
            if ($interfaceReflection->hasMethod($name)) {
                return $interfaceReflection->getMethod($name);
            }
        }

        return null;
    }

    /**
     * @throws \ReflectionException
     */
    private static function getExtendedClassReflection(File $file, int $classPointer): ?\ReflectionClass
    {
        $extendedClass = ClassHelper::getExtendedClassFullyQualifiedName($file, $classPointer);

        if (!$extendedClass) {
            return null;
        }

        return new \ReflectionClass($extendedClass);
    }
}
