<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

declare(strict_types=1);

namespace Vaimo\Helpers;

use PHP_CodeSniffer\Files\File;

use function sprintf;

class ConstantHelper
{
    public static function getName(File $file, int $pointer): string
    {
        $namePointer = TokenHelper::findNext($file, [T_STRING], $pointer + 1);

        if (!$namePointer) {
            throw new \LogicException(sprintf(
                'Cannot find constant name in file %s, pointer: %d',
                $file->getFilename(),
                $pointer
            ));
        }

        $nameToken = $file->getTokens()[$namePointer];

        return $nameToken['content'];
    }

    /**
     * @return array{scope: string, scope_specified: bool}
     *
     * Possible values for `scope` are ['private', 'protected', 'public']
     */
    public static function getConstantProperties(File $file, int $pointer): array
    {
        $startOfStatement = $file->findStartOfStatement($pointer);

        for ($i = $startOfStatement; $i < $pointer; $i++) {
            $token = $file->getTokens()[$i];

            switch ($token['code']) {
                case T_PUBLIC:
                    return ['scope' => 'public', 'scope_specified' => true];

                case T_PROTECTED:
                    return ['scope' => 'protected', 'scope_specified' => true];

                case T_PRIVATE:
                    return ['scope' => 'private', 'scope_specified' => true];
            }
        }

        return ['scope' => 'public', 'scope_specified' => false];
    }
}
