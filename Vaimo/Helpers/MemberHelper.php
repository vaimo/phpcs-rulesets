<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

declare(strict_types=1);

namespace Vaimo\Helpers;

use PHP_CodeSniffer\Files\File;
use Vaimo\ValueObject\ClassMember;

use function array_filter;
use function array_key_exists;
use function array_merge;
use function in_array;
use function ltrim;

class MemberHelper
{
    public const CLASS_MEMBER_TOKENS = [
        ClassMember::PROPERTY,
        ClassMember::CONSTANT,
        ClassMember::METHOD,
    ];

    /**
     * @return ClassMember[]
     */
    public static function collectDeclaredProperties(File $file, int $pointer): array
    {
        return self::getClassMembersList($file, $pointer, [ClassMember::PROPERTY]);
    }

    /**
     * @return ClassMember[]
     */
    public static function collectDeclaredMethods(File $file, int $pointer): array
    {
        return self::getClassMembersList($file, $pointer, [ClassMember::METHOD]);
    }

    /**
     * @return ClassMember[]
     */
    public static function collectDeclaredConstants(File $file, int $pointer): array
    {
        return self::getClassMembersList($file, $pointer, [ClassMember::CONSTANT]);
    }

    /**
     * @param int[] $types
     * @return ClassMember[]
     */
    public static function getClassMembersList(
        File $file,
        int $pointer,
        array $types = self::CLASS_MEMBER_TOKENS
    ): array {
        $members = self::getDeclaredMembers($file, $pointer);

        $promotedProperties = [];
        foreach ($members as $member) {
            if ($member->isClassConstructor()) {
                $promotedProperties = self::getPromotedProperties($file, $member->getPointer());
                break;
            }
        }

        $members = array_merge($members, $promotedProperties);

        return array_filter($members, static fn (ClassMember $member) => in_array($member->getType(), $types, true));
    }

    /**
     * @return ClassMember[]
     */
    private static function getDeclaredMembers(File $file, int $pointer): array
    {
        $classPointer = ClassHelper::getCurrentClassPointer($file, $pointer);
        if (!$classPointer) {
            return [];
        }

        $classStart = $file->getTokens()[$classPointer]['scope_opener'];
        $classEnd = $file->getTokens()[$classPointer]['scope_closer'];
        $current = $classStart;

        $membersList = [];

        while ($current < $classEnd) {
            $memberPointer = TokenHelper::findNext($file, self::CLASS_MEMBER_TOKENS, $current, $classEnd);
            if (!$memberPointer) {
                break;
            }

            $member = self::getMemberData($file, $memberPointer);
            $membersList[] = $member;
            $current = $member->getMemberEndPointer();
        }

        return $membersList;
    }

    /**
     * @return ClassMember[]
     */
    private static function getPromotedProperties(File $file, int $constructorPointer): array
    {
        $methodParameters = $file->getMethodParameters($constructorPointer);
        $promotedProperties = [];

        foreach ($methodParameters as $parameter) {
            if (!array_key_exists('property_visibility', $parameter)) {
                continue;
            }

            $promotedProperties[] = ClassMember::createPromotedProperty($file, $parameter);
        }

        return $promotedProperties;
    }

    public static function getMemberData(File $file, int $pointer): ?ClassMember
    {
        $classPointer = ClassHelper::getCurrentClassPointer($file, $pointer);
        if (!$classPointer) {
            return null;
        }

        $memberToken = $file->getTokens()[$pointer];

        switch ($memberToken['code']) {
            case ClassMember::PROPERTY:
                $name = ltrim($memberToken['content'], '$');
                $memberEnd = $file->findEndOfStatement($pointer);
                $properties = $file->getMemberProperties($pointer);

                return ClassMember::createProperty($name, $pointer, $memberEnd, $properties);

            case ClassMember::CONSTANT:
                $name = ConstantHelper::getName($file, $pointer);
                $memberEnd = $file->findEndOfStatement($pointer);
                $properties = ConstantHelper::getConstantProperties($file, $pointer);

                return ClassMember::createConstant($name, $pointer, $memberEnd, $properties);

            case ClassMember::METHOD:
                $name = FunctionHelper::getName($file, $pointer);
                // if scope_closer is not defined => it's an interface or abstract function
                $memberEnd = $memberToken['scope_closer'] ?? $file->findEndOfStatement($pointer);
                $properties = $file->getMethodProperties($pointer);

                return ClassMember::createMethod($name, $pointer, $memberEnd, $properties);

            default:
                throw new \LogicException('Unknown token');
        }
    }
}
