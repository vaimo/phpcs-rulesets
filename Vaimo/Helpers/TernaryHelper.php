<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

declare(strict_types=1);

namespace Vaimo\Helpers;

use PHP_CodeSniffer\Files\File;

class TernaryHelper
{
    /**
     * Short version of ternary operator: `$a ?: $b` is called Elvis operator
     * @see https://en.wikipedia.org/wiki/Elvis_operator
     */
    public static function isElvisOperator(File $file, int $pointer): bool
    {
        $currentToken = $file->getTokens()[$pointer];

        if ($currentToken['code'] === T_INLINE_THEN) {
            $nextTokenPointer = TokenHelper::findNextEffective($file, $pointer + 1);
            $nextToken = $file->getTokens()[$nextTokenPointer];

            return $nextToken['code'] === T_INLINE_ELSE;
        }

        $prevTokenPointer = TokenHelper::findPreviousEffective($file, $pointer - 1);
        $prevToken = $file->getTokens()[$prevTokenPointer];

        return $prevToken['code'] === T_INLINE_THEN;
    }

    public static function isMultilineTernary(File $file, int $pointer): bool
    {
        [$startPointer, $endPointer] = self::findTernaryBoundaries($file, $pointer);
        $startLine = $file->getTokens()[$startPointer]['line'];
        $endLine = $file->getTokens()[$endPointer]['line'];

        return $startLine !== $endLine;
    }

    /**
     * @return array{int, int}
     */
    private static function findTernaryBoundaries(File $file, int $pointer): array
    {
        return [
            $file->findStartOfStatement($pointer),
            $file->findEndOfStatement($pointer),
        ];
    }
}
