<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

declare(strict_types=1);

namespace Vaimo\Helpers;

use PHP_CodeSniffer\Files\File;
use PHP_CodeSniffer\Util\Tokens;

use function array_pop;
use function in_array;
use function sprintf;

class FunctionHelper
{
    public static function getName(File $file, int $pointer): string
    {
        $namePointer = TokenHelper::findNext($file, [T_STRING], $pointer + 1);

        if (!$namePointer) {
            throw new \LogicException(sprintf(
                'Cannot find a function name in %s file, pointer: %d',
                $file->getFilename(),
                $pointer
            ));
        }

        $nameToken = $file->getTokens()[$namePointer];

        return $nameToken['content'];
    }

    public static function getFullyQualifiedName(File $file, int $pointer): string
    {
        $functionName = self::getName($file, $pointer);

        if (self::isMethod($file, $pointer)) {
            return sprintf(
                '%s::%s',
                ClassHelper::getCurrentClassFullyQualifiedName($file, $pointer),
                $functionName
            );
        }

        $currentNamespace = NamespaceHelper::findCurrentNamespaceName($file, $pointer);

        return $currentNamespace
            ? sprintf('%s::%s', $currentNamespace, $functionName)
            : $functionName;
    }

    public static function isMethod(File $file, int $pointer): bool
    {
        $functionPointerConditions = $file->getTokens()[$pointer]['conditions'];

        if ($functionPointerConditions === []) {
            return false;
        }

        $lastFunctionPointerCondition = array_pop($functionPointerConditions);

        return in_array($lastFunctionPointerCondition, Tokens::$ooScopeTokens, true);
    }
}
