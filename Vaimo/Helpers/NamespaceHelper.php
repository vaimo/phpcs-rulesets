<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

declare(strict_types=1);

namespace Vaimo\Helpers;

use PHP_CodeSniffer\Files\File;

use function array_key_exists;
use function array_shift;
use function explode;
use function implode;
use function strpos;

class NamespaceHelper
{
    public static function getName(File $file, int $pointer): string
    {
        $startPointer = TokenHelper::findNextEffective($file, $pointer + 1);
        $endOfStatement = $file->findEndOfStatement($pointer);

        return TokenHelper::getEffectiveContent($file, $startPointer, $endOfStatement - 1);
    }

    public static function findCurrentNamespaceName(File $file, int $pointer): ?string
    {
        $pointer = self::findCurrentNamespacePointer($file, $pointer);

        return $pointer
            ? self::getName($file, $pointer)
            : null;
    }

    public static function findCurrentNamespacePointer(File $file, int $anyPointer): ?int
    {
        return TokenHelper::findPrevious($file, [T_NAMESPACE], $anyPointer);
    }

    /**
     * @param string[] $imports
     */
    public static function composeFqcn(string $className, array $imports, ?string $currentNamespace): string
    {
        if (strpos($className, '\\') === 0) {
            return $className;
        }

        if (array_key_exists($className, $imports)) {
            return '\\' . $imports[$className];
        }

        if (strpos($className, '\\') > 0) {
            $parts = explode('\\', $className);

            $importedNamespace = array_shift($parts);

            return self::composeFqcn($importedNamespace, $imports, $currentNamespace) . '\\' . implode('\\', $parts);
        }

        return $currentNamespace
            ? '\\' . $currentNamespace . '\\' . $className
            : '\\' . $className;
    }

    public static function findFqcnForClassName(File $file, string $className, int $pointer): string
    {
        return self::composeFqcn(
            $className,
            UseStatementHelper::findImportsInFile($file),
            self::findCurrentNamespaceName($file, $pointer)
        );
    }
}
