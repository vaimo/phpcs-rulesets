<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

declare(strict_types=1);

namespace Vaimo\Helpers;

use PHP_CodeSniffer\Files\File;

use function array_filter;
use function array_shift;
use function in_array;
use function trim;

class CommentHelper
{
    public static function getContent(File $file, int $commentStartPointer): string
    {
        $content = TokenHelper::readUntil(
            $file,
            $commentStartPointer,
            static fn(File $file, int $pointer) => in_array(
                $file->getTokens()[$pointer]['code'],
                TokenHelper::COMMENT_TOKENS
            )
        );

        return trim($content);
    }

    public static function findDocCommentOpener(File $file, int $pointer): ?int
    {
        $tokens = $file->getTokens();

        if ($tokens[$pointer]['code'] === T_DOC_COMMENT_OPEN_TAG) {
            return $pointer;
        }

        // We are looking for close tag because we will find it earlier than open tag
        // (We are making a lookup backwards)
        // And close tag has a pointer to comment opener
        $foundPointer = TokenHelper::findPrevious(
            $file,
            [T_DOC_COMMENT_CLOSE_TAG, T_OPEN_CURLY_BRACKET, T_CLOSE_CURLY_BRACKET, T_SEMICOLON],
            $pointer - 1
        );

        if (!$foundPointer || $tokens[$foundPointer]['code'] !== T_DOC_COMMENT_CLOSE_TAG) {
            return null;
        }

        return $tokens[$foundPointer]['comment_opener'];
    }

    /**
     * Returns first found annotation, ignoring the rest
     *
     * @param string $type everything is annotation if it starts with "@"
     */
    public static function getFirstAnnotationPointer(File $file, int $docCommentOpenPointer, string $type): ?int
    {
        $allPointers = self::findAnnotationPointers($file, $docCommentOpenPointer, $type);

        return array_shift($allPointers);
    }

    /**
     * Returns all annotation pointers of given type
     *
     * @param string $type everything is annotation if it starts with "@"
     * @return int[]
     */
    public static function findAnnotationPointers(File $file, int $docCommentOpenPointer, string $type): array
    {
        $tokens = $file->getTokens();
        $docCommentOpenToken = $tokens[$docCommentOpenPointer];
        $annotationPointers = $docCommentOpenToken['comment_tags'] ?? [];

        return array_filter(
            $annotationPointers,
            static fn($pointer) => $tokens[$pointer]['content'] === $type
        );
    }

    public static function getAnnotationContent(File $file, int $annotationPointer): string
    {
        return trim(TokenHelper::readUntil(
            $file,
            $annotationPointer + 1,
            static fn($file, $i) => !in_array(
                $file->getTokens()[$i]['code'],
                [T_DOC_COMMENT_STAR, T_DOC_COMMENT_CLOSE_TAG],
                true
            )
        ));
    }
}
