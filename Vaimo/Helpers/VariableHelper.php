<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

declare(strict_types=1);

namespace Vaimo\Helpers;

use PHP_CodeSniffer\Files\File;

use function array_merge;
use function array_pop;
use function array_unique;
use function in_array;
use function is_array;
use function sprintf;
use function strtolower;
use function substr;
use function token_get_all;

class VariableHelper
{
    private const TOKENS = [
        T_VARIABLE,
        T_DOLLAR,
        T_DOUBLE_QUOTED_STRING,
        T_START_HEREDOC,
        T_START_NOWDOC,
    ];

    public static function normalizeName(string $variableName): string
    {
        if (!StringHelper::startsWith($variableName, '$')) {
            return $variableName;
        }

        return substr($variableName, 1);
    }

    public static function isMethodParameter(File $file, int $stackPointer): bool
    {
        $token = $file->getTokens()[$stackPointer];

        if (!isset($token['nested_parenthesis'])) {
            return false;
        }

        $parenthesis = $file->getTokens()[array_pop($token['nested_parenthesis'])];

        if (!isset($parenthesis['parenthesis_owner'])) {
            return false;
        }

        $parenthesisOwner = $file->getTokens()[$parenthesis['parenthesis_owner']];

        return $parenthesisOwner['code'] === T_FUNCTION;
    }

    public static function isPromotedProperty(File $file, int $stackPointer): bool
    {
        if (!self::isConstructorParameter($file, $stackPointer)) {
            return false;
        }

        $previousPointer = TokenHelper::findPreviousEffective($file, $stackPointer - 1);
        $previousToken = $file->getTokens()[$previousPointer];

        $isVisibility = in_array($previousToken['code'], TokenHelper::VISIBILITY_TOKENS);
        // TODO: Switch to `$previousToken['code'] === T_READONLY` when we require phpcs 3.7 or newer;
        $isReadonlyToken = strtolower($previousToken['content']) === 'readonly';

        return $isVisibility || $isReadonlyToken;
    }

    public static function isConstructorParameter(File $file, int $stackPointer): bool
    {
        return self::isMethodParameter($file, $stackPointer)
            && self::getMethodNameForParameter($file, $stackPointer) === '__construct';
    }

    public static function getMethodNameForParameter(File $file, int $stackPointer): string
    {
        $methodPointer = self::getMethodPointerForParameter($file, $stackPointer);

        return $file->getDeclarationName($methodPointer);
    }

    public static function getMethodPointerForParameter(File $file, int $stackPointer): int
    {
        if (!self::isMethodParameter($file, $stackPointer)) {
            throw new \LogicException('Given pointer is not method parameter');
        }

        $token = $file->getTokens()[$stackPointer];
        $nestedParenthesisPointer = array_pop($token['nested_parenthesis']);
        $nestedParenthesis = $file->getTokens()[$nestedParenthesisPointer];

        return $nestedParenthesis['parenthesis_owner'];
    }

    /**
     * @return string[] variables are prepended with $, e.g ['$a', '$b']
     */
    public static function getUsedVariables(File $file, int $start, ?int $end): array
    {
        $tokens = $file->getTokens();
        $variables = [];
        while ($tokenPointer = TokenHelper::findNext($file, self::TOKENS, $start, $end)) {
            $token = $tokens[$tokenPointer];

            switch ($token['code']) {
                case T_VARIABLE:
                    $variables[] = $token['content'];
                    $skipTo = $tokenPointer + 1;
                    break;
                case T_DOLLAR:
                    [$variable, $skipTo] = self::parseDollarToken($file, $tokenPointer);
                    if ($variable) {
                        $variables[] = $variable;
                    }

                    break;
                default:
                    [$content, $skipTo] = self::readMultiLineString($file, $tokenPointer, $end);
                    $variables = array_merge($variables, self::findVariablesInString($content));
            }

            $start = $skipTo;
        }

        return array_unique($variables);
    }

    /**
     * @return array{string, int} tuple of string content and last visited token pointer
     */
    private static function parseDollarToken(File $file, int $pointer): array
    {
        $tokens = $file->getTokens();
        $nextToken = TokenHelper::findNextExcluding($file, [T_WHITESPACE], $pointer + 1);

        if ($tokens[$nextToken]['code'] !== T_OPEN_CURLY_BRACKET) {
            return [null, $nextToken];
        }

        $nextToken = TokenHelper::findNextExcluding($file, [T_WHITESPACE], $nextToken + 1);

        if ($tokens[$nextToken]['code'] !== T_STRING) {
            return [null, $nextToken];
        }

        return ['$' . $tokens[$nextToken]['content'], $nextToken + 1];
    }

    /**
     * @return array{string, int} tuple: string content and last visited token pointer
     */
    private static function readMultiLineString(File $file, int $tokenPointer, ?int $end): array
    {
        $validTokens = array_merge(self::TOKENS, [
            T_HEREDOC              => T_HEREDOC,
            T_NOWDOC               => T_NOWDOC,
            T_END_HEREDOC          => T_END_HEREDOC,
            T_END_NOWDOC           => T_END_NOWDOC,
            T_DOUBLE_QUOTED_STRING => T_DOUBLE_QUOTED_STRING,
        ]);

        $tokens = $file->getTokens();
        $token = $tokens[$tokenPointer];
        $content = $token['content'];

        for ($i = $tokenPointer + 1; $i <= $end; $i++) {
            if (!isset($validTokens[$tokens[$i]['code']])) {
                break;
            }

            $content .= $tokens[$i]['content'];
        }

        return [$content, $i];
    }

    /**
     * @return string[]
     */
    private static function findVariablesInString(string $content): array
    {
        $variables = [];
        // phpcs:ignore Magento2.Functions.DiscouragedFunction -- no alternative
        $stringTokens = token_get_all(sprintf('<?php %s;?>', $content));
        foreach ($stringTokens as $stringPtr => $stringToken) {
            if (!is_array($stringToken)) {
                continue;
            }

            switch ($stringToken[0]) {
                case T_DOLLAR_OPEN_CURLY_BRACES:
                    $variables[] = '$' . $stringTokens[$stringPtr + 1][1];
                    break;
                case T_VARIABLE:
                    $variables[] = $stringToken[1];
                    break;
            }
        }

        return $variables;
    }
}
