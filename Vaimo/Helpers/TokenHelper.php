<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

declare(strict_types=1);

namespace Vaimo\Helpers;

use Closure;
use PHP_CodeSniffer\Files\File;

use function count;
use function in_array;

/**
 * The vast majority of functions in this helper are wrappers around File::findNext() and File::findPrevious().
 * API of those functions is, well, far from perfect, so that the helper is created.
 *
 * "Heavily inspired" by SlevomatCodingStandard\Helpers\TokenHelper,
 * although we can't just use it, because it is marked as internal.
 *
 * Slevomat indeed breaks their internal API from time to time :(
 *
 * All token list constant MUST follow the same pattern:
 * each item MUST have the same key and value.
 * It is needed to make sure performance optimisations can be done easier without big code changes.
 */
class TokenHelper
{
    public const VISIBILITY_TOKENS = [
        T_PRIVATE => T_PRIVATE,
        T_PROTECTED => T_PROTECTED,
        T_PUBLIC => T_PUBLIC,
    ];

    public const INEFFECTIVE_CODES = [
        T_WHITESPACE => T_WHITESPACE,
        T_COMMENT => T_COMMENT,
        T_DOC_COMMENT => T_DOC_COMMENT,
        T_DOC_COMMENT_OPEN_TAG => T_DOC_COMMENT_OPEN_TAG,
        T_DOC_COMMENT_CLOSE_TAG => T_DOC_COMMENT_CLOSE_TAG,
        T_DOC_COMMENT_STAR => T_DOC_COMMENT_STAR,
        T_DOC_COMMENT_STRING => T_DOC_COMMENT_STRING,
        T_DOC_COMMENT_TAG => T_DOC_COMMENT_TAG,
        T_DOC_COMMENT_WHITESPACE => T_DOC_COMMENT_WHITESPACE,
        T_PHPCS_DISABLE => T_PHPCS_DISABLE,
        T_PHPCS_ENABLE => T_PHPCS_ENABLE,
        T_PHPCS_IGNORE => T_PHPCS_IGNORE,
        T_PHPCS_IGNORE_FILE => T_PHPCS_IGNORE_FILE,
        T_PHPCS_SET => T_PHPCS_SET,
    ];

    public const COMMENT_TOKENS = [
        T_DOC_COMMENT_WHITESPACE => T_DOC_COMMENT_WHITESPACE,
        T_DOC_COMMENT => T_DOC_COMMENT,
        T_DOC_COMMENT_STRING => T_DOC_COMMENT_STRING,
        T_DOC_COMMENT_STAR => T_DOC_COMMENT_STAR,
        T_COMMENT => T_COMMENT,
    ];

    public static function getEffectiveContent(File $file, int $startPointer, int $endPointer): string
    {
        return self::getContent($file, $startPointer, $endPointer);
    }

    public static function getAllContent(File $file, int $startPointer, int $endPointer): string
    {
        return self::getContent($file, $startPointer, $endPointer, false);
    }

    private static function getContent(
        File $file,
        int $startPointer,
        int $endPointer,
        bool $onlyEffective = true
    ): string {
        $content = '';
        $tokens = $file->getTokens();

        for ($i = $startPointer; $i <= $endPointer; $i++) {
            if ($onlyEffective && in_array($tokens[$i]['code'], self::INEFFECTIVE_CODES)) {
                continue;
            }

            $content .= $tokens[$i]['content'];
        }

        return $content;
    }

    /**
     * @param Closure $closure (File $file, int $pointer) => bool
     */
    public static function readUntil(File $file, int $startPointer, Closure $closure): string
    {
        $content = '';
        $tokens = $file->getTokens();
        $tokensCount = count($tokens);

        for ($i = $startPointer; $i <= $tokensCount; $i++) {
            if (!$closure($file, $i)) {
                break;
            }

            $content .= $tokens[$i]['content'];
        }

        return $content;
    }

    public static function tokenHasScope(File $file, int $pointer): bool
    {
        $token = $file->getTokens()[$pointer] ?? null;

        if (!$token) {
            throw new \OutOfBoundsException('Token with given pointer does not exist.');
        }

        $scopeOpener = $token['scope_opener'] ?? null;
        $scopeCloser = $token['scope_closer'] ?? null;

        return $scopeOpener && $scopeCloser;
    }

    /**
     * @param int|int[] $types
     * @see File::findNext() for more details about $types
     */
    public static function findNextInTokenScope(File $file, int $scopePointer, $types): ?int
    {
        if (!self::tokenHasScope($file, $scopePointer)) {
            return null;
        }

        $scopeToken = $file->getTokens()[$scopePointer];

        $next = $file->findNext($types, $scopeToken['scope_opener'], $scopeToken['scope_closer']);

        return $next === false ? null : $next;
    }

    /**
     * @param array<int|string> $types
     */
    public static function findPrevious(File $file, array $types, int $startPointer, ?int $endPointer = null): ?int
    {
        $previous = $file->findPrevious($types, $startPointer, $endPointer);

        return $previous === false ? null : $previous;
    }

    /**
     * @param array<int|string> $types
     */
    public static function findPreviousContent(
        File $file,
        array $types,
        string $content,
        int $startPointer,
        ?int $endPointer = null
    ): ?int {
        $previous = $file->findPrevious($types, $startPointer, $endPointer, $exclude = false, $content);

        return $previous === false ? null : $previous;
    }

    public static function findPreviousEffective(File $file, int $startPointer, ?int $endPointer = null): ?int
    {
        return self::findPreviousExcluding($file, self::INEFFECTIVE_CODES, $startPointer, $endPointer);
    }

    /**
     * @param array<int|string> $types
     */
    public static function findPreviousExcluding(File $file, array $types, int $startPointer, ?int $endPointer): ?int
    {
        $previous = $file->findPrevious($types, $startPointer, $endPointer, $exclude = true);

        return $previous === false ? null : $previous;
    }

    /**
     * @param array<int|string> $types
     */
    public static function findNext(File $file, array $types, int $startPointer, ?int $endPointer = null): ?int
    {
        $next = $file->findNext($types, $startPointer, $endPointer);

        return $next === false ? null : $next;
    }

    /**
     * @param array<int|string> $types
     */
    public static function findNextContent(
        File $file,
        array $types,
        string $content,
        int $startPointer,
        ?int $endPointer = null
    ): ?int {
        $next = $file->findNext($types, $startPointer, $endPointer, $exclude = false, $content);

        return $next === false ? null : $next;
    }

    public static function findNextEffective(File $file, int $startPointer, ?int $endPointer = null): ?int
    {
        return self::findNextExcluding($file, self::INEFFECTIVE_CODES, $startPointer, $endPointer);
    }

    /**
     * @param array<int|string> $types
     */
    public static function findNextExcluding(File $file, array $types, int $startPointer, ?int $endPointer = null): ?int
    {
        $next = $file->findNext($types, $startPointer, $endPointer, $exclude = true);

        return $next === false ? null : $next;
    }

    public static function getLineContent(File $phpcsFile, int $line, bool $onlyEffective = true): string
    {
        $result = '';
        $tokens = $phpcsFile->getTokens();

        foreach ($tokens as $token) {
            if ($line > $token['line']) {
                continue;
            }

            if ($line < $token['line']) {
                break;
            }

            if ($onlyEffective && in_array($token['code'], self::INEFFECTIVE_CODES)) {
                continue;
            }

            $result .= $token['content'];
        }

        return $result;
    }
}
