<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

declare(strict_types=1);

namespace Vaimo\Helpers;

use PHP_CodeSniffer\Files\File;

use function end;
use function explode;
use function str_replace;

class UseStatementHelper
{
    public static function isClosureUse(File $file, int $usePointer): bool
    {
        $nextPointer = TokenHelper::findNextEffective($file, $usePointer + 1);
        $nextToken = $file->getTokens()[$nextPointer];

        return $nextToken['code'] === T_OPEN_PARENTHESIS;
    }

    /**
     * @return array<string, string> list of key-value pairs where key is an alias and value is import
     */
    public static function findImportsInFile(File $phpcsFile): array
    {
        $nextPosition = 0;
        $imports = [];

        while ($nextUse = TokenHelper::findNext($phpcsFile, [T_USE], $nextPosition)) {
            if (self::isClosureUse($phpcsFile, $nextUse)) {
                $nextPosition = $nextUse + 1;
                continue;
            }

            $useEnd = TokenHelper::findNext($phpcsFile, [T_SEMICOLON], $nextUse);

            foreach (self::parseImport($phpcsFile, $nextUse, $useEnd) as $alias => $import) {
                $imports[$alias] = $import;
            }

            $nextPosition = $nextUse + 1;
        }

        return $imports;
    }

    /**
     * @return array<string, string>
     */
    public static function parseImport(File $phpcsFile, int $useStart, int $useEnd): array
    {
        $openBracket = TokenHelper::findNext($phpcsFile, [T_OPEN_USE_GROUP], $useStart, $useEnd);
        if ($openBracket) {
            $prefix = TokenHelper::getEffectiveContent($phpcsFile, $useStart + 1, $openBracket - 1);
            $imports = [];

            $statementEnd = TokenHelper::findNext($phpcsFile, [T_CLOSE_USE_GROUP, T_COMMA], $openBracket, $useEnd);
            while ($statementEnd && $statementEnd < $useEnd) {
                foreach (self::parseImport($phpcsFile, $openBracket + 1, $statementEnd) as $alias => $import) {
                    $imports[$alias] = $prefix . $import;
                }

                $openBracket = $statementEnd + 1;
                $statementEnd = TokenHelper::findNext($phpcsFile, [T_CLOSE_USE_GROUP, T_COMMA], $openBracket, $useEnd);
            }

            return $imports;
        }

        // check if there is as and parse aliased import
        $as = TokenHelper::findNext($phpcsFile, [T_AS], $useStart, $useEnd);
        if ($as) {
            $import = self::getImportedEntity($phpcsFile, $useStart + 1, $as - 1);
            $alias = TokenHelper::getEffectiveContent($phpcsFile, $as + 1, $useEnd - 1);

            return [$alias => $import];
        }

        $import = self::getImportedEntity($phpcsFile, $useStart + 1, $useEnd - 1);
        $parts = explode('\\', $import);
        $alias = end($parts);

        return [$alias => $import];
    }

    private static function getImportedEntity(File $file, int $startPointer, int $endPointer): string
    {
        $import = TokenHelper::getEffectiveContent($file, $startPointer, $endPointer);

        if (!StringHelper::startsWith($import, 'function')) {
            return $import;
        }

        // Make sure we handle function imports correctly
        return str_replace('function', '', $import);
    }
}
