<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

declare(strict_types=1);

namespace Vaimo\Helpers;

use PHP_CodeSniffer\Files\File;
use Vaimo\ValueObject\HeredocProperties;

use function array_merge;
use function preg_match_all;
use function str_replace;
use function trim;

class HeredocHelper
{
    /**
     * @see VariableHelper::readMultiLineString() also
     */
    public static function collectUsedProperties(File $file, int $stringPointer): HeredocProperties
    {
        $token = $file->getTokens()[$stringPointer];

        $content = self::readStringContent($file, $stringPointer, $token['code']);
        $closer = TokenHelper::findNextExcluding($file, [$token['code']], $stringPointer);

        // It helps with parsing a lot
        $content = str_replace(["\n", ' '], ['', ''], trim($content));
        preg_match_all('~(?<!\\\\)\$this->(.+?\b)(?!\()~', $content, $matches, PREG_PATTERN_ORDER);
        $properties = $matches[1] ?? [];

        preg_match_all('~(?<!\\\\)\${this->(.+?\b)(?!\()~', $content, $matches, PREG_PATTERN_ORDER);
        $properties = array_merge($properties, $matches[1] ?? []);

        return new HeredocProperties($stringPointer, $closer, $properties);
    }

    /**
     * @param string $acc Don't use it outside, it's used only to make the function tail-recursive
     */
    private static function readStringContent(File $file, int $stringPointer, string $code, string $acc = ''): string
    {
        $content = $file->getTokens()[$stringPointer];

        if ($content['code'] !== $code) {
            return $acc;
        }

        return self::readStringContent($file, $stringPointer + 1, $code, $acc . $content['content']);
    }
}
