<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

declare(strict_types=1);

namespace Vaimo\ValueObject;

use PHP_CodeSniffer\Files\File;
use Vaimo\Helpers\VariableHelper;

final class ClassMember
{
    public const CONSTANT = T_CONST;
    public const PROPERTY = T_VARIABLE;
    public const METHOD = T_FUNCTION;

    private string $name;

    private int $type;

    private ?int $pointer;

    private ?int $memberEnd;

    /**
     * @var array<string, mixed>
     *
     * array{
     *      'scope': string, (will be present for functions, properties, and constants)
     *      'scope_specified': bool, (will be present for functions, properties, and constants)
     *      'is_static': bool, (used for functions and properties)
     *
     *      'type': string, (property specific data)
     *      'type_token': int,
     *      'type_end_token': int,
     *      'nullable_token': bool,
     *
     *      'return_type': string, (function specific data)
     *      'return_type_token': int|false,
     *      'return_type_end_token': int|false,
     *      'nullable_return_type': bool,
     *      'is_abstract': bool,
     *      'is_final': bool,
     *      'has_body': bool
     * }
     *
     * @see \PHP_CodeSniffer\Files\File::getMethodProperties()
     * @see \PHP_CodeSniffer\Files\File::getMemberProperties()
     * @see \Vaimo\Helpers\ConstantHelper::getConstantProperties()
     */
    private array $properties;

    /**
     * Yes, it is intentionally private. Use named constructors.
     *
     * @param array<string, mixed> $properties
     * @see ClassMember::$properties
     */
    private function __construct(
        string $name,
        int $type,
        ?int $pointer = null,
        ?int $memberEnd = null,
        array $properties = []
    ) {
        $this->name = $name;
        $this->type = $type;
        $this->pointer = $pointer;
        $this->memberEnd = $memberEnd;
        $this->properties = $properties;
    }

    /**
     * @param array<string, mixed> $properties
     * @see ClassMember::$properties
     */
    public static function createMethod(
        string $name,
        ?int $pointer = null,
        ?int $memberEnd = null,
        array $properties = []
    ): ClassMember {
        return new self($name, self::METHOD, $pointer, $memberEnd, $properties);
    }

    /**
     * @param array<string, mixed> $properties
     * @see ClassMember::$properties
     */
    public static function createProperty(
        string $name,
        ?int $pointer = null,
        ?int $memberEnd = null,
        array $properties = []
    ): ClassMember {
        return new self($name, self::PROPERTY, $pointer, $memberEnd, $properties);
    }

    /**
     * @param array<string, mixed> $methodParameter
     * Method implies `$methodParameter` is an array item from @see File::getMethodParameters() results
     *
     * Example of a use case:
     *
     * ```php
     * foreach ($file->getMethodParameters($constructorPointer) as $parameter) {
     *     if (!array_key_exists('property_visibility', $parameter)) {
     *         continue;
     *     }
     *
     *     $promotedProperties[] = ClassMember::createPromotedProperty($file, $parameter);
     * }
     * ```
     */
    public static function createPromotedProperty(File $file, array $methodParameter): ClassMember
    {
        $properties = [
            'scope' => $methodParameter['property_visibility'],
            'scope_specified' => true,
            'is_static' => false,
            'type' => $methodParameter['type_hint'] ?? '',
            'type_token' => $methodParameter['type_hint_token'] ?? false,
            'type_end_token' => $methodParameter['type_hint_end_token'] ?? false,
            'nullable_type' => $methodParameter['nullable_type'],
        ];

        return self::createProperty(
            VariableHelper::normalizeName($methodParameter['name']),
            $file->findStartOfStatement($methodParameter['token']),
            $file->findEndOfStatement($methodParameter['token']),
            $properties
        );
    }

    /**
     * @param array<string, mixed> $properties
     * @see ClassMember::$properties
     */
    public static function createConstant(
        string $name,
        ?int $pointer = null,
        ?int $memberEnd = null,
        array $properties = []
    ): ClassMember {
        return new self($name, self::CONSTANT, $pointer, $memberEnd, $properties);
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getType(): int
    {
        return $this->type;
    }

    public function getPointer(): ?int
    {
        return $this->pointer;
    }

    public function getMemberEndPointer(): ?int
    {
        return $this->memberEnd;
    }

    public function isConstant(): bool
    {
        return $this->getType() === self::CONSTANT;
    }

    public function isFunction(): bool
    {
        return $this->getType() === self::METHOD;
    }

    public function isProperty(): bool
    {
        return $this->getType() === self::PROPERTY;
    }

    /**
     * @return array<string, mixed>
     * @see ClassMember::$properties
     */
    public function getMemberProperties(): array
    {
        return $this->properties;
    }

    public function isClassConstructor(): bool
    {
        return $this->getType() === self::METHOD && $this->getName() === '__construct';
    }
}
