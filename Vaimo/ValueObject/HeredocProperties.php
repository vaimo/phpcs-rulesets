<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

declare(strict_types=1);

namespace Vaimo\ValueObject;

final class HeredocProperties
{
    private int $startPointer;

    private int $endPointer;

    /**
     * @var string[]
     */
    private array $properties;

    /**
     * @param string[] $properties
     */
    public function __construct(int $startPointer, int $endPointer, array $properties)
    {
        $this->startPointer = $startPointer;
        $this->endPointer = $endPointer;
        $this->properties = $properties;
    }

    public function getStartPointer(): int
    {
        return $this->startPointer;
    }

    public function getEndPointer(): int
    {
        return $this->endPointer;
    }

    /**
     * @return string[]
     */
    public function getProperties(): array
    {
        return $this->properties;
    }
}
