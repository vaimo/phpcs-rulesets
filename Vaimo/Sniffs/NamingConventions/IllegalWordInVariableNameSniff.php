<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

declare(strict_types=1);

namespace Vaimo\Sniffs\NamingConventions;

use PHP_CodeSniffer\Files\File;
use PHP_CodeSniffer\Sniffs\Sniff;
use Vaimo\Helpers\InheritanceHelper;
use Vaimo\Helpers\StringHelper;

use function in_array;
use function ltrim;

class IllegalWordInVariableNameSniff implements Sniff
{
    public const CODE_FOUND = 'Found';
    public const MESSAGE_FOUND = 'Variable name "%s" contains illegal keyword "%s"';

    /**
     * @var string[] lower cased illegal keywords
     */
    public array $illegalKeywords = [];

    /**
     * Whether to allow keywords in inherited properties.
     * True by default.
     * If parent function is in project scope, Sniff will trigger on parent.
     * Otherwise there is nothing we can do with it.
     */
    public bool $allowInheritedProperties = true;

    /**
     * What if this sniff is not able to check whether the method is inherited because of ReflectionException?
     *
     * * We can allow some false-positives (default behavior)
     * * or we can trust developer and consider the method as inherited on reflection errors.
     */
    public bool $considerInheritedOnReflectionError = false;

    /**
     * @inheritDoc
     */
    public function register()
    {
        return [T_VARIABLE];
    }

    /**
     * @inheritDoc
     */
    public function process(File $file, $pointer)
    {
        $variableName = ltrim($file->getTokens()[$pointer]['content'], '$');
        $words = StringHelper::splitCamelCaseToWords($variableName);

        foreach ($words as $word) {
            if (!in_array($word, $this->illegalKeywords)) {
                continue;
            }

            if ($this->allowInheritedProperties && $this->isInheritedProperty($file, $pointer)) {
                continue;
            }

            $file->addError(self::MESSAGE_FOUND, $pointer, self::CODE_FOUND, [
                $variableName,
                $word,
            ]);
        }
    }

    private function isInheritedProperty(File $file, int $pointer): bool
    {
        try {
            return InheritanceHelper::isInheritedMember($file, $pointer);
        } catch (\ReflectionException $e) {
            return $this->considerInheritedOnReflectionError;
        }
    }
}
