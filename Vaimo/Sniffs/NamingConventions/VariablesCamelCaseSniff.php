<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

declare(strict_types=1);

namespace Vaimo\Sniffs\NamingConventions;

use PHP_CodeSniffer\Files\File;
use PHP_CodeSniffer\Sniffs\AbstractVariableSniff;
use PHP_CodeSniffer\Util\Common;
use Vaimo\Helpers\VariableHelper;

use function array_key_exists;
use function ltrim;
use function sprintf;

class VariablesCamelCaseSniff extends AbstractVariableSniff
{
    public const PROPERTY_CODE = 'Property';
    public const PARAMETER_CODE = 'Parameter';
    public const VARIABLE_CODE = 'Variable';

    /**
     * @inheritDoc
     */
    protected function processMemberVar(File $phpcsFile, $stackPtr)
    {
        $propertyName = ltrim($phpcsFile->getTokens()[$stackPtr]['content'], '$');
        // we should not raise an error if property name starts from underscore
        // It might be another issue, but we should make sure it's fine for this Sniff.
        $propertyName = ltrim($propertyName, '_');

        if (Common::isCamelCaps($propertyName, false, true, false)) {
            return;
        }

        $this->addError($phpcsFile, self::PROPERTY_CODE, $propertyName, $stackPtr);
    }

    /**
     * @inheritDoc
     */
    protected function processVariable(File $phpcsFile, $stackPtr)
    {
        $variableName = ltrim($phpcsFile->getTokens()[$stackPtr]['content'], '$');

        if (array_key_exists($variableName, $this->phpReservedVars)) {
            return;
        }

        if (Common::isCamelCaps($variableName, false, true, false)) {
            return;
        }

        $code = VariableHelper::isMethodParameter($phpcsFile, $stackPtr)
            ? self::PARAMETER_CODE
            : self::VARIABLE_CODE;

        $this->addError($phpcsFile, $code, $variableName, $stackPtr);
    }

    /**
     * @inheritDoc
     */
    protected function processVariableInString(File $phpcsFile, $stackPtr)
    {
        // Nothing to do here
        // if variable is used in a string, it must be declared
        // Therefore, it is already checked.
    }

    private function addError(File $phpcsFile, string $code, string $variableName, int $stackPtr): void
    {
        $phpcsFile->addError(
            sprintf('%s "%s" must be in camelCase format', $code, $variableName),
            $stackPtr,
            $code
        );
    }
}
