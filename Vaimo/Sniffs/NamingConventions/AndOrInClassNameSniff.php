<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

declare(strict_types=1);

namespace Vaimo\Sniffs\NamingConventions;

// phpcs:ignore Vaimo.NamingConventions.AndOrInClassName -- intentional
class AndOrInClassNameSniff extends IllegalWordInClassNameSniff
{
    /**
     * @var string[]
     */
    public array $illegalKeywords = [
        'or',
        'and',
    ];
}
