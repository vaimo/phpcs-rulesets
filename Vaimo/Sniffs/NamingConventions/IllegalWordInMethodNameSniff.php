<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

declare(strict_types=1);

namespace Vaimo\Sniffs\NamingConventions;

use PHP_CodeSniffer\Files\File;
use PHP_CodeSniffer\Sniffs\Sniff;
use Vaimo\Helpers\FunctionHelper;
use Vaimo\Helpers\InheritanceHelper;
use Vaimo\Helpers\StringHelper;

use function in_array;

class IllegalWordInMethodNameSniff implements Sniff
{
    public const CODE_FOUND = 'Found';
    public const MESSAGE_FOUND = 'Function name "%s" contains illegal keyword "%s".';

    /**
     * @var string[] lower cased illegal keywords
     */
    public array $illegalKeywords = [];

    /**
     * Whether to allow keywords in inherited functions.
     * True by default.
     * If parent function is in project scope, Sniff will trigger on parent.
     * Otherwise there is nothing we can do with it.
     */
    public bool $allowInheritedMethods = true;

    /**
     * What if this sniff is not able to check whether the method is inherited because of ReflectionException?
     *
     * * We can allow some false-positives (default behavior)
     * * or we can trust developer and consider the method as inherited on reflection errors.
     */
    public bool $considerInheritedOnReflectionError = false;

    /**
     * @inheritDoc
     */
    public function register()
    {
        return [T_FUNCTION];
    }

    /**
     * @inheritDoc
     */
    public function process(File $file, $pointer)
    {
        if ($this->allowInheritedMethods && $this->isInheritedMethod($file, $pointer)) {
            return;
        }

        $functionName = FunctionHelper::getName($file, $pointer);
        $words = StringHelper::splitCamelCaseToWords($functionName);

        foreach ($words as $word) {
            if (!in_array($word, $this->illegalKeywords)) {
                continue;
            }

            $file->addError(self::MESSAGE_FOUND, $pointer, self::CODE_FOUND, [$functionName, $word]);
        }
    }

    private function isInheritedMethod(File $file, int $pointer): bool
    {
        try {
            return InheritanceHelper::isInheritedMember($file, $pointer);
        } catch (\ReflectionException $e) {
            return $this->considerInheritedOnReflectionError;
        }
    }
}
