<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

declare(strict_types=1);

namespace Vaimo\Sniffs\NamingConventions;

use PHP_CodeSniffer\Files\File;
use Vaimo\Helpers\StringHelper;

use function in_array;

class IllegalWordInClassNameSniff
{
    public const CODE_FOUND = 'Found';
    public const MESSAGE_FOUND = 'Class name "%s" contains illegal keyword "%s".';

    /**
     * @var string[] lower cased illegal keywords
     */
    public array $illegalKeywords = [];

    /**
     * @inheritDoc
     */
    public function register()
    {
        return [T_CLASS];
    }

    /**
     * @inheritDoc
     */
    public function process(File $file, $pointer)
    {
        $className = $file->getDeclarationName($pointer);
        $words = StringHelper::splitCamelCaseToWords($className);

        foreach ($words as $word) {
            if (!in_array($word, $this->illegalKeywords)) {
                continue;
            }

            $file->addError(self::MESSAGE_FOUND, $pointer, self::CODE_FOUND, [$className, $word]);
        }
    }
}
