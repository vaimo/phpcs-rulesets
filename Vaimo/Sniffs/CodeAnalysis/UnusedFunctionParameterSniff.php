<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

declare(strict_types=1);

namespace Vaimo\Sniffs\CodeAnalysis;

use PHP_CodeSniffer\Files\File;
use PHP_CodeSniffer\Sniffs\Sniff;
use Vaimo\Helpers\InheritanceHelper;
use Vaimo\Helpers\TokenHelper;
use Vaimo\Helpers\VariableHelper;

use function array_filter;
use function array_key_exists;
use function in_array;
use function strpos;

/**
 * Reimplementation of
 * \PHP_CodeSniffer\Standards\Generic\Sniffs\CodeAnalysis\UnusedFunctionParameterSniff
 *
 * Why do we need to create new Sniff?
 *
 * * You can allow unused parameters that present in a function
 *   that is inherited from parent class of interface;
 * * You can allow unused parameters in functions that look like Magento Plugin functions
 *   (e.g. allow*, before*, around*)
 * * You can allow certain parameter names to be always ignored
 *
 * All configuration options are declared as public fields to be configurable from phpcs.xml
 */
class UnusedFunctionParameterSniff implements Sniff
{
    public const ERROR_CODE = 'Found';

    /**
     * If function name starts with something from this list, it is allowed to have unused parameters
     * @var string[]
     */
    public array $allowedMethodPrefixes = [
        'before',
        'around',
        'after',
    ];

    /**
     * List of parameter names that are always allows. Note that names MUST start from $.
     * @var string[]
     */
    public array $allowedMethodParameters = [
        '$subject',
        '$proceed',
    ];

    /**
     * Whether to allow unused parameters if method is inherited from another class
     */
    public bool $allowUnusedInheritedParameters = true;

    /**
     * If Sniff is not able to read list of inherited properties because of reflection error, what shall we do?
     *
     * We can only either allow some false-positives (default behavior)
     * or trust developer and think that all protected members are inherited.
     */
    public bool $considerInheritedOnReflectionError = false;

    /**
     * @inheritDoc
     */
    public function register()
    {
        return [T_FUNCTION, T_CLOSURE, T_FN];
    }

    /**
     * @inheritDoc
     */
    public function process(File $phpcsFile, $stackPtr)
    {
        $tokens = $phpcsFile->getTokens();
        $token  = $tokens[$stackPtr];

        $scopeOpener = $token['scope_opener'] ?? false;
        $scopeCloser = $token['scope_closer'] ?? false;
        $parenthesisOpener = $token['parenthesis_opener'] ?? false;
        $parenthesisCloser = $token['parenthesis_closer'] ?? false;

        // Skip broken or abstract function declarations.
        if (!($scopeOpener && $scopeCloser && $parenthesisOpener && $parenthesisCloser)) {
            return $stackPtr + 1;
        }

        if ($token['code'] === T_FUNCTION && $this->isAllowedMethodName($phpcsFile, $stackPtr)) {
            return $scopeCloser + 1;
        }

        $parameters = $this->buildParameters($phpcsFile, $stackPtr);

        if (empty($parameters)) {
            return $scopeCloser + 1; // No parameters - no need to check them
        }

        $usedVariables = VariableHelper::getUsedVariables($phpcsFile, $scopeOpener, $scopeCloser);

        foreach ($usedVariables as $variableName) {
            unset($parameters[$variableName]);
        }

        foreach ($parameters as $paramName => $position) {
            $phpcsFile->addError(
                'The method parameter %s is never used',
                $position,
                self::ERROR_CODE,
                [$paramName]
            );
        }

        return $scopeCloser + 1;
    }

    private function isAllowedMethodName(File $file, int $stackPtr): bool
    {
        $methodName = $this->getMethodName($file, $stackPtr);

        foreach ($this->allowedMethodPrefixes as $allowedPrefix) {
            if (strpos($methodName, $allowedPrefix) === 0) {
                return true;
            }
        }

        return false;
    }

    private function getMethodName(File $file, int $stackPtr): ?string
    {
        $methodNamePointer = TokenHelper::findNext($file, [T_STRING], $stackPtr);

        return $methodNamePointer !== false
            ? $file->getTokens()[$methodNamePointer]['content']
            : null;
    }

    /**
     * @return int[] [$parameterName => $stackPointer]
     */
    private function buildParameters(File $file, int $stackPtr): array
    {
        $parameters = $this->parseParametersList($file, $stackPtr);

        if ($this->allowUnusedInheritedParameters) {
            $parameters = $this->excludeInheritedParameters($file, $stackPtr, $parameters);
        }

        // filter out allowed parameter names
        $parameters = array_filter(
            $parameters,
            fn($name) => !in_array($name, $this->allowedMethodParameters),
            ARRAY_FILTER_USE_KEY
        );

        return $parameters;
    }

    /**
     * @return int[] [$parameterName => $stackPointer]
     */
    private function parseParametersList(File $file, int $pointer): array
    {
        $params = [];
        foreach ($file->getMethodParameters($pointer) as $param) {
            if ($this->isPromotedProperty($param)) {
                continue;
            }

            $params[$param['name']] = $param['token'];
        }

        return $params;
    }

    /**
     * @param array<string, mixed> $param
     * @see File::getMethodParameters() for more details about $param type
     */
    private function isPromotedProperty(array $param): bool
    {
        return array_key_exists('property_visibility', $param);
    }

    /**
     * @param int[] $parameters [$parameterName => $stackPointer]
     * @return int[] [$parameterName => $stackPointer]
     */
    private function excludeInheritedParameters(File $file, int $stackPtr, array $parameters): array
    {
        try {
            $inheritedParameters = InheritanceHelper::collectInheritedMethodParameters($file, $stackPtr);
        } catch (\ReflectionException $e) {
            return $this->considerInheritedOnReflectionError ? [] : $parameters;
        }

        foreach ($inheritedParameters as $inheritedParameter) {
            unset($parameters['$' . $inheritedParameter]);
        }

        return $parameters;
    }
}
