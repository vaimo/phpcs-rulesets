<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

declare(strict_types=1);

namespace Vaimo\Sniffs\CodeAnalysis;

use PHP_CodeSniffer\Files\File;
use PHP_CodeSniffer\Sniffs\Sniff;
use Vaimo\Helpers\CommentHelper;
use Vaimo\Helpers\TokenHelper;

use function count;
use function in_array;
use function preg_match;

class CopyrightSniff implements Sniff
{
    public const CODE_MISSING = 'Missing';
    public const MESSAGE_MISSING = 'Copyright is missing on file header';

    public const CODE_INCORRECT = 'IncorrectContent';
    public const MESSAGE_INCORRECT = "Wrong Copyright Disclaimer. Please use following copyright text instead: \n%s";

    /**
     * Regexp to check if actual copyright matches to.
     */
    public string $pattern = '/^\* Copyright (?i:\x{00A9}|\(c\)) Vaimo Group\. All rights reserved\.\n'
                        . ' \* See LICENSE_VAIMO\.txt for license details\./um';

    /**
     * Correct copyright content
     *
     *
     * Used to be more readable HEREDOC, but had to switch to this due to
     * @see https://github.com/slevomat/coding-standard/issues/1355
     */
    public string $example = "/**\n * Copyright © Vaimo Group. All rights reserved.\n"
                        . " * See LICENSE_VAIMO.txt for license details.\n */";

    /**
     * @inheritDoc
     */
    public function register()
    {
        return [T_OPEN_TAG];
    }

    /**
     * @inheritDoc
     */
    public function process(File $file, $pointer)
    {
        $commentStartPointer = TokenHelper::findNextExcluding($file, [T_WHITESPACE], $pointer + 1);

        if (!$commentStartPointer) {
            // can happen during live edit
            return count($file->getTokens()) + 1;
        }

        $nextToken = $file->getTokens()[$commentStartPointer];
        if (!in_array($nextToken['code'], [T_DOC_COMMENT_OPEN_TAG, T_COMMENT], true)) {
            $file->addError(self::MESSAGE_MISSING, $commentStartPointer, self::CODE_MISSING);

            return count($file->getTokens()) + 1;
        }

        $this->checkCommentContent($file, $commentStartPointer);

        return count($file->getTokens()) + 1;
    }

    private function checkCommentContent(File $file, int $pointer): void
    {
        $content = CommentHelper::getContent($file, $pointer + 1);

        if (preg_match($this->pattern, $content)) {
            return;
        }

        $file->addError(self::MESSAGE_INCORRECT, $pointer, self::CODE_INCORRECT, [$this->example]);
    }
}
