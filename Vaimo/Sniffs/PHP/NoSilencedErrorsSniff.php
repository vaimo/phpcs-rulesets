<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

declare(strict_types=1);

namespace Vaimo\Sniffs\PHP;

use PHP_CodeSniffer\Files\File;
use PHP_CodeSniffer\Standards\Generic\Sniffs\PHP\NoSilencedErrorsSniff as GenericNoSilencedErrorsSniff;
use Vaimo\Helpers\TokenHelper;
use Vaimo\Helpers\UseStatementHelper;

use function in_array;

/**
 * Reimplements Generic.PHP.NoSilencedErrors to allow to silence some functions.
 */
class NoSilencedErrorsSniff extends GenericNoSilencedErrorsSniff
{
    /**
     * @var string[]
     */
    public array $allowedFunctions = [
        'trigger_error',
    ];

    /**
     * @inheritDoc
     */
    public function process(File $file, $pointer)
    {
        $subjectToken = $this->findSubjectToken($file, $pointer);

        $isFunctionCall = $subjectToken['code'] === T_STRING;
        $isAllowedFunction = in_array(
            $this->getFullyQualifiedFunctionName($file, $subjectToken['content']),
            $this->allowedFunctions
        );

        if ($isFunctionCall && $isAllowedFunction) {
            return;
        }

        parent::process($file, $pointer);
    }

    /**
     * @return mixed[] phpcs token
     */
    private function findSubjectToken(File $file, int $pointer): array
    {
        $subjectPointer = TokenHelper::findNextEffective($file, $pointer + 1);
        $subjectToken = $file->getTokens()[$subjectPointer];

        if ($subjectToken['code'] === T_NS_SEPARATOR) {
            return $this->findSubjectToken($file, $subjectPointer);
        }

        return $subjectToken;
    }

    private function getFullyQualifiedFunctionName(File $file, string $referencedFunctionName): string
    {
        $useStatements = UseStatementHelper::findImportsInFile($file);

        foreach ($useStatements as $alias => $import) {
            if ($alias === $referencedFunctionName) {
                return $import;
            }
        }

        return $referencedFunctionName;
    }
}
