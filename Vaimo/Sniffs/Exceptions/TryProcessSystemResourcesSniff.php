<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

declare(strict_types=1);

namespace Vaimo\Sniffs\Exceptions;

use PHP_CodeSniffer\Files\File;
use PHP_CodeSniffer\Sniffs\Sniff;

use function strpos;

class TryProcessSystemResourcesSniff implements Sniff
{
    public const MESSAGE_FOUND = 'The code must be wrapped with a try block if the method uses system resources.';
    public const CODE_FOUND = 'MissingTryCatch';

    private const FUNCTIONS_TO_VALIDATE = [
        'stream_',
        'socket_',
    ];

    /**
     * @inheritDoc
     */
    public function register()
    {
        return [T_STRING];
    }

    /**
     * @inheritDoc
     */
    public function process(File $phpcsFile, $stackPtr)
    {
        $tokens = $phpcsFile->getTokens();

        foreach (self::FUNCTIONS_TO_VALIDATE as $function) {
            if (strpos($tokens[$stackPtr]['content'], $function) !== 0) {
                continue;
            }

            $useStart = $phpcsFile->findPrevious(T_USE, $stackPtr);
            $useEnd = $useStart ? $phpcsFile->findNext(T_SEMICOLON, $useStart) : false;

            if ($useStart !== false && $useEnd !== false && $stackPtr > $useStart && $stackPtr < $useEnd) {
                continue;
            }

            $tryPosition = $phpcsFile->findPrevious(T_TRY, $stackPtr - 1);

            if ($tryPosition === false) {
                $phpcsFile->addError(self::MESSAGE_FOUND, $stackPtr, self::CODE_FOUND);

                return;
            }

            $tryTag = $tokens[$tryPosition];
            $start = $tryTag['scope_opener'];
            $end = $tryTag['scope_closer'];
            if ($stackPtr > $start && $stackPtr < $end) {
                // element is wrapped by try no check required
                return;
            }

            $phpcsFile->addError(self::MESSAGE_FOUND, $stackPtr, self::CODE_FOUND);
        }
    }
}
