<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

declare(strict_types=1);

namespace Vaimo\Sniffs\Comments;

use PHP_CodeSniffer\Files\File;
use PHP_CodeSniffer\Sniffs\Sniff;
use Vaimo\Helpers\ClassHelper;
use Vaimo\Helpers\CommentHelper;
use Vaimo\Helpers\FunctionHelper;
use Vaimo\Helpers\MemberHelper;
use Vaimo\Helpers\TokenHelper;

class DeprecationNoticeSniff implements Sniff
{
    public const CODE_CONTENT_MISSING = 'ContentMissing';
    public const MESSAGE_CONTENT_MISSING = '@deprecated annotation has no content. '
        . 'Please provide an alternative in annotation content.';

    public const CODE_ERROR_MISSING = 'ErrorMissing';
    public const MESSAGE_ERROR_MISSING_CLASS = 'Class %s is marked as deprecated, '
        . 'but it does not have trigger_error() call in constructor. '
        . 'Please call `@trigger_error($deprecationNotice, E_USER_DEPRECATED)` in constructor. '
        . 'It makes deprecation notices more visible.';

    public const MESSAGE_ERROR_MISSING_FUNCTION = 'Function %s is marked as deprecated, '
        . 'but it does not have trigger_error() call inside. '
        . 'Please call `@trigger_error($deprecationNotice, E_USER_DEPRECATED)` in function body. '
        . 'It makes deprecation notices more visible.';

    /**
     * @inheritdoc
     */
    public function register()
    {
        return [T_DOC_COMMENT_OPEN_TAG];
    }

    /**
     * @inheritdoc
     */
    public function process(File $file, $docCommentOpenPointer)
    {
        $annotationPointer = CommentHelper::getFirstAnnotationPointer($file, $docCommentOpenPointer, '@deprecated');

        if (!$annotationPointer) {
            return;
        }

        $annotationContent = CommentHelper::getAnnotationContent($file, $annotationPointer);

        if (empty($annotationContent)) {
            $file->addError(
                self::MESSAGE_CONTENT_MISSING,
                $annotationPointer,
                self::CODE_CONTENT_MISSING
            );
        }

        $this->checkIfErrorTriggeredInCommentSubject(
            $file,
            $docCommentOpenPointer,
            $annotationPointer
        );
    }

    private function checkIfErrorTriggeredInCommentSubject(
        File $file,
        int $docCommentPointer,
        int $annotationPointer
    ): void {
        $subjectPointer = $this->findCommentSubject($file, $docCommentPointer);

        if (!$subjectPointer) {
            return;
        }

        $subjectToken = $file->getTokens()[$subjectPointer];

        switch ($subjectToken['code']) {
            case T_VARIABLE:
                break;
            case T_FUNCTION:
                if (!$this->functionHasErrorTriggered($file, $subjectPointer)) {
                    $file->addError(
                        self::MESSAGE_ERROR_MISSING_FUNCTION,
                        $annotationPointer,
                        self::CODE_ERROR_MISSING,
                        [FunctionHelper::getFullyQualifiedName($file, $subjectPointer)]
                    );
                }

                break;
            case T_CLASS:
                if (!$this->classHasErrorTriggered($file, $subjectPointer)) {
                    $file->addError(
                        self::MESSAGE_ERROR_MISSING_CLASS,
                        $annotationPointer,
                        self::CODE_ERROR_MISSING,
                        [ClassHelper::getFullyQualifiedName($file, $subjectPointer)]
                    );
                }
        }
    }

    private function findCommentSubject(File $file, int $docCommentPointer): ?int
    {
        return TokenHelper::findNext($file, [T_CLASS, T_FUNCTION, T_VARIABLE], $docCommentPointer);
    }

    private function findConstructorPointer(File $file, int $subjectPointer): ?int
    {
        $functions = MemberHelper::collectDeclaredMethods($file, $subjectPointer);

        foreach ($functions as $function) {
            if ($function->getName() !== '__construct') {
                continue;
            }

            return $function->getPointer();
        }

        return null;
    }

    private function classHasErrorTriggered(File $file, int $classPointer): bool
    {
        $constructorPointer = $this->findConstructorPointer($file, $classPointer);

        if (!$constructorPointer) {
            return false;
        }

        return $this->functionHasErrorTriggered($file, $constructorPointer);
    }

    private function functionHasErrorTriggered(File $file, int $functionPointer): bool
    {
        $token = $file->getTokens()[$functionPointer];

        if (!isset($token['scope_opener']) || !isset($token['scope_closer'])) {
            return true; // It seems to be an interface function or live edit
        }

        $triggerErrorPointer = $this->findTriggerErrorCall($file, $token['scope_opener'], $token['scope_closer']);

        if (!$triggerErrorPointer) {
            return false;
        }

        [$parenthesisOpener, $parenthesisCloser] = $this->findTriggerErrorCallParentheses($file, $triggerErrorPointer);
        $deprecatedErrorPointer = TokenHelper::findPreviousContent(
            $file,
            [T_STRING],
            'E_USER_DEPRECATED',
            $parenthesisCloser,
            $parenthesisOpener
        );

        return $deprecatedErrorPointer !== null;
    }

    private function findTriggerErrorCall(File $file, int $startPointer, int $endPointer): ?int
    {
        return TokenHelper::findNextContent($file, [T_STRING], 'trigger_error', $startPointer, $endPointer);
    }

    /**
     * @return int[] tuple of int representing open and close pointers for parentheses
     */
    private function findTriggerErrorCallParentheses(File $file, int $triggerErrorPointer): array
    {
        $openPointer = TokenHelper::findNext($file, [T_OPEN_PARENTHESIS], $triggerErrorPointer);
        $openToken = $file->getTokens()[$openPointer];

        return [$openToken['parenthesis_opener'], $openToken['parenthesis_closer']];
    }
}
