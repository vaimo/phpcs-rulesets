<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

declare(strict_types=1);

namespace Vaimo\Sniffs\TypeHints;

use PHP_CodeSniffer\Files\File;
use PHP_CodeSniffer\Sniffs\Sniff;
use Vaimo\Helpers\CommentHelper;

use function explode;
use function implode;
use function in_array;

class IllegalReturnTypeAnnotationSniff implements Sniff
{
    public const CODE_FOUND = 'Found';
    public const MESSAGE_FOUND = 'Found illegal return type annotation: %s';

    /**
     * @var string[]
     */
    public array $illegalReturnTypes = [
        'mixed',
    ];

    /**
     * @inheritDoc
     */
    public function register()
    {
        return [T_FUNCTION];
    }

    /**
     * @inheritDoc
     */
    public function process(File $file, $pointer)
    {
        $phpDocPointer = CommentHelper::findDocCommentOpener($file, $pointer);

        if (!$phpDocPointer) {
            return;
        }

        foreach (CommentHelper::findAnnotationPointers($file, $phpDocPointer, '@return') as $annotationPointer) {
            $returnType = CommentHelper::getAnnotationContent($file, $annotationPointer);
            $illegalTypes = [];

            foreach (explode('|', $returnType) as $type) {
                if (!in_array($type, $this->illegalReturnTypes, true)) {
                    continue;
                }

                $illegalTypes[] = $type;
            }

            if (empty($illegalTypes)) {
                continue;
            }

            $file->addError(
                self::MESSAGE_FOUND,
                $annotationPointer,
                self::CODE_FOUND,
                [implode(',', $illegalTypes)]
            );
        }
    }
}
