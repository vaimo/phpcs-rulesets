<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

declare(strict_types=1);

namespace Vaimo\Sniffs\Classes;

use PHP_CodeSniffer\Files\File;
use PHP_CodeSniffer\Sniffs\Sniff;
use Vaimo\Helpers\ClassHelper;
use Vaimo\Helpers\HeredocHelper;
use Vaimo\Helpers\InheritanceHelper;
use Vaimo\Helpers\MemberHelper;
use Vaimo\Helpers\TokenHelper;
use Vaimo\ValueObject\ClassMember;

use function array_map;
use function array_merge;
use function in_array;

/**
 * This Sniff looks for undefined class members:
 *
 * ```php
 * class AwesomeClass {
 *      private $vi;
 *
 *      public function __construct(Vi $vi) {
 *          $this->ui = $vi;
 *      }
 * }
 * ```
 *
 * How easy is it to find this typo? Easy, if you use this Sniff :)
 */
class UndefinedPropertySniff implements Sniff
{
    public const CODE_ERROR = 'UndefinedProperty';
    public const CODE_ERROR_HEREDOC = 'UndefinedPropertyInHeredoc';

    /**
     * If Sniff is not able to read list of inherited properties because of reflection error, what shall we do?
     *
     * We can only either allow some false-positives (default behavior)
     * or trust developer and think that all undeclared properties are inherited.
     */
    public bool $skipOnReflectionError = false;

    /**
     * @inheritDoc
     */
    public function register(): array
    {
        return ClassHelper::CLASS_TOKEN_CODES;
    }

    /**
     * @inheritDoc
     */
    public function process(File $phpcsFile, $stackPtr)
    {
        $opener = $phpcsFile->getTokens()[$stackPtr]['scope_opener'] ?? false;
        $closer = $phpcsFile->getTokens()[$stackPtr]['scope_closer'] ?? false;

        if ($opener === false || $closer === false) {
            return; // Skip the rest of the file, it's parse error
        }

        $declaredProperties = MemberHelper::collectDeclaredProperties($phpcsFile, $stackPtr);

        try {
            $declaredProperties = array_merge(
                $declaredProperties,
                InheritanceHelper::collectInheritedMembers($phpcsFile, $stackPtr, [ClassMember::PROPERTY])
            );
        } catch (\ReflectionException $e) {
            if ($this->skipOnReflectionError) {
                throw $e;
            }
        }

        $declaredProperties = array_map(static fn(ClassMember $member) => $member->getName(), $declaredProperties);

        foreach ($this->collectUsedProperties($phpcsFile, $opener, $closer) as [$pointer, $property]) {
            if (in_array($property, $declaredProperties)) {
                continue;
            }

            $phpcsFile->addError(
                'Property "%s" is not declared in class %s',
                $pointer,
                self::CODE_ERROR,
                [$property, ClassHelper::getCurrentClassFullyQualifiedName($phpcsFile, $stackPtr)]
            );
        }

        foreach ($this->collectHeredocProperties($phpcsFile, $opener, $closer) as [$pointer, $property]) {
            if (in_array($property, $declaredProperties)) {
                continue;
            }

            $phpcsFile->addError(
                'Property "%s" is not declared in class %s and used in heredoc starting on given line',
                $pointer,
                self::CODE_ERROR_HEREDOC,
                [$property, ClassHelper::getCurrentClassFullyQualifiedName($phpcsFile, $stackPtr)]
            );
        }
    }

    /**
     * @return mixed[] array of tuples [int $pointer, string $propertyName]
     */
    private function collectUsedProperties(File $file, int $start, int $scopeCloser): array
    {
        $usedProperties = [];
        $tokens = $file->getTokens();
        for ($i = $start; $i <= $scopeCloser; $i++) {
            $token = $tokens[$i];

            if (ClassHelper::isAnonymousClass($file, $i)) {
                $i = $token['scope_closer'];
                // We should skip entire anonymous class we found in current scope: phpcs will validate it later on
                continue;
            }

            if ($token['code'] !== T_OBJECT_OPERATOR) {
                continue;
            }

            [$pointer, $property] = $this->parseObjectOperator($file, $i, $scopeCloser);

            if (!$property) {
                continue;
            }

            $usedProperties[] = [$pointer, $property['content']];
        }

        return $usedProperties;
    }

    /**
     * @return mixed[] array of tuples [int $pointer, string $propertyName]
     */
    private function collectHeredocProperties(File $file, int $start, int $scopeCloser): array
    {
        $usedProperties = [];
        $stringTokens = [T_HEREDOC, T_DOUBLE_QUOTED_STRING, T_NOWDOC];

        while ($stringPointer = TokenHelper::findNext($file, $stringTokens, $start, $scopeCloser)) {
            $heredocProperties = HeredocHelper::collectUsedProperties($file, $stringPointer);

            foreach ($heredocProperties->getProperties() as $property) {
                $usedProperties[] = [$heredocProperties->getStartPointer(), $property];
            }

            $start = $heredocProperties->getEndPointer() ?? $stringPointer + 1;
        }

        return $usedProperties;
    }

    /**
     * @return mixed[] array of tuples [int $pointer, string $propertyName]
     */
    private function parseObjectOperator(File $file, int $index, int $scopeCloser): array
    {
        $referenceIndex = TokenHelper::findPreviousEffective($file, $index - 1);
        $referenceToken = $file->getTokens()[$referenceIndex];

        if (!($referenceToken['code'] === T_VARIABLE && $referenceToken['content'] === '$this')) {
            return [null, null];
        }

        $member = TokenHelper::findNextEffective($file, $index + 1, $scopeCloser);
        $memberToken = $file->getTokens()[$member];
        if ($memberToken['code'] !== T_STRING) {
            return [null, null]; //variable name is a string in that case
        }

        $afterMember = TokenHelper::findNextEffective($file, $member + 1, $scopeCloser);

        return $afterMember !== false && $file->getTokens()[$afterMember]['code'] !== T_OPEN_PARENTHESIS
            ? [$member, $memberToken]
            : [null, null];
    }
}
