<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

declare(strict_types=1);

namespace Vaimo\Sniffs\Classes;

use PHP_CodeSniffer\Files\File;
use PHP_CodeSniffer\Sniffs\Sniff;
use ReflectionClass;
use Vaimo\Helpers\ClassHelper;
use Vaimo\Helpers\NamespaceHelper;
use Vaimo\Helpers\StringHelper;
use Vaimo\Helpers\TokenHelper;
use Vaimo\Helpers\UseStatementHelper;

use function class_exists;
use function class_implements;
use function in_array;

/**
 * Reimplements MEQP2.Classes.ObjectInstantiation to allow edge cases:
 *
 * * Final classes
 * * Exceptions
 * * Named constructors
 */
class ObjectInstantiationSniff implements Sniff
{
    // phpcs:ignore Generic.Files.LineLength
    public const ERROR_MESSAGE_VARIABLE = 'Direct object instantiation is discouraged. Class name is stored in variable or method: %s';
    public const ERROR_MESSAGE = 'Direct object instantiation (object of %s) is discouraged.';

    public const ERROR_CODE = 'FoundDirectInstantiation';
    public const ERROR_ANONYMOUS_MESSAGE = 'Instantiation of anonymous clases is discouraged.';

    public const ERROR_ANONYMOUS_CODE = 'FoundAnonymousClassInstantiation';
    public const REFLECTION_ERROR_MESSAGE = 'Reflection error: %s.';

    public const REFLECTION_ERROR_CODE = 'ReflectionError';

    public bool $allowInstantiateFinalClasses = true;

    public bool $allowAnonymousClasses = true;

    public bool $allowNamedConstructors = true;

    public bool $reportReflectionError = false;

    /**
     * If Sniff is not able to check whether instantiated class is final, what shall we do?
     *
     * * Allow some false-positive alarms (default behavior)
     * * or trust developer and consider in final anyway
     */
    public bool $considerFinalInstantiationOnReflectionError = false;

    /**
     * @var string[]
     */
    public array $allowedClasses = [
        \DateTime::class,
        \DateTimeImmutable::class,
        \DateTimeZone::class,
        \DateInterval::class,
    ];

    /**
     * @inheritDoc
     */
    public function register()
    {
        return [T_NEW];
    }

    /**
     * @inheritDoc
     */
    public function process(File $phpcsFile, $stackPtr)
    {
        if ($this->isAnonymousClass($phpcsFile, $stackPtr)) {
            $this->handleAnonymousClassInstantiation($phpcsFile, $stackPtr);

            return;
        }

        if ($this->isExpression($phpcsFile, $stackPtr)) {
            $this->handleExpression($phpcsFile, $stackPtr);

            return;
        }

        if ($this->isException($phpcsFile, $stackPtr)) {
            return;
        }

        $fqcn = $this->getInstantiatedClassName($phpcsFile, $stackPtr);

        foreach ($this->allowedClasses as $allowedClassName) {
            // Since we cannot control whether people add allowed classes with starting slash or not,
            // and $fqcn always has it, we must ensure we compare correctly
            if (!StringHelper::startsWith($allowedClassName, '\\')) {
                $allowedClassName = '\\' . $allowedClassName;
            }

            if ($fqcn === $allowedClassName) {
                return;
            }
        }

        if ($this->allowInstantiateFinalClasses && $this->isInstantiatedClassFinal($fqcn, $phpcsFile, $stackPtr)) {
            return;
        }

        if ($this->allowNamedConstructors && $this->isNamedConstructor($fqcn, $phpcsFile, $stackPtr)) {
            return;
        }

        $phpcsFile->addError(self::ERROR_MESSAGE, $stackPtr, self::ERROR_CODE, [$fqcn]);
    }

    private function getClassName(File $file, int $stackPtr): string
    {
        $classNameStart = TokenHelper::findNextEffective($file, $stackPtr + 1);
        $classNameEnd = TokenHelper::findNext($file, [T_OPEN_PARENTHESIS, T_SEMICOLON], $classNameStart) - 1;

        $className = TokenHelper::getEffectiveContent($file, $classNameStart, $classNameEnd);

        if (!in_array($className, ['self', 'static'])) {
            return $className;
        }

        return ClassHelper::getCurrentClassFullyQualifiedName($file, $stackPtr);
    }

    private function isException(File $file, int $stackPtr): bool
    {
        $className = $this->getInstantiatedClassName($file, $stackPtr);

        if (!class_exists($className)) {
            return false;
        }

        $implementedInterfaces = class_implements($className, true);

        return in_array(\Throwable::class, $implementedInterfaces);
    }

    private function isAnonymousClass(File $file, int $stackPtr): bool
    {
        $nextPointer = TokenHelper::findNextEffective($file, $stackPtr + 1);

        return $nextPointer !== null && $file->getTokens()[$nextPointer]['code'] === \T_ANON_CLASS;
    }

    private function handleAnonymousClassInstantiation(File $file, int $stackPtr): void
    {
        if ($this->allowAnonymousClasses) {
            return;
        }

        $file->addError(self::ERROR_ANONYMOUS_MESSAGE, $stackPtr, self::ERROR_ANONYMOUS_CODE);
    }

    private function getInstantiatedClassName(File $phpcsFile, int $stackPtr): string
    {
        $className = $this->getClassName($phpcsFile, $stackPtr);
        $imports = UseStatementHelper::findImportsInFile($phpcsFile);

        return NamespaceHelper::composeFqcn(
            $className,
            $imports,
            NamespaceHelper::findCurrentNamespaceName($phpcsFile, $stackPtr)
        );
    }

    private function isInstantiatedClassFinal(string $fcqn, File $file, int $stackPtr): bool
    {
        try {
            $reflection = new ReflectionClass($fcqn);

            return $reflection->isFinal();
        } catch (\ReflectionException $e) {
            if ($this->reportReflectionError) {
                $file->addError(
                    self::REFLECTION_ERROR_MESSAGE,
                    $stackPtr,
                    self::REFLECTION_ERROR_CODE,
                    [$e->getMessage()]
                );
            }

            return $this->considerFinalInstantiationOnReflectionError;
        }
    }

    private function isExpression(File $file, int $stackPtr): bool
    {
        $nextPointer = TokenHelper::findNextEffective($file, $stackPtr + 1);
        $notExpression = [T_STRING, T_CLASS, T_NS_SEPARATOR, T_ANON_CLASS, T_SELF, T_STATIC];

        return $nextPointer !== null && !in_array($file->getTokens()[$nextPointer]['code'], $notExpression);
    }

    private function handleExpression(File $file, int $stackPtr): void
    {
        $expressionStart = TokenHelper::findNextEffective($file, $stackPtr + 1);
        $expressionEnd = $file->findEndOfStatement($expressionStart);

        $expression = TokenHelper::getEffectiveContent($file, $expressionStart, $expressionEnd);

        $file->addError(self::ERROR_MESSAGE_VARIABLE, $expressionStart, self::ERROR_CODE, [$expression]);
    }

    private function isNamedConstructor(string $fqcn, File $phpcsFile, int $stackPtr): bool
    {
        return $fqcn === ClassHelper::getCurrentClassFullyQualifiedName($phpcsFile, $stackPtr);
    }
}
