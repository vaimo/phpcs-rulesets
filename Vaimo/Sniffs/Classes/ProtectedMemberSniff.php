<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

declare(strict_types=1);

namespace Vaimo\Sniffs\Classes;

use PHP_CodeSniffer\Files\File;
use PHP_CodeSniffer\Sniffs\Sniff;
use Vaimo\Helpers\ClassHelper;
use Vaimo\Helpers\InheritanceHelper;
use Vaimo\Helpers\MemberHelper;
use Vaimo\ValueObject\ClassMember;

use function array_map;
use function in_array;

/**
 * Smarter reimplementation of MEQP2.PHP.ProtectedClassMember:
 *
 * * Allows abstract protected members
 * * Allows to redeclare protected inherited members
 */
class ProtectedMemberSniff implements Sniff
{
    public const CODE_PROPERTY = 'Property';
    private const MESSAGE_PROPERTY = 'Protected property "%s" found in class %s';

    public const CODE_FUNCTION = 'Function';
    private const MESSAGE_FUNCTION = 'Protected function "%s" found in class %s';

    public const CODE_CONSTANT = 'Constant';
    private const MESSAGE_CONSTANT = 'Protected constant "%s" found in class %s';

    private const MEMBER_TYPE_ERRORS = [
        ClassMember::PROPERTY => ['code' => self::CODE_PROPERTY, 'message' => self::MESSAGE_PROPERTY],
        ClassMember::METHOD => ['code' => self::CODE_FUNCTION, 'message' => self::MESSAGE_FUNCTION],
        ClassMember::CONSTANT => ['code' => self::CODE_CONSTANT, 'message' => self::MESSAGE_CONSTANT],
    ];

    public bool $allowAbstract = true;

    public bool $allowInherited = true;

    public bool $allowProtectedPropertyInAbstractClass = false;

    public bool $allowProtectedFunctionInAbstractClass = true;

    /**
     * If Sniff is not able to read list of inherited properties because of reflection error, what shall we do?
     *
     * We can only either allow some false-positives (default behavior)
     * or trust developer and think that all protected members are inherited.
     */
    public bool $considerInheritedOnReflectionError = false;

    /**
     * @inheritDoc
     */
    public function register()
    {
        return [T_CLASS];
    }

    /**
     * @inheritDoc
     */
    public function process(File $file, $pointer)
    {
        $classProperties = $file->getClassProperties($pointer);
        $reflectionError = false;
        try {
            $inheritedMembers = array_map(
                static fn(ClassMember $member) => $member->getName(),
                InheritanceHelper::collectInheritedMembers($file, $pointer)
            );
        } catch (\ReflectionException $e) {
            $inheritedMembers = [];
            $reflectionError = true;
        }

        foreach (MemberHelper::getClassMembersList($file, $pointer) as $member) {
            $memberProperties = $member->getMemberProperties();

            if (($memberProperties['scope'] ?? null) !== 'protected') {
                continue;
            }

            if ($this->isAllowedInheritedMember($member, $inheritedMembers)) {
                continue;
            }

            if ($reflectionError && $this->considerInheritedOnReflectionError) {
                continue;
            }

            if ($this->isAllowedProperty($member, $classProperties)) {
                continue;
            }

            if ($this->isAllowedFunction($member, $classProperties)) {
                continue;
            }

            $this->createError($file, $member);
        }
    }

    private function createError(File $file, ClassMember $member): void
    {
        ['code' => $code, 'message' => $message] = self::MEMBER_TYPE_ERRORS[$member->getType()];

        $file->addError(
            $message,
            $member->getPointer(),
            $code,
            [$member->getName(), ClassHelper::getCurrentClassFullyQualifiedName($file, $member->getPointer())]
        );
    }

    /**
     * @param mixed[] $classProperties
     * @see File::getClassProperties
     */
    private function isAllowedProperty(ClassMember $member, array $classProperties): bool
    {
        if (!$member->isProperty()) {
            return false;
        }

        return $classProperties['is_abstract'] && $this->allowProtectedPropertyInAbstractClass;
    }

    /**
     * @param mixed[] $classProperties
     * @see File::getClassProperties
     */
    private function isAllowedFunction(ClassMember $member, array $classProperties): bool
    {
        if (!$member->isFunction()) {
            return false;
        }

        if ($member->getMemberProperties()['is_abstract'] ?? false) {
            return $this->allowAbstract;
        }

        return $classProperties['is_abstract'] && $this->allowProtectedFunctionInAbstractClass;
    }

    /**
     * @param string[] $inheritedMembers
     */
    private function isAllowedInheritedMember(ClassMember $member, array $inheritedMembers): bool
    {
        return $this->allowInherited && in_array($member->getName(), $inheritedMembers);
    }
}
