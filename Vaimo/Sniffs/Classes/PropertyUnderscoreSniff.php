<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

declare(strict_types=1);

namespace Vaimo\Sniffs\Classes;

use PHP_CodeSniffer\Files\File;
use PHP_CodeSniffer\Sniffs\AbstractVariableSniff;
use Vaimo\Helpers\InheritanceHelper;
use Vaimo\Helpers\StringHelper;
use Vaimo\Helpers\VariableHelper;

class PropertyUnderscoreSniff extends AbstractVariableSniff
{
    public const CODE_FOUND = 'Found';
    public const MESSAGE_FOUND = 'Property name "%s" should not be prefixed with an underscore to indicate visibility';

    public bool $allowInherited = true;

    public bool $skipOnReflectionError = false;

    /**
     * @inheritDoc
     */
    protected function processMemberVar(File $phpcsFile, $stackPtr)
    {
        $propertyName = $this->getPropertyName($phpcsFile, $stackPtr);

        if (!StringHelper::startsWith($propertyName, '_')) {
            return;
        }

        try {
            if ($this->allowInherited && InheritanceHelper::isInheritedMember($phpcsFile, $stackPtr)) {
                return;
            }
        } catch (\ReflectionException $e) {
            if ($this->skipOnReflectionError) {
                return $phpcsFile->numTokens + 1;
            }
        }

        $phpcsFile->addError(self::MESSAGE_FOUND, $stackPtr, self::CODE_FOUND, [$propertyName]);
    }

    /**
     * @inheritDoc
     */
    protected function processVariable(File $phpcsFile, $stackPtr)
    {
        if (!VariableHelper::isPromotedProperty($phpcsFile, $stackPtr)) {
            return;
        }

        $propertyName = $this->getPropertyName($phpcsFile, $stackPtr);

        if (!StringHelper::startsWith($propertyName, '_')) {
            return;
        }

        $phpcsFile->addError(self::MESSAGE_FOUND, $stackPtr, self::CODE_FOUND, [$propertyName]);
    }

    private function getPropertyName(File $phpcsFile, int $stackPtr): string
    {
        return VariableHelper::normalizeName($phpcsFile->getTokens()[$stackPtr]['content']);
    }

    /**
     * @inheritDoc
     */
    protected function processVariableInString(File $phpcsFile, $stackPtr)
    {
        // Nothing to do here: out of scope
    }
}
