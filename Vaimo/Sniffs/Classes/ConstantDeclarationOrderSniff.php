<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

declare(strict_types=1);

namespace Vaimo\Sniffs\Classes;

use PHP_CodeSniffer\Files\File;
use PHP_CodeSniffer\Sniffs\Sniff;
use Vaimo\Helpers\MemberHelper;

class ConstantDeclarationOrderSniff implements Sniff
{
    public const CODE_ERROR = 'Incorrect';
    public const MESSAGE_ERROR = 'Class constants must be first class members.';

    /**
     * @inheritDoc
     */
    public function register()
    {
        return [T_CLASS];
    }

    /**
     * @inheritDoc
     */
    public function process(File $file, $pointer)
    {
        $nonConstantMembersFound = false;

        foreach (MemberHelper::getClassMembersList($file, $pointer) as $member) {
            if ($member->getType() !== T_CONST) {
                $nonConstantMembersFound = true;
                continue;
            }

            if (!$nonConstantMembersFound) {
                continue;
            }

            $file->addError(self::MESSAGE_ERROR, $member->getPointer(), self::CODE_ERROR);
        }
    }
}
