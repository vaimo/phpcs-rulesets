<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

declare(strict_types=1);

namespace Vaimo\Sniffs\ControlStructures;

use PHP_CodeSniffer\Files\File;
use PHP_CodeSniffer\Sniffs\Sniff;

use function array_filter;
use function count;
use function in_array;

class NestedIfSniff implements Sniff
{
    public const CODE_FOUND = 'Found';
    public const MESSAGE_FOUND = 'Nested if statement is not allowed.';

    public int $maxAllowedNesting = 0;

    /**
     * @inheritDoc
     */
    public function register()
    {
        return [T_IF, T_ELSE, T_ELSEIF];
    }

    /**
     * @inheritDoc
     */
    public function process(File $file, $pointer)
    {
        $token = $file->getTokens()[$pointer];

        $parentConditionals = array_filter(
            $token['conditions'] ?? [],
            fn ($tokenCode) => in_array($tokenCode, $this->register(), true)
        );

        if (count($parentConditionals) <= $this->maxAllowedNesting) {
            return;
        }

        $file->addError(self::MESSAGE_FOUND, $pointer, self::CODE_FOUND);
    }
}
