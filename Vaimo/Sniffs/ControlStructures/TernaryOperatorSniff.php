<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

declare(strict_types=1);

namespace Vaimo\Sniffs\ControlStructures;

use PHP_CodeSniffer\Files\File;
use PHP_CodeSniffer\Sniffs\Sniff;
use Vaimo\Helpers\TokenHelper;

use function array_keys;
use function reset;

class TernaryOperatorSniff implements Sniff
{
    public const CODE_NESTED = 'Nested';
    public const MESSAGE_NESTED = 'Nesting ternary operators is not allowed.';

    public const CODE_FOUND_IN_CONDITION = 'FoundInCondition';
    public const MESSAGE_FOUND_IN_CONDITION = 'Using ternary operator in condition is not allowed';

    public const CODE_MULTILINE_EXPRESSION = 'MultilineExpression';
    public const MESSAGE_MULTILINE_EXPRESSION = 'Multiline expressions as parts of ternary operator are not allowed.';

    /**
     * @inheritDoc
     */
    public function register()
    {
        return [T_INLINE_THEN];
    }

    /**
     * @inheritDoc
     */
    public function process(File $phpcsFile, $stackPtr)
    {
        $endOfStatement = $phpcsFile->findEndOfStatement($stackPtr);

        $this->disallowNestedTernaryOperator($phpcsFile, $stackPtr, $endOfStatement);
        $this->disallowTernaryOperatorInCondition($phpcsFile, $stackPtr);
        $this->disallowMultilineExpressions($phpcsFile, $stackPtr);
    }

    private function disallowNestedTernaryOperator(File $phpcsFile, int $stackPtr, int $endOfStatement): void
    {
        $nested = TokenHelper::findNext($phpcsFile, [T_INLINE_THEN], $stackPtr + 1, $endOfStatement);

        if (!$nested) {
            return;
        }

        $phpcsFile->addError(self::MESSAGE_NESTED, $nested, self::CODE_NESTED);
    }

    private function disallowTernaryOperatorInCondition(File $phpcsFile, int $stackPtr): void
    {
        $token = $phpcsFile->getTokens()[$stackPtr];

        $nestedParenthesis = array_keys($token['nested_parenthesis'] ?? []);
        $firstParenthesis = reset($nestedParenthesis);
        $firstParenthesisToken = $phpcsFile->getTokens()[$firstParenthesis];
        $ownerPointer = $firstParenthesisToken['parenthesis_owner'] ?? null;

        if (!$ownerPointer) {
            return;
        }

        $ownerToken = $phpcsFile->getTokens()[$ownerPointer];

        if ($ownerToken['code'] !== T_IF) {
            return;
        }

        $phpcsFile->addError(self::MESSAGE_FOUND_IN_CONDITION, $stackPtr, self::CODE_FOUND_IN_CONDITION);
    }

    private function disallowMultilineExpressions(File $phpcsFile, int $stackPtr): void
    {
        foreach ($this->findTernaryOperatorParts($phpcsFile, $stackPtr) as [$startPointer, $endPointer]) {
            if (!$this->isMultiline($phpcsFile, $startPointer, $endPointer)) {
                continue;
            }

            $phpcsFile->addError(self::MESSAGE_MULTILINE_EXPRESSION, $startPointer, self::CODE_MULTILINE_EXPRESSION);
        }
    }

    /**
     * @return array<array{int, int}> array of [int $startPointer, int $endPointer] tuples
     */
    private function findTernaryOperatorParts(File $phpcsFile, int $stackPtr): array
    {
        $startOfStatementPointer = $phpcsFile->findStartOfStatement($stackPtr);
        $endOfStatementPointer = $phpcsFile->findEndOfStatement($stackPtr);

        $thenPointer = $stackPtr;
        $elsePointer = TokenHelper::findNext($phpcsFile, [T_INLINE_ELSE], $stackPtr, $endOfStatementPointer);

        $startOfConditionPointer = $startOfStatementPointer;
        $endOfConditionPointer = TokenHelper::findPreviousEffective(
            $phpcsFile,
            $thenPointer - 1,
            $startOfStatementPointer
        );

        $startOfThenPointer = TokenHelper::findPreviousEffective(
            $phpcsFile,
            $thenPointer + 1,
            $startOfStatementPointer // Switch to inclusive endPointer to handle elvis operator
        );

        $endOfThenPointer = TokenHelper::findPreviousEffective(
            $phpcsFile,
            $elsePointer - 1,
            $startOfStatementPointer
        );

        $startOfElsePointer = TokenHelper::findNextEffective(
            $phpcsFile,
            $elsePointer - 1,
            $endOfStatementPointer + 1 // Switch to inclusive endPointer to handle ternary in parentheses
        );
        $endOfElsePointer = $endOfStatementPointer;

        return [
            [$startOfConditionPointer, $endOfConditionPointer],
            [$startOfThenPointer, $endOfThenPointer],
            [$startOfElsePointer, $endOfElsePointer],
        ];
    }

    private function isMultiline(File $phpcsFile, int $startPointer, int $endPointer): bool
    {
        $startLine = $phpcsFile->getTokens()[$startPointer]['line'];
        $endLine = $phpcsFile->getTokens()[$endPointer]['line'];

        return $startLine !== $endLine;
    }
}
