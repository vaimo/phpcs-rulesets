<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

declare(strict_types=1);

namespace Vaimo\Sniffs\ControlStructures;

use PHP_CodeSniffer\Files\File;
use PHP_CodeSniffer\Sniffs\Sniff;
use Vaimo\Helpers\StringHelper;
use Vaimo\Helpers\TernaryHelper;
use Vaimo\Helpers\TokenHelper;

use function in_array;
use function sprintf;

class TernaryOperatorPlacementSniff implements Sniff
{
    public const MESSAGE = '%s ternary operator is misplaced. Only allowed placement is %s';

    public const CODE_INLINE_THEN = 'Then';
    public const CODE_INLINE_ELSE = 'Else';
    public const CODE_INLINE_ELVIS = 'Elvis';
    public const PLACEMENT_FIRST = 'first';
    public const PLACEMENT_LAST = 'last';
    public const PLACEMENT_MIDDLE = 'middle';

    public string $allowOnly = self::PLACEMENT_FIRST;

    /**
     * @inheritDoc
     */
    public function register()
    {
        return [T_INLINE_THEN, T_INLINE_ELSE];
    }

    /**
     * @inheritDoc
     */
    public function process(File $phpcsFile, $stackPtr)
    {
        $this->validateAllowOnlyValue();

        if (TernaryHelper::isElvisOperator($phpcsFile, $stackPtr)) {
            $this->checkElvis($phpcsFile, $stackPtr);

            // Skipping next symbol in hope to not get triggered by else
            return $stackPtr + 2;
        }

        $method = $phpcsFile->getTokens()[$stackPtr]['code'] === T_INLINE_THEN
            ? 'checkThen'
            : 'checkElse';

        $this->$method($phpcsFile, $stackPtr);
    }

    private function checkThen(File $phpcsFile, int $stackPtr): void
    {
        if (!TernaryHelper::isMultilineTernary($phpcsFile, $stackPtr)) {
            return;
        }

        $placement = $this->getPlacement($phpcsFile, $stackPtr);

        if ($this->allowOnly === $placement) {
            return;
        }

        $message = sprintf(self::MESSAGE, self::CODE_INLINE_THEN, $this->allowOnly);
        $phpcsFile->addError($message, $stackPtr, self::CODE_INLINE_THEN);
    }

    private function checkElse(File $phpcsFile, int $stackPtr): void
    {
        if (!TernaryHelper::isMultilineTernary($phpcsFile, $stackPtr)) {
            return;
        }

        $placement = $this->getPlacement($phpcsFile, $stackPtr);

        if ($this->allowOnly === $placement) {
            return;
        }

        $message = sprintf(self::MESSAGE, self::CODE_INLINE_ELSE, $this->allowOnly);
        $phpcsFile->addError($message, $stackPtr, self::CODE_INLINE_ELSE);
    }

    private function getPlacement(File $phpcsFile, int $stackPtr): string
    {
        $token = $phpcsFile->getTokens()[$stackPtr];
        $tokenContent = $token['content'];

        $line = $token['line'];
        $lineContent = TokenHelper::getLineContent($phpcsFile, $line);

        if (StringHelper::startsWith($lineContent, $tokenContent)) {
            return self::PLACEMENT_FIRST;
        }

        if (StringHelper::endsWith($lineContent, $tokenContent)) {
            return self::PLACEMENT_LAST;
        }

        return self::PLACEMENT_MIDDLE;
    }

    private function checkElvis(File $phpcsFile, int $stackPtr): void
    {
        if (!TernaryHelper::isMultilineTernary($phpcsFile, $stackPtr)) {
            return;
        }

        switch ($this->allowOnly) {
            case self::PLACEMENT_FIRST:
                $placement = $this->getPlacement($phpcsFile, $stackPtr);
                break;
            case self::PLACEMENT_LAST:
                $elsePointer = TokenHelper::findNextEffective($phpcsFile, $stackPtr + 1);
                $placement = $this->getPlacement($phpcsFile, $elsePointer);
                break;
        }

        if ($this->allowOnly === $placement) {
            return;
        }

        $message = sprintf(self::MESSAGE, self::CODE_INLINE_ELVIS, $this->allowOnly);
        $phpcsFile->addError($message, $stackPtr, self::CODE_INLINE_ELVIS);
    }

    private function validateAllowOnlyValue(): void
    {
        if (!in_array($this->allowOnly, [self::PLACEMENT_FIRST, self::PLACEMENT_LAST])) {
            throw new \UnexpectedValueException('allowOnly can be either "first" or "last"');
        }
    }
}
