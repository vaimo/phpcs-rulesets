#!/usr/bin/env bash
#
# Copyright (c) Vaimo Group. All rights reserved.
# See LICENSE_VAIMO.txt for license details.
#

set -eu

MAGENTO_VERSION=$(composer config version)
export MAGENTO_VERSION

# we use the git hash to ensure the correct version is required by composer
COMMIT_HASH=$(git --git-dir /module/.git rev-parse HEAD)
# branch detection requires you to enable the "Check out to matching local branch"
# option in the git source on the Jenkins build job
BRANCH=$(git --git-dir /module/.git branch | grep -e "^*" | cut -d ' ' -f2)

echo "=== Requiring package on Magento version ${MAGENTO_VERSION} ==="
PACKAGE_NAME=$(composer config name -f /module/composer.json)
composer config --no-plugins repositories.modules path /module
# Magento 2.3.7 does not have this allow-plugins thing and errors out.
composer config --no-plugins allow-plugins.dealerdirect/phpcodesniffer-composer-installer true || true
# should always be in sync with instructions in README.md
composer remove --dev --no-update squizlabs/php_codesniffer
composer require --dev --with-all-dependencies "${PACKAGE_NAME}:dev-${BRANCH}#${COMMIT_HASH}"

# test if Vaimo standard was installed
php vendor/bin/phpcs -i | grep Vaimo || (>&2 echo "[ERROR] Vaimo coding standard not installed" && exit 1)
