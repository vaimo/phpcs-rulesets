#!groovy
// Copyright (c) Vaimo Group. All rights reserved.
// See LICENSE_VAIMO.txt for license details.

// This pipeline is meant to be used together with vaimo/composer-changelogs (>=0.10.0) and is build on an
// assumption that the CI job name matches with package name (required for package generation).

@Library('platform-jenkins-pipeline') _

def environments = [
    [
        PHP_VERSION: '8.1',
        MAGENTO_VERSION: '2.4.4',
        COMPOSER_VERSION: '2',
    ],
    [
        PHP_VERSION: '7.4',
        MAGENTO_VERSION: '2.4.3-p1',
        COMPOSER_VERSION: '2',
    ],
    [
        PHP_VERSION: '7.4',
        MAGENTO_VERSION: '2.4.2-p2',
        COMPOSER_VERSION: '2',
    ],
    [
        PHP_VERSION: '7.4',
        MAGENTO_VERSION: '2.3.7-p1',
        COMPOSER_VERSION: '2',
    ]
]

def parallelEnvironments(environments, Closure body) {
    def branches = [:]
    environments.each { environment ->
        branches[environment] = {
            stage("Environment: ${environment}") {
                environmentAsList = environment.collect { key, value ->
                    "${key}=${value}"
                }

                // useful as identifier inside the user-defined closure
                environmentAsList.add("BRANCH_ENVIRONMENT=${environment}")

                withEnv(environmentAsList) {
                    body()
                }
            }
        }
    }

    branches.failFast = true
    parallel branches
}

pipeline {
    agent { label 'docker' }
    options {
        buildDiscarder(logRotator(numToKeepStr: '5'))
        disableConcurrentBuilds()
        timeout(time: 20, unit: 'MINUTES')
        timestamps()
    }
    stages {
        stage('Start build') {
            steps {
                script {
                    env.REPO_SLUG = new com.vaimo.MultiBranchJob().repoSlug(env.GIT_URL)

                    bitbucketStatusNotify(
                         buildState: 'INPROGRESS',
                         buildKey: 'build',
                         buildName: 'Build',
                         repoSlug: env.REPO_SLUG,
                         commitId: env.GIT_COMMIT
                    )
                }
            }
        }
        stage('Run Build') {
            parallel {
                stage('PHP 7.4') {
                    environment {
                        PHP_VERSION='7.4'
                        EXPECTED_COVERAGE='100'
                        COMPOSER_ARGS='--prefer-lowest'
                    }
                    steps {
                        bitbucketStatus (key: 'analyse_code', name: 'Code Analysis', repo: "${env.REPOSITORY_URL}", commitId: "${env.COMMIT_ID}") {
                            ansiColor('xterm') {
                                sh("docker build -f ci/test-coverage/Dockerfile -t phpcs-rulesets-${env.PHP_VERSION} --build-arg PHP_VERSION --build-arg EXPECTED_COVERAGE --build-arg COMPOSER_ARGS .")
                                sh("docker run --rm phpcs-rulesets-${env.PHP_VERSION} composer changelog:validate")
                                sh("docker run --rm phpcs-rulesets-${env.PHP_VERSION} vendor/bin/phpcs --standard=ci/phpcs.xml")
                                sh("docker run --rm phpcs-rulesets-${env.PHP_VERSION} ci/test-coverage/run-tests.sh")
                            }
                        }
                    }
                }
                stage('PHP 8.1') {
                    environment {
                        PHP_VERSION='8.1'
                        EXPECTED_COVERAGE='100'
                        COMPOSER_ARGS=''
                    }
                    steps {
                        bitbucketStatus (key: 'analyse_code', name: 'Code Analysis', repo: "${env.REPOSITORY_URL}", commitId: "${env.COMMIT_ID}") {
                            ansiColor('xterm') {
                                sh("docker build -f ci/test-coverage/Dockerfile -t phpcs-rulesets-${env.PHP_VERSION} --build-arg PHP_VERSION --build-arg EXPECTED_COVERAGE --build-arg COMPOSER_ARGS .")
                                sh("docker run --rm phpcs-rulesets-${env.PHP_VERSION} composer changelog:validate")
                                sh("docker run --rm phpcs-rulesets-${env.PHP_VERSION} vendor/bin/phpcs --standard=ci/phpcs.xml")
                                sh("docker run --rm phpcs-rulesets-${env.PHP_VERSION} ci/test-coverage/run-tests.sh")
                            }
                        }
                        script {
                            env.CHANGELOG_VERSION = sh(returnStdout: true, script: 'docker run --rm phpcs-rulesets-$PHP_VERSION composer changelog:version --segments 1').trim()
                            env.RELEASE_VERSION = sh(returnStdout: true, script: 'docker run --rm phpcs-rulesets-$PHP_VERSION composer changelog:version').trim()
                        }
                    }
                }
            }
        }
        stage('Magento package compatibility test') {
            environment {
                COMPOSE_FILE = 'ci/test-runner/docker-compose.yml'
                COMPOSER_AUTH = credentials('magento-composer-build-credentials')
            }
            steps {
                parallelEnvironments(environments) {
                    script {
                        // allows us to run multiple instances of docker-compose from the same set of files
                        withEnv(["COMPOSE_PROJECT_NAME=${JOB_NAME}-${BRANCH_ENVIRONMENT}"]) {
                            sh "docker-compose pull"
                            try {
                                sh "docker-compose run magento /module/ci/test-runner/run-tests.sh"
                            } finally {
                                sh 'docker-compose down -v'
                            }
                        }
                    }
                }
            }
        }
        stage('Assess Release Necessity') {
            steps {
                script {
                    sh 'git fetch --tags $GIT_URL'
                    def branchName = env.JOB_BASE_NAME == 'master' ? '' : env.GIT_BRANCH
                    def isReleaseBranch = env.JOB_BASE_NAME == 'master' || branchName.startsWith('release/')
                    def shouldRelease = env.CHANGELOG_VERSION.toBoolean() && isReleaseBranch
                    def isTagFromChangelogMissingInVCS = sh(returnStdout: true, script: 'git tag -l $RELEASE_VERSION').trim() == ''

                    env.SHOULD_RELEASE = shouldRelease && isTagFromChangelogMissingInVCS
                }
            }
        }
        stage('Create Tags') {
            when {
                expression { env.SHOULD_RELEASE.toBoolean() == true }
            }
            steps {
                bitbucketStatus (key: 'create_tags', name: 'Create Tags') {
                    sh 'git tag $RELEASE_VERSION'
                    sh 'git push $GIT_URL --tags'
                }
            }
        }
        stage('Release Package') {
            when {
                expression { env.SHOULD_RELEASE.toBoolean() == true }
            }
            steps {
                bitbucketStatus (key: 'release_package', name: 'Releasing Package') {
                    generateComposerPackage(moduleName:env.GIT_URL, notNormalize: true)
                }
            }
        }
    }
    post {
        success {
            script {
                bitbucketStatusNotify(
                    buildState: 'SUCCESSFUL',
                    buildKey: 'build',
                    buildName: 'Build',
                    repoSlug: env.REPO_SLUG,
                    commitId: env.GIT_COMMIT
                )
            }

        }
        failure {
            script {
                bitbucketStatusNotify(
                    buildState: 'FAILED',
                    buildKey: 'build',
                    buildName: 'Build',
                    repoSlug: env.REPO_SLUG,
                    commitId: env.GIT_COMMIT
                )
            }
        }
        always {
            script {
                if (getContext(hudson.FilePath)) {
                    deleteDir()
                }
            }

            emailext (
                subject: "[VCS] [CI] ${currentBuild.result}: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'",
                body: """<p>Check full console output at <a href='${env.BUILD_URL}'>${env.JOB_NAME} [${env.BUILD_NUMBER}]</a></p>
                            Following is the last 200 lines of the log.<br/>
                            <br/>
                            --LOG-BEGIN--<br/>
                            <pre style='line-height: 22px; display: block; color: #333; font-family: Monaco,Menlo,Consolas,"Courier New",monospace; padding: 10.5px; margin: 0 0 11px; font-size: 13px; word-break: break-all; word-wrap: break-word; white-space: pre-wrap; background-color: #f5f5f5; border: 1px solid #ccc; border: 1px solid rgba(0,0,0,.15); -webkit-border-radius: 4px; -moz-border-radius: 4px; border-radius: 4px;'>
                            \${BUILD_LOG, maxLines=200, escapeHtml=false}
                            </pre>
                            --LOG-END--""",
                recipientProviders: [[$class: 'DevelopersRecipientProvider'], [$class: 'RequesterRecipientProvider']]
            )
        }
    }
}
