<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

declare(strict_types=1);

// phpcs:disable -- Test bootstrap is always a bit out of this world

if (!function_exists('__')) {
    eval(<<<PHP
namespace {
    function __(...\$args) {
        \$text = array_shift(\$args);
        return new \Magento\Framework\Phrase(\$text, \$args);
    }
}
PHP
    );
}

$vendorPaths = [
    __DIR__ . '/vendor',
    __DIR__ . '/../vendor',
    __DIR__ . '/../../vendor',
    __DIR__ . '/../../../vendor'
];

foreach ($vendorPaths as $path) {
    if (!is_dir($path)) {
        continue;
    }

    require_once $path . '/autoload.php';
    require_once $path . '/squizlabs/php_codesniffer/autoload.php';
    break;
}
