<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

declare(strict_types=1);

namespace Vaimo\Sniffs\DocumentationTest;

use function exec;
use function file;
use function is_array;
use function preg_match;
use function sprintf;
use function strpos;

// phpcs:disable Magento2.Functions.DiscouragedFunction -- low-level code outside of Magento
// phpcs:disable Magento2.Security.InsecureFunction -- low-level code outside of Magento

class CodingStandard
{
    private const STANDARD_NAME = 'Vaimo';
    private const STANDARD_DOC_FOLDER = __DIR__ . '/../../docs/standard/';
    private const SNIFF_NAME_REGEX = '/([a-z0-9]+\.){2}[a-z0-9]+/i';

    /**
     * @var string[]
     */
    private static array $documentedSniffs;

    /**
     * @var string[]
     */
    private static array $includedSniffs;

    /**
     * @return string[]
     */
    public static function getDocumentedSniffs(): array
    {
        if (!isset(self::$documentedSniffs)) {
            self::$documentedSniffs = self::readDocumentation();
        }

        return self::$documentedSniffs;
    }

    /**
     * @return string[]
     */
    public static function getIncludedSniffs(): array
    {
        if (!isset(self::$includedSniffs)) {
            self::$includedSniffs = self::readCodingStandard();
        }

        return self::$includedSniffs;
    }

    /**
     * @return string[]
     */
    private static function readDocumentation(): array
    {
        $files = new \FilesystemIterator(self::STANDARD_DOC_FOLDER);

        $documentedSniffs = [];
        foreach ($files as $file) {
            $content = file($file->getRealPath());

            foreach ($content as $lineNumber => $lineText) {
                $matches = [];
                if (!preg_match(self::SNIFF_NAME_REGEX, $lineText, $matches)) {
                    continue;
                }

                $sniffName = $matches[0];

                if (!isset($content[$lineNumber + 1]) || strpos($content[$lineNumber + 1], '---') !== 0) {
                    continue;
                }

                $documentedSniffs[] = $sniffName;
            }
        }

        return $documentedSniffs;
    }

    /**
     * @return string[]
     */
    private static function readCodingStandard(): array
    {
        $output = '';
        exec(
            sprintf('vendor/bin/phpcs --standard=%s -e', self::STANDARD_NAME),
            $output
        );

        if (!is_array($output)) {
            return [];
        }

        $sniffs = [];
        foreach ($output as $line) {
            $matches = [];

            if (!preg_match(self::SNIFF_NAME_REGEX, $line, $matches)) {
                continue;
            }

            $sniffs[] = $matches[0];
        }

        return $sniffs;
    }
}
