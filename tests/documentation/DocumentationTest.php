<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

declare(strict_types=1);

namespace Vaimo\Sniffs\DocumentationTest;

use PHPUnit\Framework\TestCase;

use function array_diff;
use function implode;

class DocumentationTest extends TestCase
{
    public function testAllSniffsAreDocumented(): void
    {
        $missingDocumentation = array_diff(CodingStandard::getIncludedSniffs(), CodingStandard::getDocumentedSniffs());

        self::assertEmpty(
            $missingDocumentation,
            $this->formatMissingSniffsMessage('All sniffs should be documented.', $missingDocumentation)
        );
    }

    public function testAllDocumentedSniffsExist(): void
    {
        $orphanSniffs = array_diff(CodingStandard::getDocumentedSniffs(), CodingStandard::getIncludedSniffs());

        self::assertEmpty(
            $orphanSniffs,
            $this->formatMissingSniffsMessage('All documented sniffs should exist in coding standard.', $orphanSniffs)
        );
    }

    /**
     * @param string[] $sniffs
     */
    private function formatMissingSniffsMessage(string $header, array $sniffs): string
    {
        $formattedSniffs = implode("\n", $sniffs);

        return <<<ERROR
${header} These are missing:

${formattedSniffs}
ERROR;
    }
}
