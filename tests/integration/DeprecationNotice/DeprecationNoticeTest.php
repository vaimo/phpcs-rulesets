<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

declare(strict_types=1);

namespace Vaimo\Sniffs\IntegrationTest\DeprecationNotice;

use Vaimo\Sniffs\IntegrationTest\IntegrationTestCase;

class DeprecationNoticeTest extends IntegrationTestCase
{
    public function testDeprecationNotice(): void
    {
        $filePath = __DIR__ . '/data/deprecationNotice.php';
        $report = $this->runCodeSniffer($filePath);

        $this->assertNoErrorsInFile($filePath, $report, [
            'Generic.PHP.NoSilencedErrors',
            'Magento2.Functions.DiscouragedFunction',
            'Vaimo.PHP.NoSilencedError',
        ]);
    }
}
