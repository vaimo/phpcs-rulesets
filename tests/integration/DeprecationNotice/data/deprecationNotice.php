<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

class Example
{
    /**
     * @deprecated use doSomething() instead
     */
    public function doSomthing()
    {
        @trigger_error(
            'Example::doSomthing() is deprecated, use Example::doSomething() instead.',
            E_USER_DEPRECATED
        );
    }

    public function doSomething()
    {

    }
}
