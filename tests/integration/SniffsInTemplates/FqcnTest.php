<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

declare(strict_types=1);

namespace Vaimo\Sniffs\IntegrationTest\SniffsInTemplates;

use Vaimo\Sniffs\IntegrationTest\IntegrationTestCase;

class FqcnTest extends IntegrationTestCase
{
    public function testFqcnInTemplateShouldBeAllowed(): void
    {
        $filePath = __DIR__ . '/data/fqcn.phtml';
        $report = $this->runCodeSniffer($filePath);

        $this->assertNoErrorsInFile($filePath, $report, [
            'SlevomatCodingStandard.Namespaces.ReferenceUsedNamesOnly',
        ]);
    }
}
