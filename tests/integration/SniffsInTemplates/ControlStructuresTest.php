<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

declare(strict_types=1);

namespace Vaimo\Sniffs\IntegrationTest\SniffsInTemplates;

use Vaimo\Sniffs\IntegrationTest\IntegrationTestCase;

class ControlStructuresTest extends IntegrationTestCase
{
    public function testTemplateSyntaxControlStructures(): void
    {
        $filePath = __DIR__ . '/data/controlStructures.phtml';
        $report = $this->runCodeSniffer($filePath);

        $this->assertNoErrorsInFile($filePath, $report, [
            'SlevomatCodingStandard.ControlStructures.BlockControlStructureSpacing',
        ]);
    }
}
