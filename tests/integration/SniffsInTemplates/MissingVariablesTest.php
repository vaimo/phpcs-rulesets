<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

declare(strict_types=1);

namespace Vaimo\Sniffs\IntegrationTest\SniffsInTemplates;

use Vaimo\Sniffs\IntegrationTest\IntegrationTestCase;

class MissingVariablesTest extends IntegrationTestCase
{
    public function testMissingVariablesInTemplates(): void
    {
        $filePath = __DIR__ . '/data/missingVariables.phtml';
        $report = $this->runCodeSniffer($filePath);

        $this->assertNoErrorsInFile($filePath, $report, [
            'SlevomatCodingStandard.Commenting.InlineDocCommentDeclaration.MissingVariable',
        ]);
    }
}
