<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

declare(strict_types=1);

namespace Vaimo\Sniffs\IntegrationTest\SniffsInTemplates;

use Vaimo\Sniffs\IntegrationTest\IntegrationTestCase;

class NestedIfTest extends IntegrationTestCase
{
    public function testIgnoresNestedIfsInTemplates(): void
    {
        $report = $this->runCodeSniffer(__DIR__ . '/data/nestedIf.phtml');

        $this->assertNoErrorsInFile(
            __DIR__ . '/data/nestedIf.phtml',
            $report,
            ['Vaimo.ControlStructures.NestedIf']
        );
    }
}
