<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

declare(strict_types=1);

/**
 * @param $a
 */
function doSomething(int $a): void
{
}
