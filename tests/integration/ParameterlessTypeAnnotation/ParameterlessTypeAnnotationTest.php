<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

declare(strict_types=1);

namespace Vaimo\Sniffs\IntegrationTest\ParameterlessTypeAnnotation;

use PHPUnit\Framework\ExpectationFailedException;
use Vaimo\Sniffs\IntegrationTest\IntegrationTestCase;
use Vaimo\Sniffs\IntegrationTest\PhpcsProcessError;

class ParameterlessTypeAnnotationTest extends IntegrationTestCase
{
    public function testDoesNotFailOnParameterlessTypeAnnotations(): void
    {
        self::expectNotToPerformAssertions();

        try {
            $this->runCodeSniffer(__DIR__ . '/data/parameterlessTypeAnnotation.php');
        } catch (PhpcsProcessError $e) {
            throw new ExpectationFailedException(
                'phpcs died with unexpected status',
                null,
                $e
            );
        }
    }
}
