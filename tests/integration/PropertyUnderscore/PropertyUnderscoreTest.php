<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

declare(strict_types=1);

namespace Vaimo\Sniffs\IntegrationTest\PropertyUnderscore;

use Vaimo\Sniffs\IntegrationTest\IntegrationTestCase;

class PropertyUnderscoreTest extends IntegrationTestCase
{
    public function testTriggersOnlyOnce(): void
    {
        $filePath = __DIR__ . '/data/InheritedProperty.php';
        $report = $this->runCodeSniffer($filePath);

        $this->assertNoErrorsInFile($filePath, $report, [
            'PSR2.Classes.PropertyDeclaration.Underscore',
        ]);

        $this->assertNoErrorsOnLine($report, 14, ['Vaimo.Classes.PropertyUnderscore']);
        $this->assertErrorOnLine($report, 16, 'Vaimo.Classes.PropertyUnderscore');
    }
}
