<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

namespace Vaimo\Sniffs\IntegrationTest\PropertyUnderscore\data;

use Vaimo\Sniffs\Stub\UnderscorePropertiesStub;

class InheritedProperty extends UnderscorePropertiesStub
{
    protected $_options = [];

    protected $_something = 'something';
}
