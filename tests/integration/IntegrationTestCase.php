<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

declare(strict_types=1);

namespace Vaimo\Sniffs\IntegrationTest;

use PHPUnit\Framework\ExpectationFailedException;
use PHPUnit\Framework\TestCase;

use function array_column;
use function array_filter;
use function escapeshellarg;
use function file_exists;
use function file_get_contents;
use function json_decode;
use function ob_get_clean;
use function ob_start;
use function passthru;
use function sprintf;
use function strpos;
use function unlink;

// phpcs:disable Magento2.Functions.DiscouragedFunction -- low-level code outside of Magento
// phpcs:disable Magento2.Security.InsecureFunction -- low-level code outside of Magento

abstract class IntegrationTestCase extends TestCase
{
    private const REPORT_PATH = __DIR__ . '/tmp/report.json';

    protected function setUp(): void
    {
        parent::setUp();

        if (!file_exists(self::REPORT_PATH)) {
            return;
        }

        unlink(self::REPORT_PATH);
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        if (!file_exists(self::REPORT_PATH)) {
            return;
        }

        unlink(self::REPORT_PATH);
    }

    /**
     * @return array{errors: int, warnings: int, messages: array<string,mixed>} $report
     * @see phpcs documentation for json report format
     * @throws PhpcsProcessError if phpcs returns 255 (Ooops, I died) status.
     */
    protected function runCodeSniffer(string $filePath): array
    {
        $resultCode = 0;

        ob_start();
        passthru(
            sprintf(
                'php vendor/bin/phpcs --standard=Vaimo --report-json=%s %s 2>&1',
                self::REPORT_PATH,
                escapeshellarg($filePath)
            ),
            $resultCode
        );
        $output = ob_get_clean();

        if ($resultCode === 255) {
            throw new PhpcsProcessError($output, $resultCode);
        }

        $json = file_get_contents(self::REPORT_PATH);
        $decoded = json_decode($json, $assoc = true, 512, JSON_THROW_ON_ERROR);

        return $decoded['files'][$filePath];
    }

    /**
     * @param array{errors: int, warnings: int, messages: array<string,mixed>} $report
     * @param string[] $sniffCodes
     * @see phpcs documentation for json report format
     */
    protected function assertNoErrorsInFile(string $filePath, array $report, array $sniffCodes): void
    {
        $this->addToAssertionCount(1);
        foreach ($report['messages'] as $message) {
            foreach ($sniffCodes as $sniffCode) {
                if (strpos($message['source'], $sniffCode) !== 0) {
                    continue;
                }

                $message = sprintf(
                    '%s is not expected to happen in %s, but it happened on line %d: %s (%s).',
                    $sniffCode,
                    $filePath,
                    $message['line'],
                    $message['message'],
                    $message['source']
                );

                throw new ExpectationFailedException($message);
            }
        }
    }

    /**
     * @param array{errors: int, warnings: int, messages: array<string,mixed>} $report
     * @param string[] $sniffCodes
     * @see phpcs documentation for json report format
     */
    protected function assertNoErrorsOnLine(array $report, int $line, array $sniffCodes): void
    {
        $this->addToAssertionCount(1);
        $sources = array_column($this->collectErrorsOnLine($report, $line), 'source');

        foreach ($sources as $source) {
            foreach ($sniffCodes as $error) {
                if (strpos($source, $error) !== 0) {
                    continue;
                }

                throw new ExpectationFailedException(
                    sprintf('"%s" is not expected to happen on line %d.', $source, $line)
                );
            }
        }
    }

    /**
     * @param array{errors: int, warnings: int, messages: array<string,mixed>} $report
     * @see phpcs documentation for json report format
     */
    protected function assertErrorOnLine(array $report, int $line, string $sniffCode): void
    {
        $this->addToAssertionCount(1);
        $sources = array_column($this->collectErrorsOnLine($report, $line), 'source');

        foreach ($sources as $source) {
            if (strpos($source, $sniffCode) === 0) {
                return;
            }
        }

        throw new ExpectationFailedException(
            sprintf('"%s" is expected to happen on line %d', $sniffCode, $line)
        );
    }

    /**
     * @param array{errors: int, warnings: int, messages: array<string,mixed>} $report
     * @return array<array{line: int, column: int, source: string, message: string}>
     * @see phpcs documentation for json report format
     */
    protected function collectErrorsOnLine(array $report, int $line): array
    {
        return array_filter(
            $report['messages'],
            static fn(array $error): bool => $error['line'] === $line
        );
    }
}
