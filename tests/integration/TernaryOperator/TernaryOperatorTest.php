<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

declare(strict_types=1);

namespace Vaimo\Sniffs\IntegrationTest\TernaryOperator;

use Vaimo\Sniffs\IntegrationTest\IntegrationTestCase;

class TernaryOperatorTest extends IntegrationTestCase
{
    public function testRequiresUseOfNullCoalesceOperator(): void
    {
        $report = $this->runCodeSniffer(__DIR__ . '/data/NullCoalesceOperatorCanBeUsed.php');

        $this->assertErrorOnLine($report, 9, 'SlevomatCodingStandard.ControlStructures.RequireNullCoalesceOperator');
    }

    public function testCatchesFormattingErrors(): void
    {
        $report = $this->runCodeSniffer(__DIR__ . '/data/Formatting.php');

        $this->assertErrorOnLine($report, 15, 'PSR12.Operators.OperatorSpacing.NoSpaceBefore');
    }
}
