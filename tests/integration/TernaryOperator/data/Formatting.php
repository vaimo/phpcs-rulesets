<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

$this->taxConfig->displayCartSubtotalInclTax() ?
    $order->getSubtotalInclTax() :
    $order->getSubtotal();

$baseDiscountTaxCompensationAmount = $this->globalStore ?
    $orderItem->getBaseDiscountTaxCompensationAmount() : $orderItem->getDiscountTaxCompensationAmount();

$sourceCode = count($sources) === 1 ? $sources[0]->getSourceCode(): $this->defaultSourceProvider->getCode();
