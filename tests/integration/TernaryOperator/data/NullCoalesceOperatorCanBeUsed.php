<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

$swatchVisualFiles = isset($attributeOptionsData['optionvisual']['value'])
    ? $attributeOptionsData['optionvisual']['value']
    : [];
