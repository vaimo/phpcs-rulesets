<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

declare(strict_types=1);

namespace Vaimo\Sniffs\IntegrationTest\LongTypeCast;

use Vaimo\Sniffs\IntegrationTest\IntegrationTestCase;

class LongTypeCastTest extends IntegrationTestCase
{
    public function testOnlyOneErrorIsShownOnLongTypeCast(): void
    {
        $report = $this->runCodeSniffer(__DIR__ . '/data/longTypeCast.php');

        $this->assertErrorOnLine($report, 10, 'SlevomatCodingStandard.PHP.TypeCast.InvalidCastUsed');
        $this->assertNoErrorsOnLine($report, 10, ['PSR12.Keywords.ShortFormTypeKeywords']);
    }
}
