<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

declare(strict_types=1);

namespace Vaimo\Helpers;

use PHPUnit\Framework\TestCase;

class StringHelperTest extends TestCase
{
    public function testSplitCamelCaseToWords(): void
    {
        self::assertSame(
            ['split', 'camel', 'case', 'to', 'words'],
            StringHelper::splitCamelCaseToWords('splitCamelCaseToWords')
        );
    }
}
