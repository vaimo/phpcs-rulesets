<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

declare(strict_types=1);

namespace Vaimo\Sniffs\Exceptions;

use SlevomatCodingStandard\Sniffs\TestCase;

class TryProcessSystemResourcesSniffTest extends TestCase
{
    public function testTryProcessSystemResources(): void
    {
        $file = self::checkFile(__DIR__ . '/data/TryProcessSystemResources/TryProcessSystemResources.php');

        self::assertSniffError($file, 18, TryProcessSystemResourcesSniff::CODE_FOUND);
        self::assertSniffError($file, 23, TryProcessSystemResourcesSniff::CODE_FOUND);
    }

    public function testAllowSystemResourcesInTheUseStatement(): void
    {
        $file = self::checkFile(__DIR__ . '/data/TryProcessSystemResources/TryProcessSystemResources.php');

        self::assertNoSniffError($file, 15);
    }

    public function testAllowSystemResourcesInTryStatement(): void
    {
        $file = self::checkFile(__DIR__ . '/data/TryProcessSystemResources/TryProcessSystemResources.php');

        self::assertNoSniffError($file, 21);
    }
}
