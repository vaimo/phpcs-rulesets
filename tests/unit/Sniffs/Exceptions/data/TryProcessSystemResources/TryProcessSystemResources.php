<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

declare(strict_types=1);

namespace  Vaimo;

use Throwable;

use function rewind;
use function stream_get_contents,
    stream_bucket_append;

stream_get_contents($stream);

try {
    stream_get_contents($steam2);
} catch (Throwable $t) {
    stream_get_contents($steam3);
}
