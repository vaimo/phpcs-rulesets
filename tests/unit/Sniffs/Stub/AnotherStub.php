<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

namespace Vaimo\Sniffs\Stub;

class AnotherStub
{
    public function doSomething($parameter)
    {
        
    }
}
