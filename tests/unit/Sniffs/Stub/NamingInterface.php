<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

namespace Vaimo\Sniffs\Stub;

interface NamingInterface
{
    function findDiamondOrGold($ores, $secondParameter): ?string;
}
