<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

namespace Vaimo\Sniffs\Stub;

class ParentNamingStub
{
    protected $diamondOre;
    protected $diamondOrGold;
    protected $isAndroid;
    protected $isThatAndThis;

    function findDiamondOrGold($ores): ?string
    {
        return $ores['diamond'] ?? $ores['gold'] ?? null;
    }
}
