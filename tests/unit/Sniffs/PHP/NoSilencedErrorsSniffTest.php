<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

declare(strict_types=1);

namespace Vaimo\Sniffs\PHP;

use SlevomatCodingStandard\Sniffs\TestCase;

class NoSilencedErrorsSniffTest extends TestCase
{
    public function testIgnoresExcludedFunctions(): void
    {
        $file = self::checkFile(__DIR__ . '/data/NoSilencedErrors/Correct.php', [
            'error' => true,
            'allowedFunctions' => ['trigger_error'],
        ]);

        self::assertNoSniffErrorInFile($file);
    }

    public function testFindsSilencedErrors(): void
    {
        $file = self::checkFile(__DIR__ . '/data/NoSilencedErrors/Failed.php', [
            'error' => true,
        ]);

        self::assertSniffError($file, 8, 'Forbidden');
    }
}
