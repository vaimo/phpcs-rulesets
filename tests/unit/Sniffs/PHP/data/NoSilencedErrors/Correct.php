<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

use function trigger_error as test;

@trigger_error('This is deprecated.', E_USER_DEPRECATED);
@\trigger_error('This is deprecated too.', E_USER_DEPRECATED);

@test('This is deprecated as well.', E_USER_DEPRECATED);
