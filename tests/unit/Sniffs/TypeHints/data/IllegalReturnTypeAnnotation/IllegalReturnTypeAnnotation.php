<?php
/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

class A {
    public function getMessage(): string
    {
        return '';
    }

    /**
     * @return mixed[]
     */
    public function getJsLayout(): array
    {
        return [];
    }

    /**
     * @return mixed
     */
    public function getMixed()
    {
        return null;
    }

    /**
     * @return string
     * @return mixed
     */
    public function getSomethingWeird()
    {
        return 'weird';
    }
}
