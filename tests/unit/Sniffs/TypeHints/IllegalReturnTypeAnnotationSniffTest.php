<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

declare(strict_types=1);

namespace Vaimo\Sniffs\TypeHints;

use SlevomatCodingStandard\Sniffs\TestCase;

class IllegalReturnTypeAnnotationSniffTest extends TestCase
{
    public function testSniff(): void
    {
        $file = self::checkFile(__DIR__ . '/data/IllegalReturnTypeAnnotation/IllegalReturnTypeAnnotation.php');

        self::assertNoSniffError($file, 8);
        self::assertNoSniffError($file, 14);
        self::assertSniffError($file, 22, IllegalReturnTypeAnnotationSniff::CODE_FOUND);
        self::assertSniffError($file, 31, IllegalReturnTypeAnnotationSniff::CODE_FOUND);

        self::assertSame(2, $file->getErrorCount());
    }
}
