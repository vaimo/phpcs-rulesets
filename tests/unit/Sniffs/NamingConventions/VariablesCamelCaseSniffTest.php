<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

declare(strict_types=1);

namespace Vaimo\Sniffs\NamingConventions;

use SlevomatCodingStandard\Sniffs\TestCase;

class VariablesCamelCaseSniffTest extends TestCase
{
    public function testSnakeCase(): void
    {
        $file = self::checkFile(__DIR__ . '/data/VariablesCamelCase/snake_case.php');

        self::assertSame(4, $file->getErrorCount());

        self::assertSniffError($file, 9, VariablesCamelCaseSniff::PROPERTY_CODE);
        self::assertSniffError($file, 11, VariablesCamelCaseSniff::PARAMETER_CODE);
        self::assertSniffError($file, 13, VariablesCamelCaseSniff::VARIABLE_CODE);
        self::assertSniffError($file, 15, VariablesCamelCaseSniff::VARIABLE_CODE);

        foreach ([26, 28, 30, 32, 34, 36, 39, 41] as $line) {
            self::assertNoSniffError($file, $line);
        }
    }
}
