<?php
/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

class NonExistingClass extends \ClassDoesntExist
{
    protected $diamondOrGold;
    protected $isThatAndThis;
}
