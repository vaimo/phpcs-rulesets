<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

use Vaimo\Sniffs\Stub\ParentNamingStub;

class CurrentNamingClass extends ParentNamingStub
{
    protected $diamondOrGold;
    protected $isThatAndThis;
}
