<?php
/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

$diamondOre = new Ore(OreCodes::DIAMOND);
$diamondOrGold = in_array($ore->getCode(), [OreCodes::DIAMOND, OreCodes::GOLD]);

$android = new Mobile('Android');
$thisAndThat = doThis() && doThat();

class Test
{
    private $diamondOre;

    private $diamondOrGold;

    private $isAndroid;

    private $isThatAndThis;
}
