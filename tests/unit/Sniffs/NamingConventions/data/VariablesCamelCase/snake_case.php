<?php
/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

class snake_case
{
    private $snake_case_property;

    public function snake_case_method(string $snake_case_parameter)
    {
        new \StdClass($snake_case_parameter);

        return $snake_case_parameter == 0;
    }

    public function testObjectSnakeCaseProperty($object)
    {
        return $object->is_valid;
    }
}

class CamelCase
{
    private $_urlBuilder;

    private $camelCaseProperty;

    public function camelCaseMethod(string $camelCaseParameter)
    {
        $camelCaseVariable = $_GET['test'];

        $anotherVariable = "$camelCaseVariable";

        return $camelCaseParameter == $anotherVariable;
    }

    protected function _getUrl()
    {
        return $this->_urlBuilder->getUrl('*/*/*');
    }
}


