<?php
/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

class AndOrInMethodNameNonExistingClass extends \ClassThatDoesNotExist
{
    public function findDiamondOrGold($ores): ?string
    {
        return parent::findDiamondOrGold(
            array_map('strtolower', $ores)
        );
    }
}

class AndOrInMethodNameNonExistingInterface implements \InterfaceThatDoesNotExist
{
    function findDiamondOrGold($ores): ?string
    {
        return $ores['diamond'] ?? $ores['gold'] ?? null;
    }
}
