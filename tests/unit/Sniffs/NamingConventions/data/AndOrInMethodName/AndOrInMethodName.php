<?php
/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

function findDiamondOre($ores): ?string
{
    return $ores['diamond'] ?? null;
}

function findDiamondOrGold($ores): ?string
{
    return $ores['diamond'] ?? $ores['gold'] ?? null;
}

function doThisAndThat(): void
{
    doThis();
    doThat();
}

function isAndroid(): bool
{
    return stripos($_SERVER['HTTP_USER_AGENT'], 'android') !== false;
}


array_map(function($i) { return $i;}, []);
