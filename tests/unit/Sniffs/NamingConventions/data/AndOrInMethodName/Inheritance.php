<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

use Vaimo\Sniffs\Stub\NamingInterface;
use Vaimo\Sniffs\Stub\ParentNamingStub;

class NamingStub extends ParentNamingStub
{
    public function findDiamondOrGold($ores): ?string
    {
        return parent::findDiamondOrGold(
            array_map('strtolower', $ores)
        );
    }
}

class InterfaceNaming implements NamingInterface
{
    function findDiamondOrGold($ores, $secondParameter): ?string
    {
        return $ores['diamond'] ?? $ores['gold'] ?? null;
    }
}
