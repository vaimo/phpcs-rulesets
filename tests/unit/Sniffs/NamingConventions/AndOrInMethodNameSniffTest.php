<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

declare(strict_types=1);

namespace Vaimo\Sniffs\NamingConventions;

use SlevomatCodingStandard\Sniffs\TestCase;

// phpcs:ignore Vaimo.NamingConventions.AndOrInClassName -- intentional
class AndOrInMethodNameSniffTest extends TestCase
{
    public function testSniff(): void
    {
        $file = self::checkFile(__DIR__ . '/data/AndOrInMethodName/AndOrInMethodName.php');

        self::assertNoSniffError($file, 7);
        self::assertSniffError(
            $file,
            12,
            AndOrInMethodNameSniff::CODE_FOUND
        );

        self::assertSniffError(
            $file,
            17,
            AndOrInMethodNameSniff::CODE_FOUND
        );
        self::assertNoSniffError($file, 23);

        // no unexpected errors
        self::assertSame(2, $file->getErrorCount());
    }

    public function testInheritedFunctions(): void
    {
        $fileName = __DIR__ . '/data/AndOrInMethodName/Inheritance.php';

        $file = self::checkFile($fileName, [
            'considerInheritedOnReflectionError' => false,
        ]);

        self::assertNoSniffError($file, 13);
        self::assertNoSniffError($file, 15);

        self::assertNoSniffError($file, 23);
    }

    public function testConsidersInheritedOnRefectionError(): void
    {
        $fileName = __DIR__ . '/data/AndOrInMethodName/NonExistingClass.php';

        $file = self::checkFile($fileName, [
            'considerInheritedOnReflectionError' => false,
        ]);

        self::assertSniffError($file, 9, AndOrInMethodNameSniff::CODE_FOUND);
        self::assertSniffError($file, 19, AndOrInMethodNameSniff::CODE_FOUND);

        self::assertSame(2, $file->getErrorCount());
    }

    public function testRaisesErrorOnReflectionError(): void
    {
        $fileName = __DIR__ . '/data/AndOrInMethodName/NonExistingClass.php';

        $file = self::checkFile($fileName, [
            'considerInheritedOnReflectionError' => true,
        ]);

        self::assertNoSniffErrorInFile($file);
    }
}
