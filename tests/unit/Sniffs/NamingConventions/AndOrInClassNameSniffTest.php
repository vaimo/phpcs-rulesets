<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

declare(strict_types=1);

namespace Vaimo\Sniffs\NamingConventions;

use SlevomatCodingStandard\Sniffs\TestCase;

// phpcs:ignore Vaimo.NamingConventions.AndOrInClassName -- intentional
class AndOrInClassNameSniffTest extends TestCase
{
    public function testSniff(): void
    {
        $file = self::checkFile(__DIR__ . '/data/AndOrInClassName/AndOrInClassName.php');

        self::assertNoSniffError($file, 7);
        self::assertSniffError(
            $file,
            11,
            AndOrInClassNameSniff::CODE_FOUND,
            'Class name "DiamondOrGold" contains illegal keyword "or".'
        );

        self::assertNoSniffError($file, 15);
        self::assertNoSniffError($file, 21);

        self::assertSniffError(
            $file,
            25,
            AndOrInClassNameSniff::CODE_FOUND,
            'Class name "ThisAndThat" contains illegal keyword "and".'
        );
    }
}
