<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

declare(strict_types=1);

namespace Vaimo\Sniffs\NamingConventions;

use SlevomatCodingStandard\Sniffs\TestCase;

// phpcs:ignore Vaimo.NamingConventions.AndOrInClassName -- intentional
class AndOrInVariableNameSniffTest extends TestCase
{
    public function testVariables(): void
    {
        $file = self::checkFile(__DIR__ . '/data/AndOrInVariableName/AndOrInVariableName.php');

        self::assertNoSniffError($file, 7);
        self::assertSniffError($file, 8, AndOrInVariableNameSniff::CODE_FOUND);

        self::assertNoSniffError($file, 10);
        self::assertSniffError($file, 11, AndOrInVariableNameSniff::CODE_FOUND);

        self::assertNoSniffError($file, 15);
        self::assertSniffError($file, 17, AndOrInVariableNameSniff::CODE_FOUND);

        self::assertNoSniffError($file, 19);
        self::assertSniffError($file, 21, AndOrInVariableNameSniff::CODE_FOUND);
    }

    public function testInheritance(): void
    {
        $file = self::checkFile(__DIR__ . '/data/AndOrInVariableName/Inheritance.php');

        self::assertNoSniffError($file, 17);
        self::assertNoSniffError($file, 18);
    }

    public function testConsidersInheritedOnReflectionError(): void
    {
        $file = self::checkFile(__DIR__ . '/data/AndOrInVariableName/NonExistingClass.php', [
            'considerInheritedOnReflectionError' => true,
        ]);

        self::assertNoSniffErrorInFile($file);
    }

    public function testDoesNotConsiderInheritedOnReflectionError(): void
    {
        $file = self::checkFile(__DIR__ . '/data/AndOrInVariableName/NonExistingClass.php', [
            'considerInheritedOnReflectionError' => false,
        ]);

        self::assertSniffError($file, 9, AndOrInVariableNameSniff::CODE_FOUND);
        self::assertSniffError($file, 10, AndOrInVariableNameSniff::CODE_FOUND);
    }
}
