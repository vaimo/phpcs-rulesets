<?php
/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

namespace Vaimo\Sniffs\CodeAnalysis\data\UnusedFunctionParameter;

use Vaimo\Sniffs\Stub\NamingInterface;

final class InheritedFromInterface implements NamingInterface
{
    /**
     * @inheritDoc
     */
    function findDiamondOrGold($ores, $secondParameter): ?string
    {
        return null;
    }
}
