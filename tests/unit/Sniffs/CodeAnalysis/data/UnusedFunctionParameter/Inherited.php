<?php
/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */
 
class AwesomeClass extends \Vaimo\Sniffs\Stub\AnotherStub
{
    public function doSomething($parameter)
    {
        $a = 1 + 1;

        return $a;
    }

    public function test($parameter)
    {
        $a = 1 + 1;

        $b = <<<STR
{$this->doSomething($a)} 
STR;

        return $a;
    }
}
