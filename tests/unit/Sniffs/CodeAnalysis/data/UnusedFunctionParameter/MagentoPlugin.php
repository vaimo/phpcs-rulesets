<?php
/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */
 
class MagentoPlugin
{
    public function aroundSomeFunction($subject, $proceed, $unusedParameter)
    {
        $a = 1 + 1;

        return $a;
    }

    public function doSomething($proceed)
    {
        $a = 1 + 1;

        return $a;
    }

    public function doAnother($subject)
    {

    }
}
