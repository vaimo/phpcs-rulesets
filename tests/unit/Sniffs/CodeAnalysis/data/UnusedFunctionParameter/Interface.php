<?php
/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */
 
interface A {
    public function doSomething($p);
}

abstract class B {
    abstract protected function execute(A $parameters);
}
