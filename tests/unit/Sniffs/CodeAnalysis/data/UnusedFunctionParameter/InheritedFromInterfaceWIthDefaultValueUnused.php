<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

namespace Vaimo\Sniffs\CodeAnalysis\data\UnusedFunctionParameter;

use Vaimo\Sniffs\Stub\NamingInterface;

class InheritedFromInterfaceWIthDefaultValueUnused implements NamingInterface
{
    function findDiamondOrGold($ores, $secondParameter, $thirdParameter = 5): ?string
    {
        return 1 + 5;
    }
}

class InheritedFromInterfaceWIthDefaultValueUsed implements NamingInterface
{
    function findDiamondOrGold($ores, $secondParameter, $thirdParameter = 5): ?string
    {
        return 1 + $thirdParameter;
    }
}
