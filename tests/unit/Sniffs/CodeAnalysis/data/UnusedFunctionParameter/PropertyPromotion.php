<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

namespace Vaimo\Sniffs\CodeAnalysis\data\UnusedFunctionParameter;

class PropertyPromotion
{
    public function __construct(
        private readonly WhateverInterface $property,
        private readonly array $list = []
    ) {}
}
