<?php
/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

class ExtendsNonExistentClass extends ClassThatDoesntExist {

    public function doSomething($a)
    {

    }

}
