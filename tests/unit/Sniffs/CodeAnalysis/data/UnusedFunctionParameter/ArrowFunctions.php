<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

array_filter(['bananas', 'oranges'], fn ($item) => true);

array_filter(['bananas', 'oranges'], fn ($item) => $item === 'bananas');
