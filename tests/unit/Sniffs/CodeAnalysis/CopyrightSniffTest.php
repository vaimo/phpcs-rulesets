<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

declare(strict_types=1);

namespace Vaimo\Sniffs\CodeAnalysis;

use SlevomatCodingStandard\Sniffs\TestCase;

use function sprintf;

class CopyrightSniffTest extends TestCase
{
    public function testCopyrightMissing(): void
    {
        $file = self::checkFile(__DIR__ . '/data/Copyright/IsMissing.php');

        self::assertSniffError($file, 3, CopyrightSniff::CODE_MISSING, CopyrightSniff::MESSAGE_MISSING);
    }

    public function testCopyrightIsNotAfterOpenTag(): void
    {
        $file = self::checkFile(__DIR__ . '/data/Copyright/IsNotAfterOpenTag.php');

        self::assertSniffError($file, 3, CopyrightSniff::CODE_MISSING, CopyrightSniff::MESSAGE_MISSING);
    }

    public function testComparesFoundCommentWithPattern(): void
    {
        $file = self::checkFile(__DIR__ . '/data/Copyright/Wrong.php');
        $message = sprintf(
            CopyrightSniff::MESSAGE_INCORRECT,
            (new CopyrightSniff())->example
        );

        self::assertSniffError($file, 2, CopyrightSniff::CODE_INCORRECT, $message);
    }

    public function testCorrectCopyright(): void
    {
        $correctFiles = [
            __DIR__ . '/data/Copyright/Correct.php',
            __DIR__ . '/data/Copyright/CorrectAscii.php',
            __DIR__ . '/data/Copyright/CorrectUpper.php',
            __DIR__ . '/data/Copyright/CorrectCommentBlock.php',
        ];

        foreach ($correctFiles as $file) {
            self::assertNoSniffErrorInFile(self::checkFile($file));
        }
    }

    public function testSkipsSecondOpenTag(): void
    {
        $file = self::checkFile(
            __DIR__ . '/data/Copyright/SecondOpenTag.phtml',
            [],
            [CopyrightSniff::CODE_MISSING, CopyrightSniff::CODE_INCORRECT]
        );

        self::assertNoSniffErrorInFile($file);
    }

    /**
     * Can happen during live checks
     */
    public function testSkipsOnEmptyFile(): void
    {
        self::assertNoSniffErrorInFile(
            self::checkFile(__DIR__ . '/data/Copyright/EmptyFile.php')
        );
    }

    public function testSkipsAfterCheckIfCommentIsLast(): void
    {
        self::assertNoSniffErrorInFile(
            self::checkFile(__DIR__ . '/data/Copyright/IsLastContent.php')
        );
    }
}
