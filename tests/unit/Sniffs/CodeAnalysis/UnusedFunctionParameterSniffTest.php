<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

declare(strict_types=1);

namespace Vaimo\Sniffs\CodeAnalysis;

use SlevomatCodingStandard\Sniffs\TestCase;

class UnusedFunctionParameterSniffTest extends TestCase
{
    public function testIgnoresAllowedMethods(): void
    {
        $file = self::checkFile(__DIR__ . '/data/UnusedFunctionParameter/MagentoPlugin.php', [
            'allowedMethodPrefixes' => ['doSomething', 'around'],
            'allowedMethodParameters' => [],
        ]);

        self::assertSame(1, $file->getErrorCount());

        self::assertSniffError($file, 23, UnusedFunctionParameterSniff::ERROR_CODE);
    }

    public function testIgnoresAllowedParameterNames(): void
    {
        $file = self::checkFile(__DIR__ . '/data/UnusedFunctionParameter/MagentoPlugin.php', [
            'allowedMethodPrefixes' => ['after'],
            'allowedMethodParameters' => ['$subject', '$proceed'],
        ]);

        self::assertSame(1, $file->getErrorCount());

        self::assertSniffError($file, 9, UnusedFunctionParameterSniff::ERROR_CODE);
    }

    public function testIgnoresInheritedParameters(): void
    {
        $file = self::checkFile(__DIR__ . '/data/UnusedFunctionParameter/Inherited.php');

        self::assertSame(1, $file->getErrorCount());
        self::assertNoSniffError($file, 9);
        self::assertSniffError($file, 16, UnusedFunctionParameterSniff::ERROR_CODE);
    }

    public function testIgnoresParameterInheritedFromInterface(): void
    {
        $file = self::checkFile(__DIR__ . '/data/UnusedFunctionParameter/InheritedFromInterface.php');

        self::assertNoSniffErrorInFile($file);
    }

    public function testChecksAdditionalParametersWithDefaultValueOnInheritedFunctions(): void
    {
        $file = self::checkFile(
            __DIR__ . '/data/UnusedFunctionParameter/InheritedFromInterfaceWIthDefaultValueUnused.php'
        );

        self::assertSniffError($file, 14, UnusedFunctionParameterSniff::ERROR_CODE);
        self::assertNoSniffError($file, 22);
    }

    public function testSuccessfullyParses(): void
    {
        // file copied from original sniff test
        $file = self::checkFile(__DIR__ . '/data/UnusedFunctionParameter/Examples.php');

        self::assertSniffError($file, 3, UnusedFunctionParameterSniff::ERROR_CODE);
        self::assertSniffError($file, 7, UnusedFunctionParameterSniff::ERROR_CODE);
        self::assertSniffError($file, 68, UnusedFunctionParameterSniff::ERROR_CODE);
    }

    public function testDoesNotConsiderInheritedOnReflectionError(): void
    {
        $file = self::checkFile(__DIR__ . '/data/UnusedFunctionParameter/ExtendsNonExistentClass.php', [
            'considerInheritedOnReflectionError' => false,
        ]);

        self::assertSame(1, $file->getErrorCount());
        self::assertSniffError($file, 9, UnusedFunctionParameterSniff::ERROR_CODE);
    }

    public function testConsidersInheritedOnReflectionError(): void
    {
        $file = self::checkFile(__DIR__ . '/data/UnusedFunctionParameter/ExtendsNonExistentClass.php', [
            'considerInheritedOnReflectionError' => true,
        ]);

        self::assertNoSniffErrorInFile($file);
    }

    public function testDoesntFailOnAbstractions(): void
    {
        self::assertNoSniffErrorInFile(
            self::checkFile(__DIR__ . '/data/UnusedFunctionParameter/Interface.php')
        );
    }

    public function testHandlesPropertyPromotion(): void
    {
        self::assertNoSniffErrorInFile(
            self::checkFile(__DIR__ . '/data/UnusedFunctionParameter/PropertyPromotion.php')
        );
    }

    public function testHandlesArrowFunctions(): void
    {
        $file = self::checkFile(__DIR__ . '/data/UnusedFunctionParameter/ArrowFunctions.php');

        self::assertSniffError($file, 8, UnusedFunctionParameterSniff::ERROR_CODE);
        self::assertNoSniffError($file, 10);
    }
}
