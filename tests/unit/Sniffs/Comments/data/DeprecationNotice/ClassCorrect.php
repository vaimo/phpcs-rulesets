<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

namespace Vaimo\Sniffs\Comments\data;

/**
 * @deprecated use AnotherClass instead
 */
class ClassCorrect
{
    public function __construct()
    {
        @trigger_error(
            sprintf('Use of %s class is deprecated, use %s instead', self::class, 'LoggerInterface'),
            E_USER_DEPRECATED
        );
    }
}
