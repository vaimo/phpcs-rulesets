<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

namespace Vaimo\Sniffs\Comments\data;

/**
 * @deprecated use AnotherClass instead
 */
class ClassHasNoConstructor
{
    private $whatever;

    public function getWhatever()
    {
        return $this->whatever;
    }

    public function setWhatever($whatever)
    {
        $this->whatever = $whatever;
    }
}
