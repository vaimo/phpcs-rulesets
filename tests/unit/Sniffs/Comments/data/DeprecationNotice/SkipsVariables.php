<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

class Example
{
    /**
     * @deprecated to be removed in 3.0
     */
    protected $field;
}
