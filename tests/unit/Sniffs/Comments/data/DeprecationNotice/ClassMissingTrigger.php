<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

/**
 * @deprecated
 */
class Example
{
    private $a;

    public function getA()
    {
        return $this->a;
    }

    public function __construct($a)
    {
        $this->a = $a;
    }
}
