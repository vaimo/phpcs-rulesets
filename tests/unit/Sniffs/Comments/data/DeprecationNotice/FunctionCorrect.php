<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

class Example
{
    /**
     * @deprecated use Example::getConstraints() instead
     */
    public function getConstreints()
    {
        @trigger_error(sprintf('%s is deprecated, use %s instead', __METHOD__, 'Example::getConstraints'), E_USER_DEPRECATED);
        return $this->getConstraints();
    }

    public function getConstraints()
    {
        return [];
    }
}
