<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

interface Example {
    /**
     * @deprecated
     */
    function getWhatever();
}

/**
 * @deprecated
 */
