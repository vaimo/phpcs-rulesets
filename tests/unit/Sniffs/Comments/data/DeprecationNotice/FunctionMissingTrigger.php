<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

class Example
{
    /**
     * @deprecated use Example::getConstraints() instead
     * @since 1.3.0
     */
    public function getConstreints()
    {
        return $this->getConstraints();
    }

    /**
     * @deprecated no alternative, if you need it: you're doing something very wrong.
     * @return ConstraintInterface[]
     */
    public function getConstraints()
    {
        trigger_error('OOOOPSIE', E_USER_ERROR);

        /** @var ConstraintInterface[] $array */
        $array = [];
        return $array;
    }
}
