<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

declare(strict_types=1);

namespace Vaimo\Sniffs\Comments;

use SlevomatCodingStandard\Sniffs\TestCase;

class DeprecationNoticeSniffTest extends TestCase
{
    public function testClassHasErrorTriggered(): void
    {
        self::assertNoSniffErrorInFile(self::checkFile(__DIR__ . '/data/DeprecationNotice/ClassCorrect.php'));
    }

    public function testFunctionHasErrorTriggered(): void
    {
        self::assertNoSniffErrorInFile(self::checkFile(__DIR__ . '/data/DeprecationNotice/FunctionCorrect.php'));
    }

    public function testMissingTriggerOnFunction(): void
    {
        $file = self::checkFile(__DIR__ . '/data/DeprecationNotice/FunctionMissingTrigger.php');
        self::assertSniffError($file, 11, DeprecationNoticeSniff::CODE_ERROR_MISSING);
    }

    public function testMissingTriggerOnClass(): void
    {
        $file = self::checkFile(__DIR__ . '/data/DeprecationNotice/ClassMissingTrigger.php');
        self::assertSniffError($file, 9, DeprecationNoticeSniff::CODE_ERROR_MISSING);
        self::assertSniffError($file, 9, DeprecationNoticeSniff::CODE_CONTENT_MISSING);
    }

    public function testMissingTriggerOnClassWithoutConstructor(): void
    {
        $file = self::checkFile(__DIR__ . '/data/DeprecationNotice/ClassHasNoConstructor.php');
        self::assertSniffError($file, 11, DeprecationNoticeSniff::CODE_ERROR_MISSING);
    }

    public function testSkipsOnVariables(): void
    {
        self::assertNoSniffErrorInFile(self::checkFile(__DIR__ . '/data/DeprecationNotice/SkipsVariables.php'));
    }

    public function testSkipsOnParsingErrors(): void
    {
        self::assertNoSniffErrorInFile(self::checkFile(
            __DIR__ . '/data/DeprecationNotice/SkipsOnParsingErrors.php',
            [],
            [DeprecationNoticeSniff::CODE_ERROR_MISSING]
        ));
    }
}
