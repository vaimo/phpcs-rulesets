<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

declare(strict_types=1);

namespace Vaimo\Sniffs\Classes;

use SlevomatCodingStandard\Sniffs\TestCase;

class ProtectedMemberSniffTest extends TestCase
{
    public function testDisallowsProtectedMembers(): void
    {
        $file = self::checkFile(__DIR__ . '/data/ProtectedMember/ProtectedMember.php');

        self::assertSniffError(
            $file,
            8,
            ProtectedMemberSniff::CODE_PROPERTY,
            'Protected property "a" found in class \ProtectedMemberA'
        );

        self::assertSniffError(
            $file,
            10,
            ProtectedMemberSniff::CODE_FUNCTION,
            'Protected function "b" found in class \ProtectedMemberA'
        );

        self::assertSniffError(
            $file,
            14,
            ProtectedMemberSniff::CODE_PROPERTY,
            'Protected property "b" found in class \ProtectedMemberB'
        );

        self::assertSniffError(
            $file,
            16,
            ProtectedMemberSniff::CODE_CONSTANT,
            'Protected constant "CONSTANT" found in class \ProtectedMemberB'
        );
    }

    public function testAllowsProtectedPropertiesInAbstractClasses(): void
    {
        $file = self::checkFile(__DIR__ . '/data/ProtectedMember/ProtectedMember.php', [
            'allowProtectedPropertyInAbstractClass' => true,
        ]);

        self::assertNoSniffError($file, 14);
    }

    public function testAllowsAbstractMembers(): void
    {
        self::assertNoSniffErrorInFile(
            self::checkFile(__DIR__ . '/data/ProtectedMember/Abstract.php')
        );
    }

    public function testDisallowsAbstractMembers(): void
    {
        $file = self::checkFile(__DIR__ . '/data/ProtectedMember/Abstract.php', [
            'allowAbstract' => false,
        ]);

        self::assertSniffError($file, 8, ProtectedMemberSniff::CODE_FUNCTION);
    }

    public function testAllowsInheritedMembers(): void
    {
        self::assertNoSniffErrorInFile(
            self::checkFile(__DIR__ . '/data/ProtectedMember/Inherited.php')
        );
    }

    public function testDisallowsInheritedMembers(): void
    {
        $file = self::checkFile(__DIR__ . '/data/ProtectedMember/Inherited.php', [
            'allowInherited' => false,
            'considerInheritedOnReflectionError' => false,
        ]);

        self::assertSniffError($file, 9, ProtectedMemberSniff::CODE_PROPERTY);
        self::assertSniffError($file, 11, ProtectedMemberSniff::CODE_FUNCTION);
    }

    public function testConsidersInheritedOnReflectionError(): void
    {
        $file = self::checkFile(__DIR__ . '/data/ProtectedMember/InheritedNonExistingClass.php', [
            'allowInherited' => true,
            'considerInheritedOnReflectionError' => true,
        ]);

        self::assertNoSniffErrorInFile($file);
    }

    public function testAllowsProtectedFunctionsInAbstractClasses(): void
    {
        $file = self::checkFile(__DIR__ . '/data/ProtectedMember/ProtectedFunctionInAbstractClass.php', [
            'allowProtectedFunctionInAbstractClass' => true,
        ]);

        self::assertNoSniffErrorInFile($file);
    }

    public function testDisallowsProtectedFunctionsInAbstractClasses(): void
    {
        $file = self::checkFile(__DIR__ . '/data/ProtectedMember/ProtectedFunctionInAbstractClass.php', [
            'allowProtectedFunctionInAbstractClass' => false,
        ]);

        self::assertSniffError($file, 10, ProtectedMemberSniff::CODE_FUNCTION);
    }

    public function testDisallowsProtectedPromotedProperties(): void
    {
        $file = self::checkFile(__DIR__ . '/data/ProtectedMember/PromotedProperty.php');

        self::assertSniffError($file, 10, ProtectedMemberSniff::CODE_PROPERTY);
    }
}
