<?php
/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

throw new \Exception('Ooops!');

$e = new \Exception('I did it again!');
throw $e;

$exceptionClass = \Exception::class;

throw new $exceptionClass('Oh, baby-baby!');

$e = new $exceptionClass('Oooops, I did it again!');
throw $e;
