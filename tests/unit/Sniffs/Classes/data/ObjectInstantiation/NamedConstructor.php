<?php
/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

namespace Vaimo\Sniffs\Classes\data;

class ObjectInstantiationNamedConstructor
{
    public static function create() {
        return new self;
    }
}

class StaticNamedConstructor
{
    private $var;

    public function __construct($var)
    {
        $this->var = $var;
    }

    public static function create($var) {
        return new static($var);
    }
}

class NamedConstructor
{
    private $var;

    public function __construct($var)
    {
        $this->var = $var;
    }

    public static function create($var) {
        return new NamedConstructor($var);
    }
}
