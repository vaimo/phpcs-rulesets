<?php
/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

use Vaimo\Sniffs\Stub\ FinalStub
    as AnotherClass;
use Vaimo\Sniffs\Stub;
use Vaimo\Sniffs\Stub\{
    TestStub,
    AnotherStub as ThirdClass
};

new TestStub();
new ThirdClass();
new Stub\FinalStub();
new \Vaimo\Sniffs\Stub\FinalStub();
new Vaimo\Sniffs\Stub\FinalStub();
new AnotherClass();

new \DateTime();

$a = 1;

$c = static function () use ($a) {
    return $a + 1;
};
