<?php
/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

$className = \Vaimo\Sniffs\Stub\FinalStub::class;

new $className;


class Test {
    public function getName()
    {
        return \Vaimo\Sniffs\Stub\FinalStub::class;
    }

    public function doSomething()
    {
        return new ($this->getName())();
    }
}
