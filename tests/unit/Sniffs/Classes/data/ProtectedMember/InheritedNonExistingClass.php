<?php
/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

class ExtendsNotExistingClass extends \ClassThatDoesntExist
{
    protected $g = '';

    protected function doSomething()
    {
        return $this->g;
    }
}
