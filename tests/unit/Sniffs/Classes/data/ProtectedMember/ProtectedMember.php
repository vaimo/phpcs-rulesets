<?php
/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */
 
class ProtectedMemberA {
    protected $a;

    protected function b() {}
}

abstract class ProtectedMemberB {
    protected $b;

    protected const CONSTANT = 'test';

    private function doWhatever() {
        return $this->b;
    }

    public function getConstant()
    {
        return self::CONSTANT;
    }
}
