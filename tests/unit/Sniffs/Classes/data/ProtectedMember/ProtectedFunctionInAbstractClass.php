<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

abstract class ProtectedFunctionInAbstractClass
{
    protected function doStuff(): void
    {
    }
}
