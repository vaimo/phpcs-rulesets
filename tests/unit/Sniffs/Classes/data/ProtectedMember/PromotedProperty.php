<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

class PromotedProperty
{
    public function __construct(protected \Whatever $whatever) {}
}
