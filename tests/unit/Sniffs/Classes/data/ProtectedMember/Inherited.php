<?php
/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

class Inherited extends \Vaimo\Sniffs\Stub\TestStub
{
    protected $g = '';

    protected function doSomething()
    {
        return $this->g;
    }
}
