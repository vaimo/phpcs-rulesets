<?php
/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

class Test {
    /**
     * Constant commentary
     */
    public const TEST = 'value';

    const SECOND = 'const';

    public $var;

    private const ANOTHER_CONSTANT = 'test';
}

const NON_CLASS_CONSTANT = 'test';
