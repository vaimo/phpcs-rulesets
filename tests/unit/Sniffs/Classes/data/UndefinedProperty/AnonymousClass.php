<?php
/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

namespace Vaimo\Sniffs\Classes\data\PropertyDeclaration;

class AnonymousClass
{
    private function createDictionaryProvider(string $name, array $items): ProviderInterface
    {
        return new class ($name, $items) implements ProviderInterface {
            private $items;

            /**
             * @param string[] $items
             */
            public function __construct(string $name, array $items)
            {
                $this->name = $name;
                $this->items = $items;
            }

            public function loadDictionaries(): array
            {
                return [new Dictionary($this->name)];
            }
        };
    }
}
