<?php
/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */
 
class A {
    public $a;
    protected $b;
    private $c,
            $g;

    public function doSomething()
    {
        $this->c = $this->a + $this->f;
        $this->d;
        $this->a = function () {};
        ($this->a)();
        $this->doSomething();
    }
}

class C extends \Vaimo\Sniffs\Stub\TestStub
{
    // TestStub declares `g` as protected and `b` as private
    // so that we expect failure on line 37 and DO NOT expect it on 36
    private $a;
    public function doSomething()
    {
        $a = new A();
        $a->doSomething();

        $this->a = new A();
        $this->a->doSomething();

        $this->g;
        return $this->b;
    }
}

class D {
    private $a, $b;

    public function getSomeString()
    {
        return <<<STRING
Hello, {$this->a}!

We are on {$this->b },
 {$this->
        b}
                and {$this->
        c}.
STRING;
    }

    public function getSomeOtherString()
    {
        return "{$this->b}" . "{$this->
        c}";
    }

    public function getAnotherString()
    {
        $a = "Hello,
        World!";

        return <<<STRING
Hello, 
World!
STRING;
    }

    public function nowdoc()
    {
        return <<<'NOWDOC'
${this->c} {$this->cs}
NOWDOC;
    }

    public function propertyOfProperty()
    {
        return $this->a->b()->c;
    }
}


