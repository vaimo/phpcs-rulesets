<?php
/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */
class NonExistingClass extends \ClassThatDoesntExist
{
    protected function doSomething()
    {
        return $this->g;
    }
}
