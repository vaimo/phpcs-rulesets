<?php
/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

class A {
    /**
     * @var B
     */
    private $b;

    public function __construct(int $b)
    {
        $this->b = $b;
    }

    public function doSomething()
    {
        $a = 'b';
        $b = 'c';
        return $this->$a + $this->$b;
    }
}
