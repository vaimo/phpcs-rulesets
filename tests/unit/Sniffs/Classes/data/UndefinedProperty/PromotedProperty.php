<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

namespace Vaimo\Sniffs\Classes\data\UndefinedProperty;

class PromotedProperty
{
    public function __construct(private \Whatever $whatever)
    {
    }

    public function getWhatever()
    {
        return $this->whatever;
    }
}
