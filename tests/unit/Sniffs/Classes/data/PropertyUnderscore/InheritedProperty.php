<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

class InheritedProperty extends \Vaimo\Sniffs\Stub\UnderscorePropertiesStub
{
    protected $_options = ['option' => 'value'];
}
