<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

class ExtendsNotExistingClass extends \ClassThatDoesntExist
{
    protected $_options = [];
}
