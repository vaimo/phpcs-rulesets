<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

namespace Vaimo\Sniffs\Classes\data\PropertyUnderscore;

class PromotedProperty
{
    public function __construct(
        private $_test,
        public readonly $_anotherProperty,
        private $totallyLegitProperty,
        public readonly $legitReadonlyProperty
    ) {
        $_x = 1 + 2;

        $test = "${_x}";
    }
}
