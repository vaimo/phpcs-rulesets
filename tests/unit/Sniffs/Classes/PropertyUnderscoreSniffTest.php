<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

declare(strict_types=1);

namespace Vaimo\Sniffs\Classes;

use SlevomatCodingStandard\Sniffs\TestCase;

class PropertyUnderscoreSniffTest extends TestCase
{
    public function testSniff(): void
    {
        $file = self::checkFile(__DIR__ . '/data/PropertyUnderscore/Underscore.php', [
            'allowInherited' => true,
            'skipOnReflectionError' => false,
        ]);

        self::assertSniffError($file, 10, PropertyUnderscoreSniff::CODE_FOUND);
        self::assertNoSniffError($file, 11);
        self::assertNoSniffError($file, 12);
    }

    public function testAllowsInheritedProperties(): void
    {
        $file = self::checkFile(__DIR__ . '/data/PropertyUnderscore/InheritedProperty.php', [
            'allowInherited' => true,
            'skipOnReflectionError' => false,
        ]);

        self::assertNoSniffErrorInFile($file);
    }

    public function testDisallowsInheritedProperties(): void
    {
        $file = self::checkFile(__DIR__ . '/data/PropertyUnderscore/InheritedProperty.php', [
            'allowInherited' => false,
            'skipOnReflectionError' => false,
        ]);

        self::assertSniffError($file, 10, PropertyUnderscoreSniff::CODE_FOUND);
    }

    public function testFailsOnReflectionError(): void
    {
        $file = self::checkFile(__DIR__ . '/data/PropertyUnderscore/InheritedNonExistingClass.php', [
            'allowInherited' => true,
            'skipOnReflectionError' => false,
        ]);

        self::assertSniffError($file, 10, PropertyUnderscoreSniff::CODE_FOUND);
    }

    public function testSkipsReflectionErrors(): void
    {
        $file = self::checkFile(__DIR__ . '/data/PropertyUnderscore/InheritedNonExistingClass.php', [
            'allowInherited' => true,
            'skipOnReflectionError' => true,
        ]);

        self::assertNoSniffErrorInFile($file);
    }

    public function testTriggersOnPromotedPropertiesWithUnderscore(): void
    {
        $file = self::checkFile(__DIR__ . '/data/PropertyUnderscore/PromotedProperty.php');

        self::assertSniffError($file, 13, PropertyUnderscoreSniff::CODE_FOUND);
        self::assertSniffError($file, 14, PropertyUnderscoreSniff::CODE_FOUND);
    }

    public function testDoesNotTriggerOnCorrectlyNamedPromotedProperties(): void
    {
        $file = self::checkFile(__DIR__ . '/data/PropertyUnderscore/PromotedProperty.php');

        self::assertNoSniffError($file, 15);
        self::assertNoSniffError($file, 16);
    }

    public function testSkipsOrdinaryVariables(): void
    {
        $file = self::checkFile(__DIR__ . '/data/PropertyUnderscore/PromotedProperty.php');

        self::assertNoSniffError($file, 18);
    }

    public function testSkipsVariablesInStrings(): void
    {
        $file = self::checkFile(__DIR__ . '/data/PropertyUnderscore/PromotedProperty.php');

        self::assertNoSniffError($file, 20);
    }
}
