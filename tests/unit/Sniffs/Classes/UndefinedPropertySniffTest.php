<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

declare(strict_types=1);

namespace Vaimo\Sniffs\Classes;

use SlevomatCodingStandard\Sniffs\TestCase;

class UndefinedPropertySniffTest extends TestCase
{
    public function testFindsUndeclaredVariable(): void
    {
        $file = self::checkFile(__DIR__ . '/data/UndefinedProperty/Undeclared.php');

        self::assertNoSniffError($file, 36); // No error on inherited property

        $errors = [
            [15, UndefinedPropertySniff::CODE_ERROR, 'f', '\\A'],
            [16, UndefinedPropertySniff::CODE_ERROR, 'd', '\\A'],
            [37, UndefinedPropertySniff::CODE_ERROR, 'b', '\\C'],
            [47, UndefinedPropertySniff::CODE_ERROR_HEREDOC, 'c', '\\D'],
            [59, UndefinedPropertySniff::CODE_ERROR_HEREDOC, 'c', '\\D'],
            [77, UndefinedPropertySniff::CODE_ERROR_HEREDOC, 'c', '\\D'],
            [77, UndefinedPropertySniff::CODE_ERROR_HEREDOC, 'cs', '\\D'],
        ];

        foreach ($errors as [$line, $code, $property, $class]) {
            self::assertSniffError($file, $line, $code);
        }

        self::assertSame(7, $file->getErrorCount());
    }

    public function testSkipsOnParsingErrors(): void
    {
        $file = self::checkFile(__DIR__ . '/data/UndefinedProperty/ParsingErrors.php');

        self::assertNoSniffErrorInFile($file);
    }

    public function testSkipsOnNonExistingClass(): void
    {
        $this->expectException(\Throwable::class);
        $this->expectExceptionMessageMatches('/^Class "?\\\ClassThatDoesntExist(")? does not exist$/');
        $file = self::checkFile(__DIR__ . '/data/UndefinedProperty/NonExistingClass.php', [
            'skipOnReflectionError' => true,
        ]);

        self::assertNoSniffErrorInFile($file);
    }

    /**
     * Since we cannot know actual value of variable and therefore cannot find out which property is used here...
     * @throws \Exception
     */
    public function testSkipsVariableProperty(): void
    {
        self::assertNoSniffErrorInFile(
            self::checkFile(__DIR__ . '/data/UndefinedProperty/VariableProperty.php')
        );
    }

    public function testHandlesAnonymousClass(): void
    {
        $file = self::checkFile(__DIR__ . '/data/UndefinedProperty/AnonymousClass.php');

        self::assertSniffError($file, 21, UndefinedPropertySniff::CODE_ERROR);
        self::assertNoSniffError($file, 22);
        self::assertSniffError($file, 27, UndefinedPropertySniff::CODE_ERROR);
    }

    public function testHandlesPromotedProperties(): void
    {
        self::assertNoSniffErrorInFile(
            self::checkFile(__DIR__ . '/data/UndefinedProperty/PromotedProperty.php')
        );
    }
}
