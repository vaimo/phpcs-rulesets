<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

declare(strict_types=1);

namespace Vaimo\Sniffs\Classes;

use SlevomatCodingStandard\Sniffs\TestCase;

class ObjectInstantiationSniffTest extends TestCase
{
    public function testDoesNotFailOnClassNameInVariable(): void
    {
        $file = self::checkFile(__DIR__ . '/data/ObjectInstantiation/Variable.php');

        self::assertSame(2, $file->getErrorCount());

        self::assertSniffError(
            $file,
            9,
            ObjectInstantiationSniff::ERROR_CODE,
            'Direct object instantiation is discouraged. Class name is stored in variable or method: $className'
        );

        self::assertSniffError(
            $file,
            20,
            ObjectInstantiationSniff::ERROR_CODE,
            // phpcs:ignore Generic.Files.LineLength
            'Direct object instantiation is discouraged. Class name is stored in variable or method: ($this->getName())()'
        );
    }

    public function testDoesNotFailIfClassIsWithoutParentheses(): void
    {
        self::assertNoSniffErrorInFile(
            self::checkFile(__DIR__ . '/data/ObjectInstantiation/WithoutParentheses.php')
        );
    }

    public function testAllowsFinalClassInstantiation(): void
    {
        self::assertNoSniffErrorInFile(
            self::checkFile(__DIR__ . '/data/ObjectInstantiation/Final.php')
        );
    }

    public function testDisallowsFinalClassInstantiation(): void
    {
        $file = self::checkFile(__DIR__ . '/data/ObjectInstantiation/Final.php', [
            'allowInstantiateFinalClasses' => false,
        ]);

        self::assertSniffError(
            $file,
            7,
            ObjectInstantiationSniff::ERROR_CODE,
            'Direct object instantiation (object of \Vaimo\Sniffs\Stub\FinalStub) is discouraged.'
        );
    }

    public function testAllowsExceptionInstantiation(): void
    {
        $file = self::checkFile(__DIR__ . '/data/ObjectInstantiation/ThrowException.php');

        self::assertNoSniffError($file, 7);
        self::assertNoSniffError($file, 9);
    }

    public function testDisallowsClassNameVariables(): void
    {
        $file = self::checkFile(__DIR__ . '/data/ObjectInstantiation/ThrowException.php');

        // Unfortunately, without running the code we could not decide
        // whether variable contains exception class name or not so that better to raise an error
        self::assertSame(2, $file->getErrorCount());
        self::assertSniffError($file, 14, ObjectInstantiationSniff::ERROR_CODE);
        self::assertSniffError($file, 16, ObjectInstantiationSniff::ERROR_CODE);
    }

    public function testAllowsListOfClasses(): void
    {
        $file = self::checkFile(__DIR__ . '/data/ObjectInstantiation/AllowedClasses.php', [
            'allowedClasses' => [\DateTime::class],
        ]);

        self::assertNoSniffErrorInFile($file);
    }

    public function testAllowsAnonymousClasses(): void
    {
        self::assertNoSniffErrorInFile(
            self::checkFile(__DIR__ . '/data/ObjectInstantiation/Anonymous.php')
        );
    }

    public function testDisallowsAnonymousClasses(): void
    {
        $file = self::checkFile(__DIR__ . '/data/ObjectInstantiation/Anonymous.php', [
            'allowAnonymousClasses' => false,
        ]);

        self::assertSniffError(
            $file,
            7,
            ObjectInstantiationSniff::ERROR_ANONYMOUS_CODE,
            ObjectInstantiationSniff::ERROR_ANONYMOUS_MESSAGE
        );
    }

    public function testParsesNamespaces(): void
    {
        $file = self::checkFile(__DIR__ . '/data/ObjectInstantiation/Namespaces.php');

        self::assertSame(2, $file->getErrorCount());

        self::assertSniffError(
            $file,
            15,
            ObjectInstantiationSniff::ERROR_CODE,
            'Direct object instantiation (object of \Vaimo\Sniffs\Stub\TestStub) is discouraged.'
        );

        self::assertSniffError(
            $file,
            16,
            ObjectInstantiationSniff::ERROR_CODE,
            'Direct object instantiation (object of \Vaimo\Sniffs\Stub\AnotherStub) is discouraged.'
        );
    }

    public function testReflectionError(): void
    {
        $file = self::checkFile(__DIR__ . '/data/ObjectInstantiation/ClassDoesNotExist.php', [
            'reportReflectionError' => true,
            'considerFinalInstantiationOnReflectionError' => true,
        ]);

        self::assertSame(1, $file->getErrorCount());

        self::assertSniffError(
            $file,
            7,
            ObjectInstantiationSniff::REFLECTION_ERROR_CODE
        );
    }

    public function testAllowsNamedConstructors(): void
    {
        $file = self::checkFile(__DIR__ . '/data/ObjectInstantiation/NamedConstructor.php');

        self::assertNoSniffErrorInFile($file);
    }

    public function testDisallowsNamedConstructors(): void
    {
        $file = self::checkFile(__DIR__ . '/data/ObjectInstantiation/NamedConstructor.php', [
            'allowNamedConstructors' => false,
        ]);

        self::assertSame(3, $file->getErrorCount());
        self::assertSniffError($file, 12, ObjectInstantiationSniff::ERROR_CODE);
        self::assertSniffError($file, 26, ObjectInstantiationSniff::ERROR_CODE);
        self::assertSniffError($file, 40, ObjectInstantiationSniff::ERROR_CODE);
    }
}
