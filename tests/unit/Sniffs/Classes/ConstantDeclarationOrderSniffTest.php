<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

declare(strict_types=1);

namespace Vaimo\Sniffs\Classes;

use SlevomatCodingStandard\Sniffs\TestCase;

class ConstantDeclarationOrderSniffTest extends TestCase
{
    public function testSniff(): void
    {
        $file = self::checkFile(__DIR__ . '/data/ConstantDeclarationOrder/ConstantDeclarationOrder.php');

        self::assertNoSniffError($file, 11);
        self::assertNoSniffError($file, 13);
        self::assertSniffError($file, 17, ConstantDeclarationOrderSniff::CODE_ERROR);
        self::assertNoSniffError($file, 20);
    }
}
