<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

$this->storeConfig->isWorkwearStore()
    ? ''
    : $this->getUrl(
        '',
        [
            '_secure' => true,
            '_direct' => 'store-finder',
            '_type' => UrlInterface::URL_TYPE_DIRECT_LINK,
        ]
    ) . '/';

$this->getUrl(
    '',
    [
        '_secure' => true,
        '_direct' => 'store-finder',
        '_type' => UrlInterface::URL_TYPE_DIRECT_LINK,
    ]
)
    ? $this->storeConfig->isWorkwearStore()
    : '';


$this->storeConfig->isWorkwearStore()
    ? $this->getUrl(
        '',
        [
            '_secure' => true,
            '_direct' => 'store-finder',
            '_type' => UrlInterface::URL_TYPE_DIRECT_LINK,
        ]
    ) . '/'
    : '';

$this->storeConfig->isWorkwearStore()
    ?: $this->getUrl(
        '',
        [
            '_secure' => true,
            '_direct' => 'store-finder',
            '_type' => UrlInterface::URL_TYPE_DIRECT_LINK,
        ]
    ) . '/';
