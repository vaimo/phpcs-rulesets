<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

$this->isConditionMet()
    ? $this->something ? $this->doThis() : $this->doThat()
    : null;

$this->isConditionMet()
    ? null
    : $this->something ? $this->doThis() : $this->doThat();
