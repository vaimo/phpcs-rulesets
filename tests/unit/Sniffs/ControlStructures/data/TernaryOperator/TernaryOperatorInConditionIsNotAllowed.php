<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

if ($this->lastProgressDisplayedTimestamp
    && $currentTimestamp - $this->lastProgressDisplayedTimestamp < ($remainingTime > 30 ? 3 : 1)) {
    return;
}


if (in_array('value', $this->getFalseableList() ?: [])) {
    doSomething();
}

// These examples are there only to check if there is no NPE in sniff code
return $remainingTime > 30 ? 3 : 1;
$remainingTime > 30 ? 3 : 1;
foreach ($this->falseableList ?: [] as $item) {

}
