<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

declare(strict_types=1);

$this->taxConfig->displayCartSubtotalInclTax() ?
    $order->getSubtotalInclTax() :
    $order->getSubtotal();

$baseDiscountTaxCompensationAmount = $this->globalStore
    ? $orderItem->getBaseDiscountTaxCompensationAmount()
    : $orderItem->getDiscountTaxCompensationAmount();

$baseDiscountTaxCompensationAmount = $this->globalStore ?
    $orderItem->getBaseDiscountTaxCompensationAmount()
    : $orderItem->getDiscountTaxCompensationAmount();

$baseDiscountTaxCompensationAmount = $this->globalStore ?:
    $orderItem->getBaseDiscountTaxCompensationAmount();

$baseDiscountTaxCompensationAmount = $this->globalStore
    ?: $orderItem->getBaseDiscountTaxCompensationAmount();

$a = $b ? $c : $d;

$a = $b ?: $c;
