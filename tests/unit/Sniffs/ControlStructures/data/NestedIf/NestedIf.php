<?php
/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

if ($test) {
    doSomething();
} else {
    $result = doAnother();
    if (!$result) {
        doThird();
    }
}


if ($test) {
    if (!$something) {
        doSomething();
    } else {
        if ($test === null) {
            doAnotherThing();
        }
    }
}

if ($collection) {
    foreach ($collection as $item) {
        if (!isGoodEnough($item)) {
            continue;
        }

        $test[] = $item;
    }
}


if (!$test) {
    return;
}

if (!$something) {
    return doSomething();
}

if ($test === null) {
    doAnotherThing();
}

if ($test) doSomething();
