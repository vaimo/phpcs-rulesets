<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

declare(strict_types=1);

namespace Vaimo\Sniffs\ControlStructures;

use SlevomatCodingStandard\Sniffs\TestCase;

class NestedIfSniffTest extends TestCase
{
    public function testNestedIf(): void
    {
        $file = self::checkFile(__DIR__ . '/data/NestedIf/NestedIf.php');

        self::assertSniffError($file, 11, NestedIfSniff::CODE_FOUND);
        self::assertSniffError($file, 18, NestedIfSniff::CODE_FOUND);
        self::assertSniffError($file, 21, NestedIfSniff::CODE_FOUND);
        self::assertSniffError($file, 29, NestedIfSniff::CODE_FOUND);
    }

    public function testAllowsConfigurableNestingLevel(): void
    {
        $file = self::checkFile(__DIR__ . '/data/NestedIf/NestedIf.php', [
            'maxAllowedNesting' => 1,
        ]);

        self::assertNoSniffError($file, 11);
        self::assertNoSniffError($file, 18);
        self::assertNoSniffError($file, 29);
    }
}
