<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

declare(strict_types=1);

namespace Vaimo\Sniffs\ControlStructures;

use SlevomatCodingStandard\Sniffs\TestCase;

class TernaryOperatorPlacementSniffTest extends TestCase
{
    public function testAllowsOnlyFirst(): void
    {
        $file = self::checkFile(__DIR__ . '/data/TernaryOperatorPlacement/TernaryOperatorPlacement.php', [
            'allowOnly' => 'first',
        ]);

        self::assertSniffError($file, 10, TernaryOperatorPlacementSniff::CODE_INLINE_THEN);
        self::assertSniffError($file, 11, TernaryOperatorPlacementSniff::CODE_INLINE_ELSE);
        self::assertNoSniffError($file, 15);
        self::assertNoSniffError($file, 16);
        self::assertSniffError($file, 18, TernaryOperatorPlacementSniff::CODE_INLINE_THEN);
        self::assertSniffError($file, 22, TernaryOperatorPlacementSniff::CODE_INLINE_ELVIS);
        self::assertNoSniffError($file, 26);
        self::assertNoSniffError($file, 28);
        self::assertNoSniffError($file, 30);
    }

    public function testAllowsOnlyLast(): void
    {
        $file = self::checkFile(__DIR__ . '/data/TernaryOperatorPlacement/TernaryOperatorPlacement.php', [
            'allowOnly' => 'last',
        ]);

        self::assertNoSniffError($file, 10);
        self::assertNoSniffError($file, 11);

        self::assertSniffError($file, 15, TernaryOperatorPlacementSniff::CODE_INLINE_THEN);
        self::assertSniffError($file, 16, TernaryOperatorPlacementSniff::CODE_INLINE_ELSE);

        self::assertNoSniffError($file, 18);
        self::assertNoSniffError($file, 19);
        self::assertSniffError($file, 20, TernaryOperatorPlacementSniff::CODE_INLINE_ELSE);

        self::assertNoSniffError($file, 22);
        self::assertSniffError($file, 26, TernaryOperatorPlacementSniff::CODE_INLINE_ELVIS);

        self::assertNoSniffError($file, 28);
        self::assertNoSniffError($file, 30);
    }

    public function testFailsOnIllegalAllowOnly(): void
    {
        $this->expectException(\UnexpectedValueException::class);
        self::checkFile(__DIR__ . '/data/TernaryOperatorPlacement/TernaryOperatorPlacement.php', [
            'allowOnly' => 'middle',
        ]);
    }
}
