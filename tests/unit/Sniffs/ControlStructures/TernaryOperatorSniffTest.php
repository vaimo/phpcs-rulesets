<?php

/**
 * Copyright © Vaimo Group. All rights reserved.
 * See LICENSE_VAIMO.txt for license details.
 */

declare(strict_types=1);

namespace Vaimo\Sniffs\ControlStructures;

use SlevomatCodingStandard\Sniffs\TestCase;

class TernaryOperatorSniffTest extends TestCase
{
    public function testNestedTernaryOperatorIsNotAllowed(): void
    {
        $file = self::checkFile(__DIR__ . '/data/TernaryOperator/NestedTernaryOperatorIsNotAllowed.php');

        self::assertSniffError($file, 9, TernaryOperatorSniff::CODE_NESTED);
        self::assertSniffError($file, 14, TernaryOperatorSniff::CODE_NESTED);
    }

    public function testTernaryOperatorInConditionIsNotAllowed(): void
    {
        $file = self::checkFile(__DIR__ . '/data/TernaryOperator/TernaryOperatorInConditionIsNotAllowed.php');

        self::assertSniffError($file, 9, TernaryOperatorSniff::CODE_FOUND_IN_CONDITION);
        self::assertSniffError($file, 14, TernaryOperatorSniff::CODE_FOUND_IN_CONDITION);
        self::assertNoSniffError($file, 19);
        self::assertNoSniffError($file, 20);
        self::assertNoSniffError($file, 21);
    }

    public function testMultilineExpressionsInsideTernaryOperatorAreNotAllowed(): void
    {
        $file = self::checkFile(
            __DIR__ . '/data/TernaryOperator/MultilineExpressionsInTernaryOperatorAreNotAllowed.php'
        );

        self::assertSniffError($file, 10, TernaryOperatorSniff::CODE_MULTILINE_EXPRESSION);
        self::assertSniffError($file, 19, TernaryOperatorSniff::CODE_MULTILINE_EXPRESSION);
        self::assertSniffError($file, 32, TernaryOperatorSniff::CODE_MULTILINE_EXPRESSION);
        self::assertSniffError($file, 43, TernaryOperatorSniff::CODE_MULTILINE_EXPRESSION);

        self::assertSame(4, $file->getErrorCount());
    }

    public function testElvisOperatorWhitespaceAllowed(): void
    {
        $file = self::checkFile(
            __DIR__ . '/data/TernaryOperator/ElvisOperatorWhitespaceAllowed.php'
        );

        self::assertNoSniffError($file, 3);
    }
}
